This is the prototype for the **Disclosure vetting application**

To run it, execute "sudo docker-compose up" at the root folder.

To make changes to the UI, access the vettingApp/app/UI folder and execute "sudo npm start". You might need to do CTRL-F5 in your browser after each file save to get the latest version.

## Reporter view
Open vettingApp/app/UI/src/userInfo.js and update the role property to UserRole.reporter. Save changes and refresh application.

## Outputchecker view
Open vettingApp/app/UI/src/userInfo.js and update the role property to UserRole.outputchecker. Save changes and refresh application.


Username: menmarc@mamvetting.onmicrosoft.com
Pass: Reporter1234

joDo@mamvetting.onmicrosoft.com 
Pass: OutputC1234