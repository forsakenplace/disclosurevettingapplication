import React,{useState} from 'react';
import DateFnsUtils from '@date-io/date-fns';
import {Container,
    Grid, 
    Paper, 
    Typography,
    FormControl,
    FormLabel,InputLabel,Select,MenuItem} from '@material-ui/core';
import {
    KeyboardDatePicker,
    MuiPickersUtilsProvider,
    } from '@material-ui/pickers';
import {Tune} from '@material-ui/icons';
import { useTranslation } from "react-i18next";
import DefaultHeader from '../CommonComponents/DefaultHeader';

import {RequestStatusId} from '../../ApplicationConfig/constants'

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
      boxShadow: 'none',
      borderRightWidth: '1px',
      borderRightStyle: 'solid',
      borderRightColor: theme.palette.divider,
      height: '100%',
      boxSizing: 'border-box',
      padding: theme.spacing(3, 2, 0, 0),
    },
    reportsSection:{
        padding:theme.spacing(2,0,0,2),
        boxShadow:'none'
    },
    filterHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 0, 3, 1),
        borderBottomWidth: '1px',
        borderBottomStyle: 'solid',
        borderBottomColor: theme.palette.divider,
      },
      datePicker: {
        margin: theme.spacing(0, 0, 3, 0),
      },
  }));

export default function ReportsDashboard(){
    const { t } = useTranslation();
    document.title =t("reportsTitle");
    const classes = useStyles();

    const [state, setState] = React.useState({
        selectedFromDate: '06/04/2020',
        selectedToDate: '08/04/2020',
        selectedDateType: 10,
        checked: [],
        requestStatusId:0
      });

    const handleFromDateChange = (date) => {
        setState({...state, selectedFromDate: date});
      };
    
      const handleToDateChange = (date) => {
        setState({...state, selectedToDate: date});
      };

      const onChangeRequestFilter=(event)=>{
        setState({...state, requestStatusId: event.target.value});
      }
    return (
        <React.Fragment>
            <DefaultHeader />
            <main tabIndex={-1}>
                <Container maxWidth="xl" className="page-container">
                <Grid container direction="row" alignItems="stretch">
                    <Grid item sm={2} lg={2}>
                        <Paper className={classes.root}>
                       
                        <div className={classes.filterHeader}>
                            <Tune fontSize="small"/>
                            <Typography variant="h6" component="h2" className="ml-2">
                            Filters
                            </Typography>
                        </div>
                        <FormControl variant="outlined" style={{width:'100%',marginBottom:'1em'}}>
                              <InputLabel id="request-status-filter-label">Request status</InputLabel>
                              <Select 
                              id="request-status-filter"
                              labelId="request-status-filter-label"
                              value={state.requestStatusId}
                              margin="dense"
                              style={{minWidth: 180}}
                              label="Filter by request status"
                              onChange={onChangeRequestFilter}
                              >
                                <MenuItem value={0}>All</MenuItem>
                                <MenuItem value={RequestStatusId.Approved}>Approved</MenuItem>
                                <MenuItem value={RequestStatusId.AwaitingReview}>Available</MenuItem>
                                <MenuItem value={RequestStatusId.Canceled}>Canceled</MenuItem>
                                <MenuItem value={RequestStatusId.Denied}>Denied</MenuItem>
                                <MenuItem value={RequestStatusId.InReview}>In review</MenuItem>
                                
                              </Select>
                            </FormControl>

                            <FormControl component="fieldset">
            <FormLabel component="legend" className="screen-reader-text">
              Date
            </FormLabel>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <KeyboardDatePicker
                id="start-date"
                className={classes.datePicker}
                variant="inline"
                margin="dense"
                format="MM/dd/yyyy"
                label="Start date"
                value={state.selectedFromDate}
                maxDate={state.selectedToDate}
                onChange={handleFromDateChange}
                inputVariant="outlined"
                PopoverProps={{
                  'aria-modal': 'true',
                }}
                KeyboardButtonProps={{
                  'aria-label': t('reportsSelectStartDate'),
                }}
              />
              <KeyboardDatePicker
                id="end-date"
                variant="inline"
                margin="dense"
                format="MM/dd/yyyy"
                label="End date"
                value={state.selectedToDate}
                minDate={state.selectedFromDate}
                onChange={handleToDateChange}
                inputVariant="outlined"
                KeyboardButtonProps={{
                  'aria-label': t('reportsSelectEndDate'),
                }}
              />
            </MuiPickersUtilsProvider>
          </FormControl>
                        
                        
                        </Paper>
                    </Grid>
                    <Grid item tabIndex={-1} sm={3} lg={3}>
                        <Paper className={classes.reportsSection}>
                            <Typography variant="h1">Requests</Typography>
                        </Paper>
                    </Grid>
                </Grid>
                </Container>
            </main>
        </React.Fragment>
    );
}