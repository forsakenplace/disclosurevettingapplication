import React,{useState} from 'react';
import {Container, Typography,Button} from '@material-ui/core';
import { useTranslation } from "react-i18next";
import DefaultHeader from '../CommonComponents/DefaultHeader';

export default function RequestReport(){
    const { t } = useTranslation();
    document.title =t("requestReportTitle");
    return (
        <React.Fragment>
            <DefaultHeader />
            <main tabIndex={-1}>
                <Container maxWidth="xl" className="page-container">
                    <Typography variant="h1">{t("requestReportTitle")}</Typography>
                </Container>
            </main>
        </React.Fragment>
    );
}