import React,{useState,useEffect} from 'react';
import {Container, Typography,Button} from '@material-ui/core';
import { useTranslation } from "react-i18next";
import DefaultHeader from '../CommonComponents/DefaultHeader';
import EnhancedTable from './RequestsListComponents/EnhancedTable';
import { useSnackbar } from 'notistack';

export default function RequestsList(){
    const { t } = useTranslation();
    document.title =t("requestListTitle");

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    
    const [pageStatus,setPageStatus]=useState({
        dataLoaded:false,
        errorMessage:''
    });

    
    useEffect(()=>{
        let key;
        if (!pageStatus.dataLoaded && pageStatus.errorMessage=='')
        {
            key=enqueueSnackbar('Loading data',{variant:'info', persist: true,preventDuplicate: true});
        }
        else if (pageStatus.dataLoaded){
            closeSnackbar(key);
            enqueueSnackbar('Succesfully loaded data',{variant:'success',autoHideDuration:2000,preventDuplicate: true});
        }

        if (!pageStatus.dataLoaded && pageStatus.errorMessage!='') {
            closeSnackbar(key);
            enqueueSnackbar(pageStatus.errorMessage,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
        }
       
    },[pageStatus]);

    return (
        <React.Fragment>
            <DefaultHeader />
            <main tabIndex={-1}>
                <Container maxWidth="xl" className="page-container">
                    <Typography variant="h1">{t("requestListTitle")}</Typography>
                    <EnhancedTable setPageStatus={setPageStatus} pageStatus={pageStatus} />
                </Container>
            </main>
        </React.Fragment>
    );
}