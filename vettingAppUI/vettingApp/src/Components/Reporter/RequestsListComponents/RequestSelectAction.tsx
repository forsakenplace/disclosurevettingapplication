import React,{useState} from 'react'
import {Select,MenuItem,InputLabel} from '@material-ui/core';
import {RequestStatusId,RequestActionId} from '../../../ApplicationConfig/constants'
import FormControl from '@material-ui/core/FormControl';

export default function RequestSelectAction(props){

    const handleChange = (event) => {
        props.updateRowHandle({requestId:props.requestId,status:event.target.value})
    };

    var items=[];

    if (props.requestStatusId==RequestStatusId.InReview)
    {
        items.push({name:"Withdraw",actionId: RequestActionId.Withdraw});
        items.push({name:"Cancel",actionId: RequestActionId.Cancel});
        items.push({name:"Duplicate",actionId: RequestActionId.Duplicate});
    }
    else if (props.requestStatusId==RequestStatusId.AwaitingReview)
    {
        items.push({name:"Withdraw",actionId: RequestActionId.Withdraw});
        items.push({name:"Cancel",actionId: RequestActionId.Cancel});
    }
    else if (props.requestStatusId==RequestStatusId.Canceled || 
        props.requestStatusId==RequestStatusId.Denied )
    {
        items.push({name:"Duplicate",actionId: RequestActionId.Duplicate});
    }
    else if (props.requestStatusId==RequestStatusId.WorkInProgress)
    {
        items.push({name:"Submit",actionId: RequestActionId.Submit});
        items.push({name:"Edit",actionId: RequestActionId.Edit});
        items.push({name:"Delete",actionId: RequestActionId.Delete});
    }
    else if (props.requestStatusId==RequestStatusId.Approved)
    {
        items.push({name:"Duplicate",actionId: RequestActionId.Duplicate});
    }

    return(
        <FormControl>
    <InputLabel id={"operations_" + props.requestId}>Action</InputLabel>
    <Select 
    labelId={"operations_" + props.requestId}
    onChange={handleChange}
    style={{minWidth: 120}}
    value=""
    >
         {items.map((item, i) => (
           
            <MenuItem key={props.requestId + "_" + i} id={props.requestId + "_" + i} value={item.actionId}>{item.name}</MenuItem>

          ))}
   
    </Select>
    </FormControl>)
}