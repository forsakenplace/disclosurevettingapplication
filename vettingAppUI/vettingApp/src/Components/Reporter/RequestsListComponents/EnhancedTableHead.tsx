import React from 'react';
import {TableHead,TableRow,TableCell,TableSortLabel} from '@material-ui/core';

import PropTypes from 'prop-types';
import clsx from 'clsx';

const headCells = [
    { id: 'statusId', numeric: true, disablePadding: false, label: 'Status' },
    { id: 'submittedOn', numeric: false, disablePadding: false, label: 'Submitted on' },
    { id: 'updatedOn', numeric: false, disablePadding: false, label: 'Updated on' },
    { id: 'requester', numeric: false, disablePadding: false, label: 'Requester' },
    { id: 'outputchecker', numeric: false, disablePadding: false, label: 'Output Checker' },
    { id: 'fileOutputCount', numeric: true, disablePadding: false, label: 'File output count' },
    { id: 'operations', numeric: true, disablePadding: false, label: 'Actions' },
  ];
  
  export default function EnhancedTableHead(props) {
    const { classes, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };
  
    return (
      <TableHead>
        <TableRow>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align='left'
              padding={headCell.disablePadding ? 'none' : 'default'}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }
  
  EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
  };