import React from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { useFormContext } from 'react-hook-form';
import ActionBar from './action-bar';

export default function RequestFooter() {

    const { register } = useFormContext();

    return (
        <Grid container spacing={3}>
            <Grid item xs={6}>
                <Typography variant="h2" style={{fontSize:'1.5em',marginBottom:'1em'}}>Request Footer</Typography>
                <TextField
                    id="additionalComments"
                    name="additionalComments"
                    label="Additional comments which may be helpful to the analyst"
                    variant="outlined"
                    inputRef={register}
                    fullWidth
                    multiline
                />
            </Grid>
            <Grid item xs={6}>
                <ActionBar />
            </Grid>
        </Grid>
    );
}