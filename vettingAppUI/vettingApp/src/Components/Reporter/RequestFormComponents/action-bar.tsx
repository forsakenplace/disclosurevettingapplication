import React from 'react';
import { useHistory,useParams } from 'react-router-dom';
import {Button,Typography} from '@material-ui/core';
import {Save,Cancel,NavigateNext,ArrowBack} from '@material-ui/icons'

export default function ActionBar(props) {
    const history = useHistory();

    let nextCaption="Submit"
    
    if (props.navigationNextCaption)
    {
        nextCaption=props.navigationNextCaption
    }
    return (
        <>
            <Typography variant="h2">Requester</Typography>
            <p>user account</p>
            <Typography variant="h2">Actions</Typography>
            <Button color="primary" onClick={()=> console.log("saved!")}>
                <Save />&nbsp;Save
            </Button>
            <br />
            <Button color="primary" onClick={()=> history.push("/")}>
                <Cancel />&nbsp;Cancel
            </Button>
            <br />
            <Button color="primary" type="submit"><NavigateNext />&nbsp;{nextCaption}</Button>
        </>
    );
}