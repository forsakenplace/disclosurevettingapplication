import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Tooltip from '@material-ui/core/Tooltip';
import InfoIcon from '@material-ui/icons/Info';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import {Typography} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';

import { useFormContext, Controller } from 'react-hook-form';
import { useStateMachine } from 'little-state-machine';
import ActionBar from './action-bar';

const useStyles = makeStyles((theme) => ({
    inputMargin:{
        marginBottom:'1em',
        marginTop:'1em'
        },
}))

export default function RequestHeader() {
    const classes = useStyles();

    const { state } = useStateMachine();
    const { register, errors, control, setValue, trigger } = useFormContext();

    const [fileDisabled, setFileDisabled] = useState(state.requestFormData.outputFilesFolder ? false : true);

    const requiredErrorMessage = "This field is required";
    
    //fake data until this can be figured out
    const fileLocations = ["/folder1", "/folder2"];
    
    const handleFileChange = (e, onChange)=> {
        if(e.target.value !== ""){
            setFileDisabled(false);
            setValue("supportingFilesFolder",e.target.value);
            trigger("supportingFilesFolder");
        }else {
            setFileDisabled(true);
            setValue("supportingFilesFolder","");
        }

        onChange(e.target.value);
    };

    return (
        <Grid container spacing={3} direction="row">
            <Grid item xs={6}>
                <Typography variant="h2" style={{fontSize:'1.5em',marginBottom:'1em'}}>Request header</Typography>
                <TextField
                    id="name"
                    name="name"
                    label="Name"
                    variant="outlined"
                    inputRef={register({required:requiredErrorMessage})}
                    required
                    fullWidth
                    error={!!errors.name}
                    helperText={errors.name?.message}
                    className={classes.inputMargin}
                />
                <TextField
                    id="email"
                    name="email"
                    label="Email"
                    variant="outlined"
                    inputRef={register({required:requiredErrorMessage})}
                    required
                    fullWidth
                    error={!!errors.email}
                    helperText={errors.email?.message}
                    className={classes.inputMargin}
                />
                <TextField
                    id="date"
                    name="date"
                    label="Date"
                    inputRef={register({required:requiredErrorMessage})}
                    required
                    fullWidth
                    error={!!errors.date}
                    helperText={errors.date?.message}
                    inputProps={{readOnly:true}}
                    className={classes.inputMargin}
                />
                <TextField
                    id="username"
                    name="username"
                    label="User Name"
                    variant="outlined"
                    inputRef={register({required:requiredErrorMessage})}
                    required
                    fullWidth
                    error={!!errors.username}
                    helperText={errors.username?.message}
                    className={classes.inputMargin}
                />
                <TextField
                    id="contract"
                    name="contract"
                    label="Contract #"
                    variant="outlined"
                    inputRef={register({required:requiredErrorMessage})}
                    required
                    fullWidth
                    error={!!errors.contract}
                    helperText={errors.contract?.message}
                    className={classes.inputMargin}
                />
                <TextField
                    id="project"
                    name="project"
                    label="Project Title"
                    variant="outlined"
                    inputRef={register({required:requiredErrorMessage})}
                    required
                    fullWidth
                    error={!!errors.project}
                    helperText={errors.project?.message}
                    className={classes.inputMargin}
                />
                <FormControl required variant="outlined" fullWidth error={!!errors.outputFilesFolder} className={classes.inputMargin} >
                    <InputLabel id="outputFilesFolder-label">Output Folder Name</InputLabel>
                    <Controller
                        render={({onBlur, onChange, value})=> (
                            <Select
                                id="outputFilesFolder"
                                label="Output Folder Name *"
                                labelId="outputFilesFolder-label"
                                onChange={(e)=> handleFileChange(e, onChange)}
                                value={value}
                            >
                                <MenuItem key={-1} value="">None</MenuItem>
                                {fileLocations.map((item, index)=> (
                                        <MenuItem key={index} value={item}>{item}</MenuItem>
                                    ))
                                }
                            </Select>
                        )}
                        name="outputFilesFolder"
                        control={control}
                        rules={{required:requiredErrorMessage}}
                        
                    />
                    <FormHelperText>{errors.outputFilesFolder?.message}</FormHelperText>
                </FormControl>
                <FormControl required variant="outlined" fullWidth error={!!errors.supportingFilesFolder} disabled={fileDisabled} className={classes.inputMargin} >
                    <InputLabel id="supportingFilesFolder-label">Supporting Files Folder Name</InputLabel>
                    <Controller
                        render={({onBlur, onChange, value})=> (
                            <Select
                                id="supportingFilesFolder"
                                label="Supporting Files Folder Name *"
                                labelId="supportingFilesFolder-label"
                                onChange={onChange}
                                value={value}
                            >
                                <MenuItem key={-1} value="">None</MenuItem>
                                {fileLocations.map((item, index)=> (
                                        <MenuItem key={index} value={item}>{item}</MenuItem>
                                    ))
                                }
                            </Select>
                        )}
                        name="supportingFilesFolder"
                        control={control}
                        rules={{required:requiredErrorMessage}}
                    />
                    <FormHelperText>{errors.supportingFilesFolder?.message}</FormHelperText>
                </FormControl>

                <p>Please check your output against the vetting guidelines; consult an analyst if you are unsure</p>
                <p>Please delete values you do not need released at this time</p>
                <p>A completed request form will be stored as part of the request record</p>

                <FormControl component="fieldset" required error={!!errors.consistentOutput}>
                    <FormLabel component="legend">Is the requested output consistent with the approved proposal for this project?</FormLabel>
                    <Controller
                        render={({onBlur, onChange, value})=> (
                            <RadioGroup id="consistentOutput" onChange={onChange} value={value} row>
                                <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                <FormControlLabel value="No" control={<Radio />} label="No" />
                                <FormControlLabel value="NA" control={<Radio />} label="NA" />
                            </RadioGroup>
                        )}
                        name="consistentOutput"
                        control={control}
                        rules={{required:requiredErrorMessage}}
                    />
                    <FormHelperText>{errors.consistentOutput?.message}</FormHelperText>
                </FormControl>
                <FormControl component="fieldset" required error={!!errors.vettingRules}>
                    <FormLabel component="legend">Have you checked the vetting rules to determine if there are geographical, institutional, household size and/or population requirements for your output</FormLabel>
                    <Controller
                        render={({onBlur, onChange, value})=> (
                            <RadioGroup id="vettingRules" onChange={onChange} value={value} row>
                                <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                <FormControlLabel value="No" control={<Radio />} label="No" />
                                <FormControlLabel value="NA" control={<Radio />} label="NA" />
                            </RadioGroup>
                        )}
                        name="vettingRules"
                        control={control}
                        rules={{required:requiredErrorMessage}}
                    />
                    <FormHelperText>{errors.vettingRules?.message}</FormHelperText>
                </FormControl>
                <FormControl component="fieldset" required error={!!errors.finalOutput}>
                    <FormLabel component="legend">
                        Is the requested output your final output?
                        <Tooltip title="If no, future vetting release reuests under this contract may be restricted due to residual disclosure. You are strongly encouraged to consult with your analyst." arrow>
                            <InfoIcon />
                        </Tooltip>
                    </FormLabel>
                    <Controller
                        render={({onBlur, onChange, value})=> (
                            <RadioGroup id="finalOutput" onChange={onChange} value={value} row>
                                <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                <FormControlLabel value="No" control={<Radio />} label="No" />
                                <FormControlLabel value="NA" control={<Radio />} label="NA" />
                            </RadioGroup>
                        )}
                        name="finalOutput"
                        control={control}
                        rules={{required:requiredErrorMessage}}
                    />
                    <FormHelperText>{errors.finalOutput?.message}</FormHelperText>
                </FormControl>
            </Grid>
            <Grid item xs={6}>
                <ActionBar navigationNextCaption={state.requestFormData.tabsDisabled ? "Next section" : "Submit"} />
            </Grid>
        </Grid>
    );
}