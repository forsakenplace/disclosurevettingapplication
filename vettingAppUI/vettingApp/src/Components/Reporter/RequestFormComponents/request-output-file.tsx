import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import InfoIcon from '@material-ui/icons/Info';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { useFormContext, Controller } from 'react-hook-form';
import { useStateMachine } from 'little-state-machine';
import {updateRequestFormData} from '../../../Actions/actions';
import ActionBar from './action-bar';
import RequestSupportingFilesList from './request-supporting-files-list';

export default function RequestOutputFile({output}) {
    const { state } = useStateMachine(updateRequestFormData);
    const index = output-1;
    const outputFileData = state.requestFormData.outputFiles[index];
    const { register, control, errors, setValue, watch } = useFormContext();

    const [mandatories, setMandatories] = useState(outputFileData.mandatorySupportingFiles);

    const watchFields = watch([
        "outputFiles["+index+"].outputMethod",
        "outputFiles["+index+"].includeWeightVariable",
        "outputFiles["+index+"].linkedData",
        "outputFiles["+index+"].descriptiveStats",
        "outputFiles["+index+"].modifiedWeights",
        "outputFiles["+index+"].includeMatrix",
        "outputFiles["+index+"].roundingOutput"
    ]);

    const files = ["file1", "file2"];

    const outputMethods = {
        "1.Descriptive": ["ANOVA", "Correlation matrix", "Cross-tabular analysis", "Distributions", "Frequencies", "Kurtosis", "Means", "Medians", "Modes", "Percentages", "Quartiles", "Ranges", "Ratios", "Regression models with only one independent variable", "Skewness", "Standard deviations", "Totals", "Variances"],
        "2.Scaling": ["Factor Analysis"],
        "3.Graphs": ["Histograms"],
        "4.Multivariable regression analysis": ["Logistic Regression", "OLS", "Poisson", "Probit", "Tobit"],
        "5.Complex modeling": ["Event History Analysis", "Fixed Effects Models", "Growth Analysis", "Hierarchical Linear Modeling", "Random Effects Models", "Simultaneous-Equations Models", "Structural equation modeling", "Survival Analysis"],
        "6.Other":[]
    };
    const outputMethodsTerms = [];
    for(const [key, value] of Object.entries(outputMethods)){
        if(value.length>0){
            for(let term of value){
                outputMethodsTerms.push({method:key, term:term});
            }
        }
    }

    const handleMandatoryChange = (e, target, files, onChange)=> {
        if(e.target.value === target){
            let newMandatories = {...mandatories};
            for(let file of files){
                newMandatories[file] = {fileName:"", notes:""};
            }
            setMandatories(newMandatories);
        }else if(mandatories[files[0]]){
            let newMandatories = {...mandatories};
            for(let file of files){
                delete newMandatories[file];
            }
            setMandatories(newMandatories);
        }

        onChange(e.target.value);
    };

    const requiredErrorMessage = "This field is required";

    return (
        <Grid container spacing={3}>
            <Grid item xs={6}>
                <fieldset>
                    <legend>Output Header</legend>
                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <FormControl required variant="outlined" fullWidth error={errors.outputFiles && errors.outputFiles[index].outputFile ? true : false} >
                                <InputLabel id="outputFile-label">File for release</InputLabel>
                                <Controller
                                    render={({onBlur, onChange, value})=> (
                                        <Select
                                            id="outputFile"
                                            label="File for release *"
                                            labelId="outputFile-label"
                                            onChange={onChange}
                                            value={value}
                                        >
                                            {files.map((item, index)=> (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                ))
                                            }
                                        </Select>
                                    )}
                                    name={"outputFiles["+index+"].outputFile"}
                                    control={control}
                                    rules={{required:requiredErrorMessage}}
                                />
                                <FormHelperText>{errors.outputFiles && errors.outputFiles[index].outputFile ? errors.outputFiles[index].outputFile.message : ""}</FormHelperText>
                            </FormControl>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                id="sheetName"
                                name={"outputFiles["+index+"].sheetName"}
                                label="Sheet Name"
                                variant="outlined"
                                helperText="Each sheet for a spreadsheet file should be listed separately"
                                inputRef={register}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField
                                id="datasetName"
                                name={"outputFiles["+index+"].datasetName"}
                                label="Survey or Dataset Name and Cycle(s)"
                                variant="outlined"
                                inputRef={register}
                                fullWidth
                            />
                        </Grid>
                        <Grid container item spacing={1} xs={12}>
                            <Grid item xs={6}>
                                <FormControl variant="outlined" fullWidth error={errors.outputFiles && errors.outputFiles[index].outputMethod ? true : false} >
                                    <InputLabel id="outputMethod-label">Output Method</InputLabel>
                                    <Controller
                                        render={({onBlur, onChange, value})=> (
                                            <Select
                                                id="outputMethod"
                                                label="Output Method *"
                                                labelId="outputMethod-label"
                                                onChange={(e)=> handleMandatoryChange(e,"3.Graphs", ["supporting tabulations for graphs"], onChange)}
                                                value={value}
                                            >
                                                {Object.keys(outputMethods).map((item, index)=> (
                                                        <MenuItem key={index} value={item}>{item}</MenuItem>
                                                    ))
                                                }
                                            </Select>
                                        )}
                                        name={"outputFiles["+index+"].outputMethod"}
                                        control={control}
                                    />
                                    <FormHelperText>{errors.outputFiles && errors.outputFiles[index].outputMethod ? errors.outputFiles[index].outputMethod.message : ""}</FormHelperText>
                                </FormControl>
                            </Grid>
                            <Grid item xs={6}>
                                <Autocomplete
                                    id="outputMethodSearch"
                                    options={outputMethodsTerms}
                                    onChange={(e, val)=> {
                                        setValue("outputMethod", val.method);
                                    }}
                                    getOptionLabel={(option)=> option.term}
                                    renderOption={(option)=> (<>{option.term} - {option.method}</>)}
                                    renderInput={(params)=> <TextField {...params} name="outputMethodSearch" label="Not sure? Search here" variant="outlined"/>}
                                    getOptionSelected={(option, value)=> {return option.term===value.term && option.method===value.method;}}
                                />
                            </Grid>
                            {watchFields["outputFiles["+index+"].outputMethod"]==="6.Other" &&
                                <Grid item xs={6}>
                                    <TextField
                                        id="outputMethodDescription"
                                        name={"outputFiles["+index+"].outputMethodDescription"}
                                        label="Description of output Method"
                                        variant="outlined"
                                        inputRef={register({required:requiredErrorMessage})}
                                        error={errors.outputFiles && errors.outputFiles[index].outputMethodDescription ? true : false}
                                        helperText={errors.outputFiles && errors.outputFiles[index].outputMethodDescription ? errors.outputFiles[index].outputMethodDescription.message : ""}
                                        required
                                        fullWidth
                                        multiline
                                    />
                                </Grid>
                            }
                        </Grid>
                        <Grid container item spacing={1} xs={12}>
                            <Grid item xs={12}>
                                <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].includeWeightVariable ? true : false}>
                                    <FormLabel component="legend">Does this output include a weight variable?</FormLabel>
                                    <Controller
                                        render={({onBlur, onChange, value})=> (
                                            <RadioGroup id="includeWeightVariable" onChange={onChange} value={value} row>
                                                <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                <FormControlLabel value="No" control={<Radio />} label="No" />
                                            </RadioGroup>
                                        )}
                                        name={"outputFiles["+index+"].includeWeightVariable"}
                                        control={control}
                                    />
                                    <FormHelperText>{errors.outputFiles && errors.outputFiles[index].includeWeightVariable ? errors.outputFiles[index].includeWeightVariable.message : ""}</FormHelperText>
                                </FormControl>
                            </Grid>
                            {watchFields["outputFiles["+index+"].includeWeightVariable"]==="Yes" &&
                                <>
                                    <Grid item xs={12}>
                                        <TextField
                                            id="weightVariableName"
                                            name={"outputFiles["+index+"].weightVariableName"}
                                            label="Name of weight variable"
                                            variant="outlined"
                                            inputRef={register({required:requiredErrorMessage})}
                                            error={errors.outputFiles && errors.outputFiles[index].weightVariableName ? true : false}
                                            helperText={errors.outputFiles && errors.outputFiles[index].weightVariableName ? errors.outputFiles[index].weightVariableName.message : ""}
                                            required
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormControl component="fieldset" required error={errors.outputFiles && errors.outputFiles[index].weightVariableType ? true : false}>
                                            <FormLabel component="legend">Is the weight variable scaled or normalized?</FormLabel>
                                            <Controller
                                                render={({onBlur, onChange, value})=> (
                                                    <RadioGroup id="weightVariableType" onChange={onChange} value={value} row>
                                                        <FormControlLabel value="Scaled" control={<Radio />} label="Scaled" />
                                                        <FormControlLabel value="Normalized" control={<Radio />} label="Normalized" />
                                                    </RadioGroup>
                                                )}
                                                name={"outputFiles["+index+"].weightVariableType"}
                                                control={control}
                                                rules={{required:requiredErrorMessage}}
                                            />
                                            <FormHelperText>{errors.outputFiles && errors.outputFiles[index].weightVariableType ? errors.outputFiles[index].weightVariableType.message : ""}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                </>
                            }
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="sampleUsed"
                                name={"outputFiles["+index+"].sampleUsed"}
                                label="Sample, sub-sample or inclusions/exclusions used"
                                variant="outlined"
                                helperText="Example: males 50 years of age or older. Required if you subsetted or selected only a certain set of respondents from the data for all or part of the analysis"
                                inputRef={register}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                id="geographyLevel"
                                name={"outputFiles["+index+"].geographyLevel"}
                                label="Level of geography"
                                variant="outlined"
                                helperText="Examples: national, provincial"
                                inputRef={register}
                                fullWidth
                            />
                        </Grid>
                    </Grid>
                </fieldset>
                <fieldset>
                    <legend>Output Supporting Files</legend>
                    <Grid container spacing={3}>
                        <Grid container item xs={6} spacing={1}>
                            <Grid item xs={12}>
                                <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].linkedData ? true : false}>
                                    <FormLabel component="legend">Is linked data used?</FormLabel>
                                    <Controller
                                        render={({onBlur, onChange, value})=> (
                                            <RadioGroup id="linkedData" onChange={onChange} value={value} row>
                                                <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                <FormControlLabel value="No" control={<Radio />} label="No" />
                                                <FormControlLabel value="NA" control={<Radio />} label="NA" />
                                            </RadioGroup>
                                        )}
                                        name={"outputFiles["+index+"].linkedData"}
                                        control={control}
                                    />
                                    <FormHelperText>{errors.outputFiles && errors.outputFiles[index].linkedData ? errors.outputFiles[index].linkedData.message : false}</FormHelperText>
                                </FormControl>
                            </Grid>
                            {watchFields["outputFiles["+index+"].linkedData"] === "Yes" &&
                                <Grid item xs={12}>
                                    <TextField
                                        id="linkedDataDescription"
                                        name={"outputFiles["+index+"].linkedDataDescription"}
                                        label="Describe how linkage was done"
                                        variant="outlined"
                                        inputRef={register({required:requiredErrorMessage})}
                                        error={errors.outputFiles && errors.outputFiles[index].linkedDataDescription ? true : false}
                                        helperText={errors.outputFiles && errors.outputFiles[index].linkedDataDescription ? errors.outputFiles[index].linkedDataDescription.message : "Examples: person-based, record-based, matching geographies"}
                                        required
                                        fullWidth
                                        multiline
                                    />
                                </Grid>
                            }
                        </Grid>
                        <Grid item xs={6}>
                            <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].dollarIncluded ? true : false}>
                                <FormLabel component="legend">
                                    Are variables related to income, earnings, tax and/or dollar values included?
                                    <Tooltip title="If yes, check the vetting guidelines and requirements for these kinds of variables. Consult your analyst if needed." arrow>
                                        <InfoIcon />
                                    </Tooltip>
                                </FormLabel>
                                <Controller
                                    render={({onBlur, onChange, value})=> (
                                        <RadioGroup id="dollarIncluded" onChange={(e)=> handleMandatoryChange(e, "Yes", ["unweighted supporting sample counts", "syntax used for variable creation, analysis and running the vetting tests", 'vetting test results (e.g. test of magnitude, dominance, etc)'], onChange)} value={value} row>
                                            <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                            <FormControlLabel value="No" control={<Radio />} label="No" />
                                            <FormControlLabel value="NA" control={<Radio />} label="NA" />
                                        </RadioGroup>
                                    )}
                                    name={"outputFiles["+index+"].dollarIncluded"}
                                    control={control}
                                />
                                <FormHelperText>{errors.outputFiles && errors.outputFiles[index].dollarIncluded ? errors.outputFiles[index].dollarIncluded.message : ""}</FormHelperText>
                            </FormControl>
                        </Grid>
                        <Grid container item xs={12} spacing={1}>
                            <Grid item xs={12}>
                                <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].descriptiveStats ? true : false}>
                                    <FormLabel component="legend">Does the request include descriptive statistics?</FormLabel>
                                    <Controller
                                        render={({onBlur, onChange, value})=> (
                                            <RadioGroup id="descriptiveStats" onChange={(e)=> handleMandatoryChange(e, "Yes", ['correct supporting documentation according to the vetting rules (e.g. counts are unweighted / weighted / weighted and rounded)'], onChange)} value={value} row>
                                                <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                <FormControlLabel value="No" control={<Radio />} label="No" />
                                                <FormControlLabel value="NA" control={<Radio />} label="NA" />
                                            </RadioGroup>
                                        )}
                                        name={"outputFiles["+index+"].descriptiveStats"}
                                        control={control}
                                    />
                                    <FormHelperText>{errors.outputFiles && errors.outputFiles[index].descriptiveStats ? errors.outputFiles[index].descriptiveStats.message : ""}</FormHelperText>
                                </FormControl>
                            </Grid>
                            {watchFields["outputFiles["+index+"].descriptiveStats"] === "Yes" &&
                                <>
                                    <Grid item xs={12}>
                                        <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].outputLabelled ? true : false} required>
                                            <FormLabel component="legend">Is the output clearly labelled (tables have a title and every variable and category is labelled)?</FormLabel>
                                            <Controller
                                                render={({onBlur, onChange, value})=> (
                                                    <RadioGroup id="outputLabelled" onChange={onChange} value={value} row>
                                                        <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                        <FormControlLabel value="No" control={<Radio />} label="No" />
                                                    </RadioGroup>
                                                )}
                                                name={"outputFiles["+index+"].outputLabelled"}
                                                control={control}
                                                rules={{required:requiredErrorMessage}}
                                            />
                                            <FormHelperText>{errors.outputFiles && errors.outputFiles[index].outputLabelled ? errors.outputFiles[index].outputLabelled.message : ""}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].minCellSize ? true : false} required>
                                            <FormLabel component="legend">Are minimum cell sizes met as per the rules for the data?</FormLabel>
                                            <Controller
                                                render={({onBlur, onChange, value})=> (
                                                    <RadioGroup id="minCellSize" onChange={onChange} value={value} row>
                                                        <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                        <FormControlLabel value="No" control={<Radio />} label="No" />
                                                    </RadioGroup>
                                                )}
                                                name={"outputFiles["+index+"].minCellSize"}
                                                control={control}
                                                rules={{required:requiredErrorMessage}}
                                            />
                                            <FormHelperText>{errors.outputFiles && errors.outputFiles[index].minCellSize ? errors.outputFiles[index].minCellSize.message : ""}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                </>
                            }
                        </Grid>
                        <Grid item xs={6}>
                            <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].equivalentDescriptiveStats ? true : false}>
                                <FormLabel component="legend">
                                    Does this request include model output or graphs that are equivalent to a descriptive statistics?
                                    <Tooltip title="Examples: a model with a single independant variable, a model with all possible interactions, histograms" arrow>
                                        <InfoIcon />
                                    </Tooltip>
                                </FormLabel>
                                <Controller
                                    render={({onBlur, onChange, value})=> (
                                        <RadioGroup id="equivalentDescriptiveStats" onChange={(e)=> handleMandatoryChange(e, "Yes", ["unweighted frequency table for respondent counts"], onChange)} value={value} row>
                                            <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                            <FormControlLabel value="No" control={<Radio />} label="No" />
                                            <FormControlLabel value="NA" control={<Radio />} label="NA" />
                                        </RadioGroup>
                                    )}
                                    name={"outputFiles["+index+"].equivalentDescriptiveStats"}
                                    control={control}
                                />
                                <FormHelperText>{errors.outputFiles && errors.outputFiles[index].equivalentDescriptiveStats ? errors.outputFiles[index].equivalentDescriptiveStats.message : ""}</FormHelperText>
                            </FormControl>
                        </Grid>
                        <Grid container item xs={6} spacing={1}>
                            <Grid item xs={12}>
                                <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].modifiedWeights ? true : false}>
                                    <FormLabel component="legend">
                                        Did you apply modified (e.g. standardized) weights in the analysis?
                                        <Tooltip title="If yes, consult with your analyst about the vetting rules for modified weights." arrow>
                                            <InfoIcon />
                                        </Tooltip>
                                    </FormLabel>
                                    <Controller
                                        render={({onBlur, onChange, value})=> (
                                            <RadioGroup id="modifiedWeights" onChange={onChange} value={value} row>
                                                <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                <FormControlLabel value="No" control={<Radio />} label="No" />
                                                <FormControlLabel value="NA" control={<Radio />} label="NA" />
                                            </RadioGroup>
                                        )}
                                        name={"outputFiles["+index+"].modifiedWeights"}
                                        control={control}
                                    />
                                    <FormHelperText>{errors.outputFiles && errors.outputFiles[index].modifiedWeights ? errors.outputFiles[index].modifiedWeights : ""}</FormHelperText>
                                </FormControl>
                            </Grid>
                            {watchFields["outputFiles["+index+"].modifiedWeights"]==="Yes" &&
                                <Grid item xs={12}>
                                    <TextField
                                        id="modifiedWeightsDescription"
                                        name={"outputFiles["+index+"].modifiedWeightsDescription"}
                                        label="Describe why and how the weights were modified"
                                        variant="outlined"
                                        inputRef={register({required:requiredErrorMessage})}
                                        error={errors.outputFiles && errors.outputFiles[index].modifiedWeightsDescription ? true : false}
                                        helperText={errors.outputFiles && errors.outputFiles[index].modifiedWeightsDescription ? errors.outputFiles[index].modifiedWeightsDescription.message : ""}
                                        required
                                        fullWidth
                                        multiline
                                    />
                                </Grid>
                            }
                        </Grid>
                        <Grid container item xs={12} spacing={1}>
                            <Grid item xs={12}>
                                <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].includeMatrix ? true : false} >
                                    <FormLabel component="legend">Does this output include a correlation or covariance matrix?</FormLabel>
                                    <Controller
                                        render={({onBlur, onChange, value})=> (
                                            <RadioGroup id="includeMatrix" onChange={onChange} value={value} row>
                                                <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                <FormControlLabel value="No" control={<Radio />} label="No" />
                                                <FormControlLabel value="NA" control={<Radio />} label="NA" />
                                            </RadioGroup>
                                        )}
                                        name={"outputFiles["+index+"].includeMatrix"}
                                        control={control}
                                    />
                                    <FormHelperText>{errors.outputFiles && errors.outputFiles[index].includeMatrix ? errors.outputFiles[index].includeMatrix.message : ""}</FormHelperText>
                                </FormControl>
                            </Grid>
                            {watchFields["outputFiles["+index+"].includeMatrix"]==="Yes" &&
                                <>
                                    <Grid item xs={12}>
                                        <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].continuousVariables ? true : false} required>
                                            <FormLabel component="legend">Does the matrix include continuous variables?</FormLabel>
                                            <Controller
                                                render={({onBlur, onChange, value})=> (
                                                    <RadioGroup id="continuousVariables" onChange={(e)=> handleMandatoryChange(e, "Yes", ["unweighted sample size"], onChange)} value={value} row>
                                                        <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                        <FormControlLabel value="No" control={<Radio />} label="No" />
                                                    </RadioGroup>
                                                )}
                                                name={"outputFiles["+index+"].continuousVariables"}
                                                control={control}
                                                rules={{required:requiredErrorMessage}}
                                            />
                                            <FormHelperText>{errors.outputFiles && errors.outputFiles[index].continuousVariables ? errors.outputFiles[index].continuousVariables.message : ""}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].dichotomousVariables ? true : false} required>
                                            <FormLabel component="legend">Does the matrix inclue dichotomous variables?</FormLabel>
                                            <Controller
                                                render={({onBlur, onChange, value})=> (
                                                    <RadioGroup id="dichotomousVariables" onChange={(e)=> handleMandatoryChange(e, "Yes", ["unweighted cross-tabulation table"], onChange)} value={value} row>
                                                        <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                        <FormControlLabel value="No" control={<Radio />} label="No" />
                                                    </RadioGroup>
                                                )}
                                                name={"outputFiles["+index+"].dichotomousVariables"}
                                                control={control}
                                                rules={{required:requiredErrorMessage}}
                                            />
                                            <FormHelperText>{errors.outputFiles && errors.outputFiles[index].dichotomousVariables ? errors.outputFiles[index].dichotomousVariables.message : ""}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].correlatedVariables ? true : false} required>
                                            <FormLabel component="legend">Does the matrix include a dichotomous variable correlated with a continuous variable</FormLabel>
                                            <Controller
                                                render={({onBlur, onChange, value})=> (
                                                    <RadioGroup id="correlatedVariables" onChange={(e)=> handleMandatoryChange(e, "Yes", ["unweighted sub-totals for the categories of the dichotomous variable correlated with a continuous variable"], onChange)} value={value} row>
                                                        <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                        <FormControlLabel value="No" control={<Radio />} label="No" />
                                                    </RadioGroup>
                                                )}
                                                name={"outputFiles["+index+"].correlatedVariables"}
                                                control={control}
                                                rules={{required:requiredErrorMessage}}
                                            />
                                            <FormHelperText>{errors.outputFiles && errors.outputFiles[index].correlatedVariables ? errors.outputFiles[index].correlatedVariables.message : ""}</FormHelperText>
                                        </FormControl>
                                    </Grid>
                                </>
                            }
                        </Grid>
                        <Grid container item xs={6} spacing={1}>
                            <Grid item xs={12}>
                                <FormControl component="fieldset" error={errors.outputFiles && errors.outputFiles[index].roundingOutput ? true : false}>
                                    <FormLabel component="legend">
                                        Is rounding of output required for this vetting request?
                                        <Tooltip title="If yes, ensure that any forced rounding to zero is shown." arrow>
                                            <InfoIcon />
                                        </Tooltip>
                                    </FormLabel>
                                    <Controller
                                        render={({onBlur, onChange, value})=> (
                                            <RadioGroup id="roundingOutput" onChange={(e)=> handleMandatoryChange(e, "Yes", ["unrounded version of this output"], onChange)} value={value} row>
                                                <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                <FormControlLabel value="No" control={<Radio />} label="No" />
                                                <FormControlLabel value="NA" control={<Radio />} label="NA" />
                                            </RadioGroup>
                                        )}
                                        name={"outputFiles["+index+"].roundingOutput"}
                                        control={control}
                                    />
                                    <FormHelperText>{errors.outputFiles && errors.outputFiles[index].roundingOutput ? errors.outputFiles[index].roundingOutput.message : ""}</FormHelperText>
                                </FormControl>
                            </Grid>
                            {watchFields["outputFiles["+index+"].roundingOutput"]==="Yes" &&
                                <Grid item xs={12}>
                                    <TextField
                                        id="roundingOutputDescription"
                                        name={"outputFiles["+index+"].roundingOutputDescription"}
                                        label="Describe the approach to rounding and rounding base"
                                        variant="outlined"
                                        inputRef={register({required:requiredErrorMessage})}
                                        error={errors.outputFiles && errors.outputFiles[index].roundingOutputDescription ? true : false}
                                        helperText={errors.outputFiles && errors.outputFiles[index].roundingOutputDescription ? errors.outputFiles[index].roundingOutputDescription.message : ""}
                                        required
                                        fullWidth
                                        multiline
                                    />
                                </Grid>
                            }
                        </Grid>
                        <Grid item xs={12}>
                            <RequestSupportingFilesList control={control} errors={errors} mandatories={mandatories} files={files} index={index}/>
                        </Grid>
                    </Grid>
                </fieldset>
            </Grid>
            <Grid item xs={6}>
                <ActionBar />
            </Grid>
        </Grid>
    );
}