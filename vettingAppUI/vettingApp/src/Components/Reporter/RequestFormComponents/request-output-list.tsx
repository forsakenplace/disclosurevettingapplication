import React, { useState } from 'react';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';

import { useStateMachine } from 'little-state-machine';
import { useFormContext } from 'react-hook-form';
import {updateRequestFormData} from '../../../Actions/actions';
import ActionBar from './action-bar';
import outputFileDefaults from '../../../StateObjects/outputFileDefaults.json';

import {makeStyles} from '@material-ui/styles';


const useStyles = makeStyles((theme) => ({
    buttonMargin: {
        marginBottom:'1em'
    }
  }));

export default function RequestOutputList({handleEditOutputFile, outputFiles, setOutputFiles}) {
    const classes = useStyles();
    const { state, action } = useStateMachine(updateRequestFormData);
    const {errors, clearErrors} = useFormContext();

    const addOutputFile = ()=> {
        let defaults = {...outputFileDefaults};
        defaults["id"] = outputFiles.length+1;
        const newOutputList = outputFiles.concat(defaults);
        setOutputFiles(newOutputList);
        state.requestFormData["outputFiles"] = newOutputList;
        action(state.requestFormData);

        clearErrors("outputFiles");
    };

    const deleteOutputFile = (index)=>{
        const newOutputList = outputFiles.slice(0, index).concat(outputFiles.slice(index+1));
        for(let i = 0; i<newOutputList.length; i++){
            newOutputList[i].id = i+1;
        }
        setOutputFiles(newOutputList);
        state.requestFormData["outputFiles"] = newOutputList;
        action(state.requestFormData);
    };
    
    return (
        <Grid container spacing={3}>
            <Grid item xs={6}>
                <Typography variant="h2" style={{fontSize:'1.5em',marginBottom:'1em'}}>Output file list</Typography>
                <Button onClick={addOutputFile} variant="contained" className={classes.buttonMargin}>Add Output File</Button>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>File Name</TableCell>
                                <TableCell />
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {outputFiles.map((item, index)=> (
                                    <TableRow key={index}>
                                        <TableCell>
                                            {"Output file #" + item.id}
                                        </TableCell>
                                        <TableCell>
                                            {item.outputFile}
                                        </TableCell>
                                        <TableCell>
                                            <Button type="button" variant="contained" onClick={()=> handleEditOutputFile(item.id)}>Modify</Button>&nbsp;
                                            <Button type="button" variant="contained" onClick={()=> deleteOutputFile(index)}>Delete</Button>
                                        </TableCell>
                                    </TableRow>
                                ))
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
                <FormHelperText error={!!errors.outputFiles}>{errors.outputFiles?.message}</FormHelperText>
            </Grid>
            <Grid item xs={6}>
                <ActionBar />
            </Grid>
        </Grid>
    );
}