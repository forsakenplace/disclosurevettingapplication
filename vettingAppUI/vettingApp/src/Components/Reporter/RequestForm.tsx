import React,{useState, useEffect} from 'react';
import {Container, Typography,Grid,Paper, Tabs, Tab} from '@material-ui/core';
import { useTranslation } from "react-i18next";
import DefaultHeader from '../CommonComponents/DefaultHeader';
import FormErrorsAlert from '../CommonComponents/FormErrorsAlert';
import {useStateMachine} from 'little-state-machine';
import {CalendarToday,Edit} from '@material-ui/icons';
import {makeStyles} from '@material-ui/styles';

import { format } from 'date-fns';
import {updateRequestFormData} from '../../Actions/actions';
import {FormProvider, useForm} from 'react-hook-form';
import RequestHeader from './RequestFormComponents/request-header';
import RequestOutputList from './RequestFormComponents/request-output-list';
import RequestOutputFile from './RequestFormComponents/request-output-file';
import RequestDisclosure from './RequestFormComponents/request-disclosure';
import RequestFooter from './RequestFormComponents/request-footer';
import outputFileDefaults from '../../StateObjects/outputFileDefaults.json';

const useStyles = makeStyles((theme) => ({
    header:{
        backgroundColor:'#f5f5f5',
        width:'100%',
        height:150,
        padding: theme.spacing(3, 0, 0, 3),
        borderBottom:'1px solid #dee2e6',
        flexGrow:1,
        boxSizing:'border-box',
      },
      requestStatusPaper:{
        boxShadow:'none',
        padding: theme.spacing(1, 2, 1, 2),
        maxWidth:200,
        float:'left'
      },
}))

export default function RequestForm(){
    const classes = useStyles();

    const { t } = useTranslation();
    document.title =t("requestFormTitle");

    const { state, action } = useStateMachine(updateRequestFormData);
    
    if(state.requestFormData.date === ""){
        state.requestFormData.date = format(new Date(), "yyyy-MM-dd");
    }
    const formMethods = useForm({defaultValues: state.requestFormData});

    const [tabValue, setTabValue] = useState(0);
    const handleTabChange = (e, newVal)=> {
        setTabValue(newVal);
    };
    const [tabsDisabled, setTabsDisabled] = useState(state.requestFormData.tabsDisabled);
    const [editOutputFile, setEditOutputFile] = useState(0);
    const defaultFilesList = state.requestFormData["outputFiles"] ? state.requestFormData["outputFiles"] : [];
    const [outputFiles, setOutputFiles] = useState(defaultFilesList);

    const onSubmit = (data)=> {
        if(tabsDisabled){
            data.tabsDisabled = false;
            action(data);
            setTabsDisabled(false);
            setTabValue(1);
        }else if(editOutputFile!==0){
            data.outputFiles[editOutputFile-1] = {...outputFileDefaults, ...data.outputFiles[editOutputFile-1]};
            data.outputFiles[editOutputFile-1].id = editOutputFile;
            setOutputFiles(data.outputFiles);
            action(data);
            setEditOutputFile(0);
        }else{
            if(outputFiles.length===0 && !formMethods.errors.outputFiles){
                formMethods.setError("outputFiles", {type:"required", message:"An output file is required"});
                return;
            }

            if(!data.otherDisclosureSupportingFiles){
                data.otherDisclosureSupportingFiles=[];
            }
            action(data);
            setTabValue(4);
        }
    };

    useEffect(()=> {
        formMethods.reset(state.requestFormData);
    }, [tabsDisabled, editOutputFile]);

    return (
        <React.Fragment>
            <DefaultHeader />
            <main tabIndex={-1}>
                <Container maxWidth="xl" className="page-container">
                   <Grid container direction="column" spacing={2}>
                        <Grid item>
                            <Paper className={classes.header}>
                            <Grid container direction="row">
                                <Grid item xs={6}>
                                    <Typography variant="h1">{t("requestFormTitle")}</Typography>
                                    <CalendarToday fontSize="small"/>
                                    <Typography variant="body2" component='span'>Submitted at 13:47 on Tuesday, August 11th 2020</Typography> 
                                </Grid>
                                <Grid item xs={6}>
                                    <Paper className={classes.requestStatusPaper} style={{backgroundColor:'#6b778c'}}>
                                        <Edit style={{ color: '#FFFFFF',marginTop:5}}/>
                                        <Typography component="span" style={{ color: '#FFFFFF',marginTop:-5}}>Work in progress</Typography>
                                    </Paper>
                                </Grid>
                            </Grid>
                            </Paper>
                        </Grid>
                        <Grid item>
                            <FormErrorsAlert errors={formMethods.errors}/>
                        </Grid>
                        <Grid item>
                           <Typography variant="h1">{t("requestFormTitle")}</Typography>
                            <Tabs value={tabValue} onChange={handleTabChange}>
                                <Tab label="Request Header" id="simple-tab-0" aria-controls="simple-tabpanel-0"/>
                                <Tab label="Request Output Files List" id="simple-tab-1" aria-controls="simple-tabpanel-1" disabled={tabsDisabled}/>
                                <Tab label="Request Dislcosure" id="simple-tab-2" aria-controls="simple-tabpanel-2" disabled={tabsDisabled}/>
                                <Tab label="Request Footer" id="simple-tab-3" aria-controls="simple-tabpanel-3" disabled={tabsDisabled}/>
                                <Tab label="Discussion" id="simple-tab-4" aria-controls="simple-tabpanel-4"/>
                            </Tabs>
                            <FormProvider {...formMethods}>
                                <form onSubmit={formMethods.handleSubmit(onSubmit)} noValidate>
                                    {tabsDisabled &&
                                        <div hidden={tabValue!==0}>
                                            <RequestHeader />
                                        </div>
                                    }
                                    {!tabsDisabled && editOutputFile===0 &&
                                        <>
                                        <div hidden={tabValue!==0}>
                                            <RequestHeader />
                                        </div>
                                        <div hidden={tabValue!==1}>
                                            <RequestOutputList handleEditOutputFile={(output)=> setEditOutputFile(output)} outputFiles={outputFiles} setOutputFiles={setOutputFiles}/>
                                        </div>
                                        <div hidden={tabValue!==2}>
                                            <RequestDisclosure />
                                        </div>
                                        <div hidden={tabValue!==3}>
                                            <RequestFooter />
                                        </div>
                                        </>
                                    }
                                    {!tabsDisabled && editOutputFile!==0 &&
                                        <div hidden={tabValue!==1}>
                                            <RequestOutputFile output={editOutputFile} />
                                        </div>
                                    }
                                </form>
                            </FormProvider>
                            <div hidden={tabValue!==4}>
                                <pre>{JSON.stringify(state.requestFormData, null ,2)}</pre>
                            </div>
                        </Grid>
                    </Grid>
                </Container>
            </main>
        </React.Fragment>
    );
}