import React,{useState,useEffect} from 'react'
import {
  Grid,
  Container,
  Paper,
  Select,
  MenuItem,
  Typography,
  InputLabel,
  FormControl,
  List,
  ListItem,
  AppBar,
  Tabs,
  Button,
Tab,
Box} from  '@material-ui/core'

import {makeStyles} from '@material-ui/styles';

import DefaultHeader from '../CommonComponents/DefaultHeader';

import { updateRequestList} from '../../Actions/actions';
import { useStateMachine } from 'little-state-machine';

import {RequestStatusId} from '../../ApplicationConfig/constants'

import RequestCard from './RequestCard';

import Link from '@material-ui/core/Link';
import {Link as RouterLink} from 'react-router-dom';
import { useParams} from "react-router";

import RequestDetails from './RequestDetails';

import { useSnackbar } from 'notistack';

import {Auth} from '../../Api/auth'


const useStyles = makeStyles((theme) => ({
    root: {
      boxShadow: 'none',
      borderRightWidth: '1px',
      borderRightStyle: 'solid',
      borderRightColor: theme.palette.divider,
      height: '100%',
      boxSizing: 'border-box',
      padding: theme.spacing(3, 2, 0, 0),
    },
    requestDetailZone:{
      boxShadow: 'none',
      borderRightWidth: '1px',
      borderRightStyle: 'solid',
      borderRightColor: theme.palette.divider,
    },
    requestIdenZone:{
      boxShadow: 'none',
      padding: theme.spacing(2, 0, 0, 2),
    },
    listRoot: {
      width: '100%'
    },
    header:{
      backgroundColor:'#f5f5f5',
      width:'100%',
      height:150,
      padding: theme.spacing(3, 0, 0, 3),
      borderBottom:'1px solid #dee2e6',
      flexGrow:1,
      boxSizing:'border-box',
    },
    requestStatusPaper:{
      boxShadow:'none',
      padding: theme.spacing(1, 2, 1, 2),
      maxWidth:200,
      float:'right'
    },
    filterTab:{
      width:'100%',
      height:100
    }
  }));

export default function RequestDashboard()
{
    const {
      action,
        state: {
          requestsInfo:{rows},
          userInfo,
          projectInfo:{name}
        },
      } = useStateMachine(updateRequestList);
    
      const [pageStatus,setPageStatus]=React.useState({
        dataLoaded:false,
        errorMessage:''
    });
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    useEffect(()=>{
      let key;
      if (!pageStatus.dataLoaded && pageStatus.errorMessage=='')
      {
          key=enqueueSnackbar('Loading data',{variant:'info', persist: true,preventDuplicate: true});
      }
      else if (pageStatus.dataLoaded){
          closeSnackbar(key);
          enqueueSnackbar('Succesfully loaded data',{variant:'success',autoHideDuration:2000,preventDuplicate: true});
      }

      if (!pageStatus.dataLoaded && pageStatus.errorMessage!='') {
          closeSnackbar(key);
          enqueueSnackbar(pageStatus.errorMessage,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
      }
  
  },[pageStatus]);

    /* useEffect(() => {
      if (rows.length==0){

        Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/getRequests/" + name)
            .then((response)=>{
              if (!response.ok) {
                setPageStatus({...pageStatus,dataLoaded:false,errorMessage:"An error occured while getting data. Please try again."})
              }
              return response;
            })
            .then(res => res.json())
            .then(
              (result) => {
                action({rows:result})
                setPageStatus({...pageStatus,dataLoaded:true})
      
              }
            )
      }
    }) */
    

    const classes = useStyles();
    const [requestStatusId,setRequestStatus]=useState(RequestStatusId.AwaitingReview)

    const onChangeRequestFilter=(event)=>{
      setRequestStatus(event.target.value);
      
    }

    let {lang}=useParams("lang");
    let {requestId}=useParams("requestId");

    return(
        <React.Fragment>
        <DefaultHeader />
            <main tabIndex={-1}>
                <Container maxWidth="xl" className="page-container">
                    <Grid container direction="row" alignItems="stretch">
                        <Grid item sm={3} lg={2}>
                        <Paper className={classes.root}>                           
                            <FormControl variant="outlined" style={{width:'100%'}}>
                              <InputLabel id="request-status-filter-label">Filter by request status</InputLabel>
                              <Select 
                              id="request-status-filter"
                              labelId="request-status-filter-label"
                              value={requestStatusId}
                              margin="dense"
                              style={{minWidth: 180}}
                              label="Filter by request status"
                              onChange={onChangeRequestFilter}
                              >
                             
                                <MenuItem value={RequestStatusId.AwaitingReview}>Available</MenuItem>
                                <MenuItem value={RequestStatusId.InReview}>In review</MenuItem>
                                
                              </Select>
                            </FormControl>
                            <List component="nav" className={classes.listRoot}>
                                {rows.filter(r=>(r.statusId==requestStatusId)).map((row)=>{

                                      return (<ListItem key={row.id}><RequestCard
                                        statusId={row.statusId} 
                                          reporter={row.requester}
                                          fileCount={row.fileOutputCount}
                                          updatedOn="3:30pm"
                                          requestId={row.id}                                     
                                          /></ListItem>)
                                  })}
                            </List>
                        </Paper>
                        </Grid>
                        <Grid item tabIndex={-1} sm={10} md={10} lg={10}>
                            <RequestDetails language={lang} currentUser={userInfo.id} requestId={requestId}/>

                        </Grid>
                    </Grid>
                </Container>
            </main>
        </React.Fragment>
    );
}

