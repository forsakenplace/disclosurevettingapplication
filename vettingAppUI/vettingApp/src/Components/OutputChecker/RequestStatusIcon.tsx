import React from 'react'
import CheckCircle from '@material-ui/icons/CheckCircle';
import AccessTime from '@material-ui/icons/AccessTime';
import Cancel from '@material-ui/icons/Cancel';
import Visibility from '@material-ui/icons/Visibility';
import Flag from '@material-ui/icons/Flag';
import Edit from '@material-ui/icons/Edit';

import {RequestStatusId} from '../../ApplicationConfig/constants'

export function RequestStatusIcon(props){

    var {statusId, ...other} = props;

    let iconToDisplay=<Edit {...other} />;

    if(props.statusId==RequestStatusId.AwaitingReview)
    {
      iconToDisplay= <AccessTime {...other} />;
    }
    else if (props.statusId==RequestStatusId.Canceled){
      iconToDisplay=  <Cancel  {...other} />;
    }
    else if (props.statusId==RequestStatusId.Approved){
      iconToDisplay=<CheckCircle {...other} />;
    }
    else if (props.statusId==RequestStatusId.InReview)
    {
      iconToDisplay=<Visibility {...other} />
    }
    else if (props.statusId==RequestStatusId.Denied)
    {
      iconToDisplay=<Flag {...other}/>
    }
       

    return(
        iconToDisplay
    )
}