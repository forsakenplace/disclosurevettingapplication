import React,{useEffect} from "react";
import {Grid,
    Paper,
    Link,
    Typography,
    Tabs,
    Tab,Button,Box
} from '@material-ui/core';

import update from 'react-addons-update';

import {makeStyles} from '@material-ui/styles';
import {Link as RouterLink} from 'react-router-dom';

import {RequestStatusId} from '../../ApplicationConfig/constants';

import { updateRequestList} from '../../Actions/actions';
import { useStateMachine } from 'little-state-machine';


import {
    CalendarToday,
    Note,
    InsertDriveFile,
    Description,
    Comment,
    AddCircle
  } from '@material-ui/icons';

import {RequestStatusIcon} from './RequestStatusIcon';
import { useSnackbar } from 'notistack';

import PropTypes from 'prop-types';

import {Auth} from '../../Api/auth'

const useStyles = makeStyles((theme) => ({
    root: {
      boxShadow: 'none',
      borderRightWidth: '1px',
      borderRightStyle: 'solid',
      borderRightColor: theme.palette.divider,
      height: '100%',
      boxSizing: 'border-box',
      padding: theme.spacing(3, 2, 0, 0),
    },
    requestDetailZone:{
      boxShadow: 'none',
      borderRightWidth: '1px',
      borderRightStyle: 'solid',
      borderRightColor: theme.palette.divider,
    },
    requestIdenZone:{
      boxShadow: 'none',
      padding: theme.spacing(2, 0, 0, 2),
    },
    listRoot: {
      width: '100%'
    },
    header:{
      backgroundColor:'#f5f5f5',
      width:'100%',
      height:150,
      padding: theme.spacing(3, 0, 0, 3),
      borderBottom:'1px solid #dee2e6',
      flexGrow:1,
      boxSizing:'border-box',
    },
    requestStatusPaper:{
      boxShadow:'none',
      padding: theme.spacing(1, 2, 1, 2),
      maxWidth:200,
      float:'right'
    },
    filterTab:{
      width:'100%',
      height:100
    }
  }));

export default function RequestDetails(props){

    const classes = useStyles();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const [requestData,setRequestData]=React.useState({statusId:0,
      requester:0,
      projectName:'',
      outputcheckers:[],
      submittedOnTime:'',
      submittedOnDateFormat:''});

    const [pageStatus,setPageStatus]=React.useState({
        dataLoaded:false,
        errorMessage:''
    });

    const [refreshRequest,setRefreshRequest]=React.useState(false);

    const {
      action,
      state: {
        requestsInfo:{rows},
      },
    } = useStateMachine(updateRequestList);

    const [value, setValue] = React.useState(0);

    const handleDetailChange = (event, newValue) => {
      setValue(newValue);
    };

    useEffect(()=>{
      let key;
      if (!pageStatus.dataLoaded && pageStatus.errorMessage=='')
      {
          key=enqueueSnackbar('Loading data',{variant:'info', persist: true,preventDuplicate: true});
      }
      else if (pageStatus.dataLoaded && pageStatus.errorMessage==''){
          closeSnackbar(key);
          enqueueSnackbar('Succesfully loaded data',{variant:'success',autoHideDuration:2000,preventDuplicate: true});
      }

      if (!pageStatus.dataLoaded && pageStatus.errorMessage!='') {
          closeSnackbar(key);
          enqueueSnackbar(pageStatus.errorMessage,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
      }
      else if (pageStatus.errorMessage!='') {
        enqueueSnackbar(pageStatus.errorMessage,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
      }
  
  },[pageStatus]);

    useEffect(() => {

      if (props.requestId==13)
      {
        setRequestData({'id':props.requestId,
        'statusId': RequestStatusId.AwaitingReview,
        'projectName': 'DAAAS-whatever',
        'statusDesc':'Awaiting review',
        'submittedOnDateFormat':'Jul 30th,2020',
        'submittedOnTime':'13:30:00',
        'submittedOn':'2020-07-30 13:30:00',
        'updatedOnFormat': 'Jul 31th,2020',
        'updatedOn':'2020-07-30 13:30:00',
        'requester':'marc-andre.menard@cloud.statcan.ca',
        'outputcheckers':[],
        'fileOutputCount':0,
        })
    }
    else if (props.requestId==45){
      setRequestData({'id':props.requestId,
      'statusId': RequestStatusId.InReview,
      'projectName': 'DAAAS-whatever',
      'statusDesc':'In review',
      'submittedOnDateFormat':'Jul 30th,2020',
      'submittedOnTime':'13:30:00',
      'submittedOn':'2020-07-30 13:30:00',
      'updatedOnFormat': 'Jul 31th,2020',
      'updatedOn':'2020-07-30 13:30:00',
      'requester':'marc-andre.menard@cloud.statcan.ca',
      'outputcheckers':[{'id':'outputChecker@oc.com'}],
      'fileOutputCount':0,
      })
    }

      setPageStatus({...pageStatus,dataLoaded:true})
      
      /*Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/getRequest/" + props.requestId)
        .then((response)=>{
          if (!response.ok) {
            setPageStatus({...pageStatus,dataLoaded:false,errorMessage:"An error occured while getting data. Please try again."})
          }
          return response;
        })
        .then(res => res.json())
        .then(
          (result) => {
            setRequestData(result.data)
            
              //update row in rows list
              const rowToUpdateIndex=rows.findIndex(el=>el.id==props.requestId)
              let newRowsList=[...rows]
              newRowsList[rowToUpdateIndex]={...newRowsList[rowToUpdateIndex],
                statusId:result.data.statusId,
                outputcheckers:result.data.outputcheckers}
              action({rows:newRowsList})
            
            setPageStatus({...pageStatus,dataLoaded:true})
          }
        )    */ 
    },[props.requestId,refreshRequest]);

    const handleUpdateStatus=(newStatusId)=>{

      Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/updateRequestStatus/" + props.requestId + "/" + newStatusId)
      .then(res => res.json())
      .then((response)=>{
        if (response.statusCode!=200)
        {
          setPageStatus({...pageStatus,errorMessage:response.message})
        }
        else{
          //trigger a refresh on the current vetting request
          setRefreshRequest(!refreshRequest);
        }
      })
      .catch(
        (error)=>{
          setPageStatus({...pageStatus,errorMessage:"An error occured while getting data. Please try again."})
        }
      )
    }

    const handleAssignRequest=(event)=>{

      setPageStatus({...pageStatus,dataLoaded:false})

      Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/assignRequest/" + props.requestId)
      .then(res => res.json())
      .then((response)=>{
        if (response.statusCode!=200)
        {
          setPageStatus({...pageStatus,errorMessage:response.message})
        }
        else{
          //trigger a refresh on the current vetting request
          setRefreshRequest(!refreshRequest);
        }
      })
      .catch(
        (error)=>{
          setPageStatus({...pageStatus,errorMessage:"An error occured while getting data. Please try again."})
        }
      )
    }

    let backColor;
    let statusDesc;
    if(requestData.statusId==RequestStatusId.AwaitingReview)
    {
        statusDesc="Awaiting review";
        backColor='#ffab00';
    }
    else if (requestData.statusId==RequestStatusId.Canceled){
        statusDesc="Canceled";
        backColor='#FF0000';
    }
    else if (requestData.statusId==RequestStatusId.Approved){
        statusDesc="Approved";
      backColor='#064';
    }
    else if (requestData.statusId==RequestStatusId.InReview)
    {
        statusDesc="In review";
        backColor='#ffab00';
    }
    else if (requestData.statusId==RequestStatusId.Denied)
    {
        statusDesc="Errors, needs revision";
        backColor='#bf2600';
    }
    else
    {
      statusDesc="Work in progress";
      backColor='#6b778c';
    }
    
    var requestAssignedToMe=requestData.outputcheckers.map((user)=>{return user.id}).indexOf(props.currentUser)!= -1

        return(<>
            {pageStatus.dataLoaded && (<Grid container direction="column">

        <Grid item>

        <Paper className={classes.header} component="header">

            <Grid container direction='row'>
            <Grid item xs={6}>
                <Link component={RouterLink} to={"/" + props.language + "/home"}>« Back to dashboard</Link>
                <Typography variant="h1">Request details</Typography>
                <CalendarToday fontSize="small"/> <Typography variant="body2" component='span'>Submitted at {requestData.submittedOnTime} on {requestData.submittedOnDateFormat}</Typography> 
            </Grid>
            <Grid item xs={4}>
                <Paper className={classes.requestStatusPaper} style={{backgroundColor:backColor}}>
                <RequestStatusIcon statusId={requestData.statusId} style={{ color: '#FFFFFF',marginTop:5}}/> <Typography component="span" style={{ color: '#FFFFFF',marginTop:-5}}>{statusDesc}</Typography>
                </Paper>
            </Grid>
            </Grid>

        </Paper>

        </Grid>

        <Grid item>

        <Grid container direction="row">
        <Grid item lg={10}>
            <Paper className={classes.requestDetailZone}>
                <Tabs value={value} onChange={handleDetailChange} aria-label="simple tabs example">
                <Tab icon={<InsertDriveFile />} label="Files" {...a11yProps(0)} />
                <Tab icon={<Description />} label="Residual disclosure" {...a11yProps(1)} />
                <Tab icon={<Note />} label="Comments" {...a11yProps(2)} />
                <Tab icon={<Comment />} label="Discussion" {...a11yProps(3)} />
                </Tabs>
            
                <TabPanel value={value} index={0}>
                Item One
                </TabPanel>
                <TabPanel value={value} index={1}>
                Item Two
                </TabPanel>
                <TabPanel value={value} index={2}>
                Item Three
                </TabPanel>
                <TabPanel value={value} index={3}>
                Item Four
                </TabPanel>
            </Paper>
            </Grid>
            <Grid item lg={2} >
            <Paper className={classes.requestIdenZone}>
                <Typography variant="h6">Requester</Typography>
                <p>{requestData.requester}</p>
                <Typography variant="h6">Project</Typography>
                <p>{requestData.projectName}</p>
                {requestData.outputcheckers.length>0 && 
                (<>
                <Typography variant="h6">Reviewers</Typography>
                {requestData.outputcheckers.map((user)=>(
                  <p>{user.id}</p>
                ))}
                </>)}
               
                {!requestAssignedToMe && (requestData.statusId==RequestStatusId.InReview || 
                requestData.statusId==RequestStatusId.AwaitingReview) &&  (<Button color="primary" onClick={handleAssignRequest} ><AddCircle style={{color:"#008000"}}/>&nbsp;Assign to me</Button>)}

                {requestAssignedToMe && requestData.statusId==RequestStatusId.InReview && (<>
                  <Typography variant="h6">Actions</Typography>     
                  <Button color="primary" onClick={()=>handleUpdateStatus(RequestStatusId.Approved)}><RequestStatusIcon statusId={RequestStatusId.Approved} style={{ color: '#008000'}}/>&nbsp;Approve request</Button>
                  <Button color="primary" onClick={()=>handleUpdateStatus(RequestStatusId.Denied)}><RequestStatusIcon statusId={RequestStatusId.Canceled} style={{ color: '#f00'}}/>&nbsp;Deny request</Button>
                  <Button color="primary" onClick={()=>handleUpdateStatus(RequestStatusId.WorkInProgress)}><RequestStatusIcon statusId={RequestStatusId.Denied} style={{ color: '#ffa500'}}/>&nbsp;Request revisions</Button>

                </>) }
              
                

            </Paper>                                  
            </Grid>
        </Grid>
        </Grid>
        </Grid>)}</>)
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }