import React from 'react'

import Edit from '@material-ui/icons/Edit';

import InsertDriveFile from '@material-ui/icons/InsertDriveFile';

import Person from '@material-ui/icons/Person';


import RadioButtonUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import CheckBox from '@material-ui/icons/CheckBox';
import EventNote from '@material-ui/icons/EventNote';

import Link from '@material-ui/core/Link';
import {Link as RouterLink} from 'react-router-dom';
import { useParams} from "react-router";

import { makeStyles } from '@material-ui/core/styles';
import {Paper, Grid, Typography} from  '@material-ui/core';

import {RequestStatusId} from '../../ApplicationConfig/constants'

const useStyles = makeStyles((theme) => ({
    
    request:{
        backgroundColor:'#FFFFFF',
        marginTop:'1em',
        marginBottom:'1em',
        padding:'3px'
    },
  }));


export default function RequestCard(props){
    const classes = useStyles();

    let {lang}=useParams("lang");

    var iconToDisplay=<Edit style={{ color: '#6b778c'}} />
    var statusDescIcon=<RadioButtonUnchecked fontSize="small" style={{color:'#6B778C'}} />
    var statusDesc=""

    if(props.statusId==RequestStatusId.AwaitingReview)
    {
    
      statusDescIcon=<RadioButtonUnchecked fontSize="small" style={{color:'#6B778C'}} />
      statusDesc="Unclaimed"
    }
    else if (props.statusId==RequestStatusId.Canceled){
     
      statusDescIcon=<CheckBox fontSize="small" style={{color:'#6B778C'}} />
      statusDesc="Assigned"
    }
    else if (props.statusId==RequestStatusId.Approved){
      
      statusDescIcon=<CheckBox fontSize="small" style={{color:'#6B778C'}} />
      statusDesc="Assigned"
    }
    else if (props.statusId==RequestStatusId.InReview)
    {
   
      statusDescIcon=<CheckBox fontSize="small" style={{color:'#6B778C'}} />
      statusDesc="Assigned"
    }
    else if (props.statusId==RequestStatusId.Denied)
    {
  
      statusDescIcon=<CheckBox fontSize="small" style={{color:'#6B778C'}} />
      statusDesc="Assigned"
    }   

    return(
        <Paper className={classes.request}>
            <Grid container direction="column" spacing={1}>
                <Grid item><Link to={"/" + lang + "/viewRequest/"+props.requestId} component={RouterLink}><Typography component="span" >Request</Typography></Link></Grid>
                <Grid item>
                    <Grid container>
                        <Grid item>
                            <Paper style={{width:'160',boxShadow:'none'}}>
                                <Person fontSize="small" style={{color:'#6B778C'}}/><Typography component="span" style={{fontSize:'12px',fontWeight:'bold',color:'#6B778C'}}>{props.reporter}</Typography>
                            </Paper>
                        </Grid>
                        <Grid item>
                            <InsertDriveFile fontSize="small" style={{color:'#6B778C'}} /><Typography component="span" style={{fontSize:'12px',fontWeight:'bold',color:'#6B778C'}}>{props.fileCount}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item>
                    <Grid container>
                        <Grid item>
                            <Paper style={{width:'160',boxShadow:'none'}}>
                            {statusDescIcon}<Typography component="span" style={{fontSize:'12px',fontWeight:'bold',color:'#6B778C'}}>{statusDesc}</Typography>
                            </Paper>
                        </Grid>
                        <Grid item>
                            <EventNote fontSize="small" style={{color:'#6B778C'}} /><Typography component="span" style={{fontSize:'12px',fontWeight:'bold',color:'#6B778C'}}>{props.updatedOn}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Paper>
    )
}