import React from 'react';
import {Link as RouterLink} from 'react-router-dom';
import {
  AppBar,
  Toolbar,
  Button,
} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import { useParams} from "react-router";

import Branding from './Branding';
import Language from './Language';
import ProjectBar from './ProjectBar';
import Logout from './Logout';

import {ControlPoint,Assessment,List} from '@material-ui/icons';

import {UserRole} from '../../ApplicationConfig/constants'

import { useTranslation } from "react-i18next";

import { getUserInfo, resetRequestFormData} from '../../Actions/actions';
import { useStateMachine } from 'little-state-machine';

export default function DefaultHeader(props) {
  let { lang } = useParams();
  const { t } = useTranslation();

  const {
    state: {
      userInfo: { role },
    },
  } = useStateMachine(getUserInfo);

  const classes = defaultStyles();


  const {action} = useStateMachine(resetRequestFormData);

  return (
    <React.Fragment>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <div className={classes.branding}>
            <Branding />
          </div>
          <div className={classes.search}>
            <ProjectBar />
          </div>
          <div className={classes.operations}>
          {role==UserRole.reporter && <>
            <Button component={RouterLink} to={"/" + lang + "/home"} color="primary">
                <List /> {t("requestListButton")}
            </Button>
            <Button component={RouterLink} to={"/" + lang + "/newrequest"} color="primary" onClick={action}>
                <ControlPoint /> {t("newRequestButton")}
            </Button></>
          }
            <Button component={RouterLink} color="primary" to={"/" + lang + "/reports"}>
              <Assessment /> {t("reportsButton")}
            </Button> 
          </div>

          <div className={classes.lang}>
            <Language />
          </div>
          <div className={classes.accountOptions}>
            <Logout />
          </div>
        </Toolbar>
      </AppBar>
    </React.Fragment>
  );
}

const defaultStyles = makeStyles((theme) => ({
  accountOptions: {
    [theme.breakpoints.down('sm')]: {
      order: 5,
    },
    [theme.breakpoints.down('xs')]: {
      order: 4,
    },
  },
  appBar: {
    'zIndex': 1200,
    'backgroundColor': theme.palette.common.white,
    'color': theme.palette.text.primary,
    '& .MuiToolbar-root': {
      display: 'flex',
      flexWrap: 'wrap',
    },
  },
  branding: {
    [theme.breakpoints.down('sm')]: {
      order: 1,
      flexGrow: 1,
      width: 'calc(100vw - 112px)',
    },
    '& img': {
      height: theme.spacing(3),
      display: 'block',
      [theme.breakpoints.down('sm')]: {
        height: theme.spacing(2.5),
        marginTop: theme.spacing(1),
      },
    },
  },
  operations: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.down('sm')]: {
      'order': 2,
      'textAlign': 'right',
      '& button': {
        paddingRight: 0,
      },
    },
  },
  lang: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.down('sm')]: {
      'order': 2,
      'textAlign': 'right',
      '& button': {
        paddingRight: 0,
      },
    },
  },
  navMenu: {
    [theme.breakpoints.down('sm')]: {
      order: 3,
    },
    [theme.breakpoints.down('xs')]: {
      flexGrow: 1,
    }
  },
  search: {
    'flexGrow': 1,
    'textAlign': 'center',
    [theme.breakpoints.down('sm')]: {
      order: 4,
    },
    [theme.breakpoints.down('xs')]: {
      order: 5,
      flexBasis: '100%',
    },
    '& > div': {
      width: '60%',
      display: 'inline-block',
      [theme.breakpoints.down('md')]: {
        width: '90%',
      },
      [theme.breakpoints.down('xs')]: {
        width: '100%',
      },
    },
  },
}));
