import React, {useState,useEffect,useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

import { updateProject} from '../../Actions/actions';
import { useStateMachine } from 'little-state-machine';


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    hidden:{
        visibility:'hidden'
    },
  }));

export default function ProjetBar()
{
    const {
        action,
        state: {
          projectInfo: { name },
          userInfo:{projects}
        },
      } = useStateMachine(updateProject);

    const classes = useStyles();

    const onProjectChange=(event)=>{
      action({name:event.target.value});
    }
    
    return (
        <div className={classes.root}>
          
          <FormControl variant="outlined" style={{width:'100%'}}>
            <InputLabel id="project-name-label">Filter by project</InputLabel>
            <Select id="project-name-select" 
            labelId="project-name-label" 
            onChange={onProjectChange}
            value={name}
            label="Filter by project"
            margin="dense">
                {projects.map((project)=>(
                  <MenuItem key={project.id} value={project.id}>{project.name}</MenuItem>
                ))}
            </Select>
            </FormControl>
        </div>
      );
}
