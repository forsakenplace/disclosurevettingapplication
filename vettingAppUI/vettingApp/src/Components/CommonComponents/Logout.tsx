import React from 'react';
import {Button} from '@material-ui/core';

import {Auth} from '../../Api/auth';

export default function Logout(){

    const logoutHandler=()=>{
        Auth.authorizedFetch(process.env.API_ROOT_URL + "/logout")
        .then(res => res.json())
        .then(
            (result) => {
                if (result.statusCode==200)
                {
                    Auth.logout();
                    window.location.href=result.redirectUrl;
                }
            }
        )
        .catch(

        )
    }

    return (
        <Button onClick={logoutHandler} variant="outlined" color="primary">
        Logout
      </Button>
    )
}