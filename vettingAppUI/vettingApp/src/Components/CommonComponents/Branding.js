import React from 'react';
import {makeStyles} from '@material-ui/styles';
import { useParams} from "react-router";
import { useTranslation } from "react-i18next";

export default function Branding() {
  const defaultStyles = makeStyles((theme) => ({
    brandImage: {
      margin: theme.spacing(1.5),
    },
  }));

  const classes = defaultStyles();
  let { lang } = useParams();
  const { t } = useTranslation();

  return (
    <React.Fragment>
      <a href={"https://www.canada.ca/" + lang + ".html"}>
        <img
          src={process.env.STATIC_URL + '/images/sig-blk-' + lang + '.svg'}
          alt=""
          className={classes.brandImage}
        />
        <span className="screen-reader-text">
          Government of Canada / <span lang={t("flipLang")}>Gouvernement du Canada</span>
        </span>
      </a>
    </React.Fragment>
  );
}
