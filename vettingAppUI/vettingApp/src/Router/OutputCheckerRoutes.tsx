import React,{useEffect,useState} from 'react';
import {Route,Switch,useRouteMatch} from 'react-router-dom';

import RequestDashboard from '../Components/OutputChecker/RequestDashboard';
import RequestsDashboard from '../Components/OutputChecker/RequestsDashboard';
import ReportsDashboard from '../Components/Reports/ReportsDashboard';
import RequestReport from '../Components/Reports/RequestReport';

import {Auth} from '../Api/auth';

export default function OutputCheckerRoutes(){
    let match = useRouteMatch();
    return (
       <Switch>
<Route exact path={match.path+"/home"}>
    <RequestsDashboard />
</Route>
<Route path={match.path+"/viewRequest/:requestId"} > 
    <RequestDashboard />
</Route>
<Route path={match.path+"/reports"}>     
                           <ReportsDashboard />
                        </Route>  
                        
                        <Route path={match.path+"/reports/:requestId"}>     
                           <RequestReport />
                        </Route>
</Switch>
    )
}