import React,{useEffect,useState} from 'react';
import {Switch,Route,Redirect} from 'react-router-dom';
import {CircularProgress} from '@material-ui/core';

import Logout from '../Components/CommonComponents/Logout';
import OutputCheckerRoutes from './OutputCheckerRoutes';
import ReporterRoutes from './ReporterRoutes';

import Localization from '../HoC/Localization';

import { updateUserInfo, updateProject } from '../Actions/actions';
import { useStateMachine } from 'little-state-machine';
import { UserRole } from '../ApplicationConfig/constants';

import {Auth} from '../Api/auth'

export default function InitializeRoutes(){

    const [pageStatus,setPageStatus]=useState({
        dataLoaded:true,
        errorMessage:'',
        component:null
      });

    const {
      actions,
      state: {
        userInfo: { id,fullName,role, projects },
        projectInfo:{name}
      },
    } = useStateMachine({updateUserInfo,updateProject});

    //setPageStatus({...pageStatus,dataLoaded:true});

   
    return(
        <Switch>
            <Route exact path="/">
                <Redirect from='/' to='/en/home' /> 
            </Route>
            <Route path="/:lang(en|fr)?">
                {pageStatus.dataLoaded && 
                <Localization>
                    {(role==UserRole.outputchecker && <OutputCheckerRoutes />) || (role==UserRole.reporter && <ReporterRoutes />)}
                </Localization>}

                
                {!pageStatus.dataLoaded && pageStatus.errorMessage.length==0 &&
                <div style={{height:'100%',display: 'flex',alignItems:'center',justifyContent:'center'}}>
                <CircularProgress />
                </div>}
                
                {pageStatus.errorMessage.length>0 && 
                <div style={{height:'100%',display: 'flex',alignItems:'center',justifyContent:'center', flexDirection:'column'}}>
                  {pageStatus.errorMessage}
                 
                  {pageStatus.component}
                 
                </div>}

            </Route>

        </Switch>
    )
}