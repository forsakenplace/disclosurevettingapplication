import React,{useEffect,useState} from 'react';
import {Route,Switch,useRouteMatch} from 'react-router-dom';

import RequestForm from '../Components/Reporter/RequestForm';
import RequestsList from '../Components/Reporter/RequestsList';

import ReportsDashboard from '../Components/Reports/ReportsDashboard';
import RequestReport from '../Components/Reports/RequestReport';

import {Auth} from '../Api/auth';

export default function ReporterRoutes(){
    let match = useRouteMatch();
    return (
       <Switch>
           <Route exact path={match.path+"/home"}>
                <RequestsList />
            </Route>
            <Route exact path={match.path+"/newrequest"}>
                <RequestForm />
            </Route>
            <Route path={match.path+"/reports"}>     
                <ReportsDashboard />
            </Route>
            <Route path={match.path+"/reports/:requestId"}>     
                <RequestReport />
            </Route> 
        </Switch>

    )
}