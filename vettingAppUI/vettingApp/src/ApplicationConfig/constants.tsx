export enum RequestStatusId {
    WorkInProgress = 2,
    Canceled=3,
    InReview=5,
    AwaitingReview=1,
    Approved=4,
    Denied=6
}

export enum RequestActionId{
    Submit=1,
    Cancel=2,
    Edit=3,
    Withdraw=4,
    Delete=5,
    Duplicate=6
}

export enum UserRole{
    reporter=1,
    outputchecker=2,
    admin=3
}