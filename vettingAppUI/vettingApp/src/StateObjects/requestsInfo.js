
import {RequestStatusId} from '../ApplicationConfig/constants'

function createData(id,statusId,statusDesc, submittedOn,submittedOnFormat, updatedOn, updatedOnFormat,requester,outputcheckers, outputchecker,fileOutputCount,operations) {
    return { id, statusId, statusDesc,submittedOn, submittedOnFormat, updatedOn, updatedOnFormat, requester,outputcheckers, outputchecker,fileOutputCount,operations };
  }

export default {
    rows: [
  createData(13,RequestStatusId.AwaitingReview,'Awaiting review','2019-07-22','Jul 22th,2020', '2019-07-30','Jul 30th,2020', 'tmph25@gmail.com',[],'', '1',''),
  createData(23,RequestStatusId.WorkInProgress,'Work in progress', '2019-07-21','Jul 21th,2020', '2019-07-24','Jul 29th,2020', 'marc-andre.menard@cloud.statcan.ca',[], '', '2',''),
  createData(45,RequestStatusId.InReview,'In review', '2019-06-21','Jun 21th,2020', '2019-08-24','Aug 24th,2020', 'tmph25@gmail.com', ['johndoe@canada.ca','johndoe2@canada.ca','outputChecker@oc.com'],'outputChecker@oc.com', '5',''),
  createData(52,RequestStatusId.Approved,'Approved', '2019-07-21','Jul 21th,2020', '2019-07-24','Jul 24th,2020', 'tmph25@gmail.com',['outputChecker@oc.com'], 'outputChecker@oc.com', '4',''),
  createData(55,RequestStatusId.Canceled,'Cancel', '2019-05-21','May 21th,2020', '2019-05-24','May 24th,2020', 'tmph25@gmail.com',['outputChecker@oc.com'], 'outputChecker@oc.com', '5',''),
  createData(56,RequestStatusId.Denied,'Errors, need revision', '2019-04-21','April 21th,2020', '2019-04-26','April 26th,2020', 'tmph25@gmail.com', ['outputChecker@oc.com'], 'outputChecker@oc.com', '5',''),
  createData(57,RequestStatusId.WorkInProgress,'Work in progress', '2019-08-21','August 21th,2020', '2019-04-26','August 26th,2020', 'marc-andre.menard@cloud.statcan.ca', ['outputChecker@oc.com'], 'outputChecker@oc.com', '5','')
    ]
  };
  