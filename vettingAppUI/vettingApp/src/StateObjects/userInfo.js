import {UserRole} from '../ApplicationConfig/constants'

export default {
    id: 'marc-andre.menard@cloud.statcan.ca',
    fullName:'Marc-Andre Menard',
    role: UserRole.reporter,
    projects:[{'id':1,'name': "DAAAS-1234"},{'id':2,'name': "DAAAS-65345"},{'id':3,'name': "DAAAS-33234"}]
  };


  /* export default {
    id: 'outputChecker@oc.com',
    fullName:'Lucile Austero',
    role: UserRole.outputchecker,
    projects:[{'id':1,'name': "DAAAS-1234"},{'id':2,'name': "DAAAS-65345"},{'id':3,'name': "DAAAS-33234"}]
  }; */