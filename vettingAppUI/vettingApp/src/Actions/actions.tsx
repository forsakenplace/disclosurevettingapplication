import requestFormDefaults from '../StateObjects/requestFormDefaults.json';

//Get actions

export function getProjectInfo(state) {
  return {
    projectInfo: {
      ...state.projectInfo
    },
  };
}

export function getUserInfo(state) {
  return {
    userInfo: {
      ...state.userInfo
    },
  };
}

export function getRequestListRows(state) {
  return {
    requestsInfo: {
      ...state.requestsInfo
    },
  };
}

//update actions

export function updateRequestList(state, payload) {
  return {
    ...state,
    requestsInfo: {
      ...state.requestsInfo,
      ...payload,
    },
  };
}

export function updateProject(state, payload) {
  return {
    ...state,
    projectInfo: {
      ...state.projectInfo,
      ...payload,
    },
  };
}

export function updateOcRequestFilter(state, payload) {
  return {
    ...state,
    ocRequestFilter: {
      ...state.ocRequestFilter,
      ...payload,
    },
  };
}

export function updateUserInfo(state, payload) {
  return {
    ...state,
    userInfo: {
      ...state.userInfo,
      ...payload,
    },
  };
}

export function updateRequestFormData(state, payload){
  return {
      ...state,
      requestFormData : {
          ...state.requestFormData,
          ...payload
      }
  };
}

//reset actions

export function resetRequestFormData(state){
    return {
        ...state,
        requestFormData : requestFormDefaults
    };
}