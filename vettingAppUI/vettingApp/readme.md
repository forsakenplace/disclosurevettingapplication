## Start applicaton
Do the following:
1. Access vettingAppUI/vettingApp folder
2. Run npm start. The app should be available on http://localhost:1234 

## Boot in Reporter (researcher) mode
1. Open vettingAppUI/vettingApp/src/StateObjects/userInfo.js
2. You will see two exports. First one will boot application in reporter mode (default)
3. If you already had the application running in your browser, close it and reopen the app in a new tab.

## Boot in Output checker (analyst)  mode
1. Open vettingAppUI/vettingApp/src/StateObjects/userInfo.js
2. You will see two exports. Commen the first export. Uncomment the second one.
3. If you already had the application running in your browser, close it and reopen the app in a new tab.

You can click on either requests in "awaiting review" or "In review" mode to see the UI you would get afterwards. 

## Access the vetting form
1. Boot the app in Reporter mode
2. Click on the New Request button

You have to fill out the first section (request header) before being able to continue. To continue, click on Next section. Afterwards, you should be able to navigate through all tabs. 

Most of fields for request header will be prefilled. We hope that only "Output folder name", "Supporting files folder" and questions(radio button) afterwards will be the only fields to answer.

Error messages will be triggered when you click Submit. It gets all errors accross all tabs and display them in a common list. It is buggy at this stage.

Discussions tab should appear only after a request has been assigned to an analyst.


## Notes
This project was put together quickly so UX can see what we have done so far. It is subject to change and wont be maintained over time. Our plan is to have our application running on the CLOUD infrastructure.