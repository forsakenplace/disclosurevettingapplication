from setuptools import setup, find_packages

setup(
    name="disclosureVetting",
    packages=find_packages(),
    install_requires=[
        'flask'
    ],
    entry_points={
        'console_scripts':[
            "disclosureVetting=app.cli:cli"
        ]
    }
)