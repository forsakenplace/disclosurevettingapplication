#!/bin/bash
set -e

while !</dev/tcp/db/5432; do
    echo "Waiting for POSTGRES"
    sleep 1
done

python setup.py develop

disclosureVetting db initdb

disclosureVetting run --host=0.0.0.0