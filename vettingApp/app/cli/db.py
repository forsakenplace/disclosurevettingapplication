import click
from flask.cli import AppGroup
from app import db
from app.models import Request, UserRole, RequestReviewer, Project, RequestStatusCodeset,RoleCodeset,UserProject, History
from app.api.constants import RequestStatus
import datetime

db_cli = AppGroup('db', help='Manage database initialization')

@db_cli.command(name='initdb')
def initialize_db():
    click.echo("Initializing the database...")

    db.drop_all()
    #creates tables in the database
    db.create_all()
   
    
    click.echo("Adding request status codeset...")
    db.session.add(RequestStatusCodeset(id=4,name_en="Approved",name_fr="Approuve"))
    db.session.add(RequestStatusCodeset(id=7,name_en="Draft",name_fr="Brouillon"))
    db.session.add(RequestStatusCodeset(id=2,name_en="Work in progress",name_fr="Travail en cours"))
    db.session.add(RequestStatusCodeset(id=1,name_en="Awaiting review",name_fr="En attente"))
    db.session.add(RequestStatusCodeset(id=3,name_en="Canceled",name_fr="Annule"))
    db.session.add(RequestStatusCodeset(id=6,name_en="Errors, needs revision",name_fr="Erreurs, besoin de revision"))
    db.session.add(RequestStatusCodeset(id=5,name_en="In review",name_fr="Revu en cours"))

    db.session.commit()

    db.session.add(RoleCodeset(id=1,name="reporter"))
    db.session.add(RoleCodeset(id=2,name="outputchecker"))
    db.session.add(RoleCodeset(id=3,name="admin"))


    db.session.add(Project(id=1,name="DAaa-11-421"))
    db.session.add(Project(id=2, name="3PO-3123"))
    db.session.add(Project(id=3, name="LV-426"))
    db.session.add(Project(id=4, name="ZX3-te6"))
    db.session.add(Project(id=5, name="PX3-teda6"))

    db.session.commit()

    db.session.add(UserProject(user_id="menmarc@mamvetting.onmicrosoft.com", project_id=1))
    db.session.add(UserProject(user_id="menmarc@mamvetting.onmicrosoft.com", project_id=2))
    db.session.add(UserProject(user_id="menmarc@mamvetting.onmicrosoft.com", project_id=3))

    db.session.add(UserProject(user_id="joDo@mamvetting.onmicrosoft.com", project_id=1))
    db.session.add(UserProject(user_id="joDo@mamvetting.onmicrosoft.com", project_id=2))
    db.session.add(UserProject(user_id="joDo@mamvetting.onmicrosoft.com", project_id=3))

    db.session.add(UserRole(user_id="menmarc@mamvetting.onmicrosoft.com",role_id=1))
    db.session.add(UserRole(user_id="joDo@mamvetting.onmicrosoft.com",role_id=2))
    db.session.add(UserRole(user_id="lucile@mamvetting.onmicrosoft.com",role_id=2))

    request=Request(requester_id="menmarc@mamvetting.onmicrosoft.com",status_id=RequestStatus.Draft,project_id=1)
    db.session.add(request)
    db.session.commit()

    history=History(status_id=RequestStatus.Draft,request_id=request.id,user_id=request.requester_id)
    db.session.add(history)
    db.session.commit()

    request=Request(requester_id="menmarc@mamvetting.onmicrosoft.com",status_id=RequestStatus.WorkInProgress,project_id=1)
    db.session.add(request)
    db.session.commit()

    history=History(status_id=RequestStatus.Draft,request_id=request.id,user_id=request.requester_id)
    db.session.add(history)

    history=History(status_id=RequestStatus.WorkInProgress,request_id=request.id,user_id=request.requester_id)
    db.session.add(history)

    db.session.commit()


    request=Request(requester_id="menmarc@mamvetting.onmicrosoft.com",
    status_id=RequestStatus.AwaitingReview,
    project_id=1,
    date_submitted=datetime.datetime(2020, 5, 17),
    date_updated=datetime.datetime(2020, 5, 18))

    db.session.add(request)
    db.session.commit()

    history=History(status_id=RequestStatus.Draft,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 5, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.WorkInProgress,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 5, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.AwaitingReview,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 5, 18))
    db.session.add(history)

    db.session.commit()

    requestReviewer=RequestReviewer(outputchecker_id="joDo@mamvetting.onmicrosoft.com")

    request=Request(requester_id="menmarc@mamvetting.onmicrosoft.com",
    status_id=RequestStatus.Canceled,
    project_id=1,
    date_submitted=datetime.datetime(2020, 4, 16),
    date_updated=datetime.datetime(2020, 5, 19))
    request.outputcheckers.append(requestReviewer)

    db.session.add(request)
    db.session.commit()

    history=History(status_id=RequestStatus.Draft,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 4, 16))
    db.session.add(history)

    history=History(status_id=RequestStatus.WorkInProgress,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 4, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.AwaitingReview,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 4, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.InReview,request_id=request.id,user_id=requestReviewer.outputchecker_id,date_created=datetime.datetime(2020, 4, 18))
    db.session.add(history)

    history=History(status_id=RequestStatus.Canceled,request_id=request.id,user_id=requestReviewer.outputchecker_id,date_created=datetime.datetime(2020, 4, 19))
    db.session.add(history)
    db.session.commit()

    requestReviewer=RequestReviewer(outputchecker_id="joDo@mamvetting.onmicrosoft.com")

    request=Request(requester_id="mathm@mamvetting.onmicrosoft.com",
    status_id=RequestStatus.Canceled,
    project_id=1,
    date_submitted=datetime.datetime(2020, 4, 17),
    date_updated=datetime.datetime(2020, 5, 19))
    request.outputcheckers.append(requestReviewer)

    db.session.add(request)
    db.session.commit()

    history=History(status_id=RequestStatus.Draft,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 4, 16))
    db.session.add(history)

    history=History(status_id=RequestStatus.WorkInProgress,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 4, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.AwaitingReview,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 4, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.InReview,request_id=request.id,user_id=requestReviewer.outputchecker_id,date_created=datetime.datetime(2020, 5, 18))
    db.session.add(history)

    history=History(status_id=RequestStatus.Canceled,request_id=request.id,user_id=requestReviewer.outputchecker_id,date_created=datetime.datetime(2020, 5, 19))
    db.session.add(history)
    db.session.commit()

    requestReviewer=RequestReviewer(outputchecker_id="joDo@mamvetting.onmicrosoft.com")
    request=Request(requester_id="menmarc@mamvetting.onmicrosoft.com",
    status_id=RequestStatus.InReview,
    project_id=1,
    date_submitted=datetime.datetime(2020, 6, 16),
    date_updated=datetime.datetime(2020, 6, 18))
    request.outputcheckers.append(requestReviewer)

    db.session.add(request)
    db.session.commit()

    history=History(status_id=RequestStatus.Draft,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 6, 16))
    db.session.add(history)

    history=History(status_id=RequestStatus.WorkInProgress,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 6, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.AwaitingReview,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 6, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.InReview,request_id=request.id,user_id=requestReviewer.outputchecker_id,date_created=datetime.datetime(2020, 6, 18))
    db.session.add(history)
    db.session.commit()

    requestReviewer=RequestReviewer(outputchecker_id="joDo@mamvetting.onmicrosoft.com")
    request=Request(requester_id="menmarc@mamvetting.onmicrosoft.com",
    status_id=RequestStatus.Approved,
    project_id=1,
    date_submitted=datetime.datetime(2020, 6, 17),
    date_updated=datetime.datetime(2020, 6, 20))
    request.outputcheckers.append(requestReviewer)

    db.session.add(request)
    db.session.commit()

    history=History(status_id=RequestStatus.Draft,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 6, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.WorkInProgress,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 6, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.AwaitingReview,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 6, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.InReview,request_id=request.id,user_id=requestReviewer.outputchecker_id,date_created=datetime.datetime(2020, 6, 20))
    db.session.add(history)

    history=History(status_id=RequestStatus.Approved,request_id=request.id,user_id=requestReviewer.outputchecker_id,date_created=datetime.datetime(2020, 6, 20))
    db.session.add(history)
    db.session.commit()

    requestReviewer=RequestReviewer(outputchecker_id="lucile@mamvetting.onmicrosoft.com")
    request=Request(requester_id="menmarc@mamvetting.onmicrosoft.com",
    status_id=RequestStatus.Denied,
    project_id=1,
    date_submitted=datetime.datetime(2020, 6, 17),
    date_updated=datetime.datetime(2020, 6, 20))
    request.outputcheckers.append(requestReviewer)

    db.session.add(request)
    db.session.commit()

    history=History(status_id=RequestStatus.Draft,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 6, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.WorkInProgress,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 6, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.AwaitingReview,request_id=request.id,user_id=request.requester_id,date_created=datetime.datetime(2020, 6, 17))
    db.session.add(history)

    history=History(status_id=RequestStatus.InReview,request_id=request.id,user_id=requestReviewer.outputchecker_id,date_created=datetime.datetime(2020, 6, 20))
    db.session.add(history)

    history=History(status_id=RequestStatus.Denied,request_id=request.id,user_id=requestReviewer.outputchecker_id,date_created=datetime.datetime(2020, 6, 20))
    db.session.add(history)
    db.session.commit()
 

    click.echo("The database is ready")
