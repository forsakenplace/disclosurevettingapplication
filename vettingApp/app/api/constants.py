from enum import IntEnum,Enum

class RequestStatus(IntEnum):
    WorkInProgress = 2,
    Canceled=3,
    InReview=5,
    AwaitingReview=1,
    Approved=4,
    Denied=6,
    Draft=7

class RequestAction(IntEnum):
    Submit=1,
    Cancel=2,
    Edit=3,
    Withdraw=4,
    Delete=5,
    Duplicate=6

class Role(IntEnum):
    Admin=3,
    Reporter=1,
    Outputchecker=2