from flask import Blueprint, jsonify,make_response,session, request
from sqlalchemy import desc
from sqlalchemy import and_
from app.models import Request,Project,UserRole,RequestReviewer,UserProject, History
from app import db
from app.api.constants import RequestAction,RequestStatus,Role
from app.api.config import Config
from app.api.filters import verify_session
import json

requests_view = Blueprint('requests', __name__)


@requests_view.route("/api/assignRequest/<int:id>")
@verify_session
def assignRequest(id):
    userId=str(session.get('user').get("family_name"))
    reqResponseCode=200
    reqMessage=''
    #get information on the user doing the update request
    role=UserRole.query.filter(UserRole.user_id==userId).first()

    #if you have permissions to update a request
    if role.role_id == Role.Outputchecker or role.role_id==Role.Admin:
        request = Request.query.get(id)

        if request.outputcheckers.count()>0 and request.outputcheckers.filter(RequestReviewer.outputchecker_id==userId).count()==1:
            reqResponseCode=400
            reqMessage='You are already assigned to this request.'
        else:
            if request.status_id==RequestStatus.AwaitingReview or request.status_id==RequestStatus.InReview:    
                try:
                    request.status_id=RequestStatus.InReview
                    request.outputcheckers.append(RequestReviewer(outputchecker_id=userId))

                    history=History(request_id=request.id,
                    status_id=request.status_id,user_id=userId)
                    db.session.add(history)
                    
                    db.session.commit()
                except:
                    reqResponseCode=500
                    reqMessage='An error occured while assigning request.'
            else:
                reqResponseCode=401
                reqMessage='You are not authorized to perform this action.'  
    else:
        reqResponseCode=401
        reqMessage='You are not authorized to perform this action.'

    reqResponse={'statusCode':reqResponseCode, "message":reqMessage}
    response=make_response(jsonify(reqResponse),reqResponseCode)
    return response


@requests_view.route("/api/updateRequestStatus/<int:id>/<int:newStatusId>")
@verify_session
def updateRequest(id,newStatusId):
  
    userId=str(session.get('user').get("family_name"))
    reqResponseCode=200
    reqMessage=''

    #get information on the user doing the update request
    role=UserRole.query.filter(UserRole.user_id==userId).first()

    #if you have permissions to update a request
    if role.role_id == Role.Outputchecker or role.role_id==Role.Admin:
        request = Request.query.get(id)
        # if reequest is currently binded to the logged outputchecker
        if request.outputcheckers.count()>0 and request.outputcheckers.filter(RequestReviewer.outputchecker_id==userId).count()==1:
            if request.status_id==RequestStatus.InReview:
                if newStatusId==RequestStatus.Approved or newStatusId==RequestStatus.WorkInProgress or newStatusId==RequestStatus.Denied:
                    try:
                        request.status_id=newStatusId

                        history=History(request_id=request.id,
                        status_id=newStatusId,user_id=userId)
                        db.session.add(history)

                        db.session.commit()
                    except:
                        reqResponseCode=500
                        reqMessage='An error occured while updating the request.'
                else:
                    reqResponseCode=401
            else:
                reqResponseCode=401
        else:
            reqResponseCode=401
    else:
        reqResponseCode=401

    if (reqResponseCode==401):
        reqMessage='You are not authorized to perform this action.'

    reqResponse={'statusCode':reqResponseCode, "message":reqMessage}
    response=make_response(jsonify(reqResponse),reqResponseCode)
    return response

@requests_view.route("/api/getRequest/<int:id>")
@verify_session
def getRequest(id):
    userId=str(session.get('user').get("family_name"))
    role=UserRole.query.filter(UserRole.user_id==userId).first()
    

    reqResponseCode=200
    requestData=''

    request = Request.query.get(id)
    if request:
        hasAccess=False
        if role.role_id == Role.Reporter:
            hasAccess=UserProject.query.filter(and_(UserProject.user_id==userId, UserProject.project_id==request.project_id)).count()>0
        elif role.role_id == Role.Outputchecker or role.role_id == Role.Admin:
            hasAccess=True

        if hasAccess:
            outputcheckers=[]
            if request.outputcheckers.count() > 0:
                outputcheckers=request.outputcheckers.order_by(desc(RequestReviewer.dateCreated))

            requestData={'id':request.id,
                'statusId':request.status_id,
                'projectName': request.project.name,
                'statusDesc':request.status.name_en,
                'submittedOnDateFormat':request.date_submitted.strftime("%d %b, %Y"),
                'submittedOnTime':request.date_submitted.strftime('%H:%M'),
                'submittedOn':request.date_submitted.strftime("%Y-%m-%d, %H:%M:%S"),
                'updatedOnFormat': request.date_updated.strftime("%d %b, %Y"),
                'updatedOn':request.date_updated.strftime("%Y-%m-%d, %H:%M:%S"),
                'requester':request.requester_id,
                'outputcheckers':[{'id':entry.outputchecker_id} for entry in outputcheckers],
                'fileOutputCount':request.file_output_count,
                'data': request.data
                }
        else:
            reqResponseCode=401
    else:
        reqResponseCode=404
    
    reqResponse={'statusCode':reqResponseCode, 'data':requestData}
    response=make_response(jsonify(reqResponse),reqResponseCode)
    return response


@requests_view.route("/api/getRequests/<int:projectId>")
@verify_session
def getRequests(projectId):
    userId=str(session.get('user').get("family_name"))
    role=UserRole.query.filter(UserRole.user_id==userId).first()

    #check role of user
    if role.role_id==Role.Reporter or (role.role_id==Role.Outputchecker and projectId!=0):
        requests = Request.query.filter(Request.project_id==projectId)
    elif role.role_id==Role.Outputchecker and projectId==0:
        requests = Request.query

    if requests:
        requests=requests.order_by(desc(Request.date_updated))

    requestsList =[]

    for request in requests:
        outputcheckers=[]
        outputchecker=''
        if request.outputcheckers.count() > 0:
            outputcheckers=request.outputcheckers.order_by(desc(RequestReviewer.dateCreated))
            outputchecker=outputcheckers.first().outputchecker_id

        requestsList.append({'id':request.id,
        'statusId':request.status_id,
        'statusDesc':request.status.name_en,
        'submittedOnFormat':request.date_submitted.strftime("%d %b, %Y"),
        'submittedOn':request.date_submitted.strftime("%Y-%m-%d, %H:%M:%S"),
        'updatedOnFormat': request.date_updated.strftime("%d %b, %Y"),
         'updatedOn':request.date_updated.strftime("%Y-%m-%d, %H:%M:%S"),
        'requester':request.requester_id,
        'outputcheckers':[{'id':entry.outputchecker_id} for entry in outputcheckers],
        'outputchecker': outputchecker,
        'fileOutputCount':request.file_output_count,
        })

    response=make_response(jsonify(requestsList))

    return response

@requests_view.route("/api/getUserInfo")
@verify_session
def getUserInfo():

    userId=str(session.get('user').get("family_name")) #it is set to his email...terrible
    userName=session.get('user').get("name")

    role=UserRole.query.filter(UserRole.user_id==userId)
    reqResponseCode=200
    reqMessage=''
    if role.count()==0:
        reqResponseCode=401
        reqMessage='User does not exist in database.'
        reqResponse={'statusCode':reqResponseCode,"message":reqMessage}
        response=make_response(jsonify(reqResponse))
        return response
    else:
        role=role.first()

    projectsList=[]

    if role.role_id==Role.Reporter:
        userProjects=UserProject.query.filter(UserProject.user_id==userId)
        projectsList= [{'id':userproject.project.id,'name':userproject.project.name} for userproject in userProjects]
    else:
        projects=Project.query.order_by(Project.name)
        projectsList= [{'id':project.id,'name':project.name} for project in projects]
    
    
    if role.role_id==Role.Outputchecker:
        projectsList.insert(0,{'id':0,'name':'All'})

    userInfo={'id':userId,
    'fullName':userName,
    'role': role.role_id,
    'projects': projectsList
    }

    reqResponse={'token': session.get('token_id'),'statusCode':reqResponseCode, "message":reqMessage,'data':userInfo}
    response=make_response(jsonify(reqResponse))
    return response

def validateNewStatus(statusId,newStatusId):

    if newStatusId==RequestAction.Submit:
        return statusId==RequestStatus.WorkInProgress

    elif newStatusId==RequestAction.Withdraw:
        return (statusId==RequestStatus.AwaitingReview or 
        statusId==RequestStatus.InReview)

    elif newStatusId==RequestAction.Duplicate:
        return (statusId==RequestStatus.AwaitingReview or 
        statusId==RequestStatus.InReview or
        statusId==RequestStatus.Canceled or
        statusId==RequestStatus.Denied or
        statusId==RequestStatus.Approved
        )

    elif newStatusId==RequestAction.Delete:
        return (statusId==RequestStatus.WorkInProgress or statusId==RequestStatus.Draft)

    elif newStatusId==RequestAction.Cancel:
        return (statusId==RequestStatus.AwaitingReview or 
        statusId==RequestStatus.InReview)
    
    return False

@requests_view.route("/api/updateRow/<language>/<int:requestId>/<int:newStatusId>")
@verify_session
def updateRowStatus(language,requestId,newStatusId):
    
    reqResponseCode=200
    reqMessage=''

    if language in Config.LANGUAGES:
    
        try:
            userId=str(session.get('user').get("family_name"))
            request=Request.query.filter(and_(Request.requester_id==userId, Request.id==requestId))
            #if request is associated with current logged user
            if request.count()==1:
                request=request.first()

                #if new status is allowed
                if validateNewStatus(request.status_id,newStatusId):
                    newRequestStatus=-1

                    if newStatusId==RequestAction.Submit:
                        if request.outputcheckers.count()>0:
                            newRequestStatus=RequestStatus.InReview
                        else:
                            newRequestStatus=RequestStatus.AwaitingReview

                    elif newStatusId==RequestAction.Withdraw:
                        newRequestStatus=RequestStatus.WorkInProgress

                    elif newStatusId==RequestAction.Cancel:
                        newRequestStatus=RequestStatus.Canceled

                    elif newStatusId==RequestAction.Duplicate:
                        try:
                            newRequest=Request(
                                requester_id=request.requester_id,
                                status_id=RequestStatus.WorkInProgress,
                                project_id=request.project_id,
                                data=request.data
                            )
                            db.session.add(newRequest)
                            db.session.commit()

                            history=History(request_id=newRequest.id,
                            status_id=RequestStatus.Draft,user_id=userId)

                            db.session.add(history)
                            db.session.commit()

                        except:
                            reqResponseCode=500
                            reqMessage="Error occurred while duplicating request."

                    elif newStatusId==RequestAction.Delete:
                        #extra step: cant delete if request has been previously reviewed
                        hasBeenReviewed=History.query.filter(and_(History.request_id==request.id, History.status_id==RequestStatus.InReview)).count()>0
                        if hasBeenReviewed:
                            reqResponseCode=401
                            reqMessage="Not authorized."
                        else:
                            try:
                                historyList=db.session.query(History).filter(History.request_id==request.id)
                                historyList.delete(synchronize_session=False)
                                db.session.delete(request)
                                db.session.commit()
                            except:
                                reqResponseCode=500
                                reqMessage="Error occurred while deleting request."
                    
                    if newRequestStatus !=-1:
                        try:
                            request.status_id=newRequestStatus

                            history=History(request_id=request.id,
                            status_id=newRequestStatus,user_id=userId)

                            db.session.add(history)

                            db.session.commit()
                        except:
                            reqResponseCode=500
                            reqMessage="Error occurred while updating request."
                else:
                    reqResponseCode=401
                    reqMessage="Operation not permitted"
            else:       
                reqResponseCode=404
                reqMessage="Request not found."
        except:
            #raise Exception()
            reqResponseCode=500
            reqMessage="An unexpected error occured."

    else:
        reqResponseCode=400
        reqMessage="Invalid request."

    reqResponse={'statusCode':reqResponseCode, "message":reqMessage}

    response=make_response(jsonify(reqResponse),reqResponseCode)

    return response


@requests_view.route("/api/createNewRequest", methods=["POST"])
@verify_session
def createNewRequest():
    userId=str(session.get('user').get("family_name"))
    role=UserRole.query.filter(UserRole.user_id==userId).first()

    reqResponseCode=200
    reqMessage=''

    if role.role_id == Role.Reporter or role.role_id == Role.Admin:
        requestData = request.data.decode("utf-8")
        requestJSON = json.loads(requestData)
        newRequest = Request(
            requester_id=session.get("user").get("preferred_username"),
            status_id=RequestStatus.Draft,
            project_id=requestJSON["project"]["id"],
            file_output_count=len(requestJSON["outputFiles"]),
            data=requestData
            )
        db.session.add(newRequest)
        db.session.commit()
    else:
        reqResponseCode=401
    
    reqResponse={'statusCode':reqResponseCode, "message":reqMessage, "newRequestId":newRequest.id }
    response=make_response(jsonify(reqResponse),reqResponseCode)
    return response

@requests_view.route("/api/editRequest/<int:requestId>", methods=["POST"])
@verify_session
def editRequest(requestId):
    userId=str(session.get('user').get("family_name"))
    role=UserRole.query.filter(UserRole.user_id==userId).first()

    reqResponseCode=200

    editRequest = Request.query.get(requestId)
    if editRequest:
        if editRequest.requester_id != userId :
            reqResponseCode=401
        elif role.role_id == Role.Reporter or role.role_id == Role.Admin:
            requestBodyJSON = json.loads(request.data.decode("utf-8"))

            editRequest.data = requestBodyJSON["formData"]
            editRequest.file_output_count=len(requestBodyJSON["formData"]["outputFiles"])
            editRequest.status_id=requestBodyJSON["requestStatus"]
        else:
            reqResponseCode=401
    else:
        reqResponseCode=404
    
    reqResponse={'statusCode':reqResponseCode}
    response=make_response(jsonify(reqResponse),reqResponseCode)
    return response