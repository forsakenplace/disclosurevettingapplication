from flask import Blueprint, render_template, session, redirect,url_for,request,make_response,jsonify
import uuid
import requests
from app.config import Config
from app.api.filters import verify_session
from app.api.msal import _load_cache,_save_cache,_build_msal_app,_get_token_from_cache

core_view = Blueprint('core', __name__)


@core_view.route("/logout")
@verify_session
def logout():
    session.clear()
    response={'statusCode': 200, 'redirectUrl':Config.LOGOUT + "?post_logout_redirect_uri=" + url_for("core.home", _external=True)}
    return make_response(jsonify(response),200)

@core_view.route("/", defaults={'path': ''})
@core_view.route('/<path:path>')
def home(path):
    if not session.get("user"):
        session["state"] = str(uuid.uuid4())
        auth_url = _build_msal_app().get_authorization_request_url(
        Config.SCOPE,  # Technically we can use empty list [] to just sign in,
                           # here we choose to also collect end user consent upfront
        state=session["state"],
        redirect_uri=url_for("core.authorized", _external=True))
        return redirect(auth_url)
    else:
        return render_template("index.html",token=session.get('token_id'))

@core_view.route("/getAToken")  # Its absolute URL must match your app's redirect_uri set in AAD
def authorized():
    if request.args['state'] != session.get("state"):
        return redirect(url_for("core.home"))
    cache = _load_cache()
    result = _build_msal_app(cache).acquire_token_by_authorization_code(
        request.args['code'],
        scopes=Config.SCOPE,  # Misspelled scope would cause an HTTP 400 error here
        redirect_uri=url_for("core.authorized", _external=True))
    if "error" in result:
        return "Login failure: %s, %s" % (
            result["error"], result.get("error_description"))
    session["user"] = result.get("id_token_claims")
    session['token_id']=result.get("id_token")
    _save_cache(cache)
    return redirect(url_for("core.home"))

@core_view.route("/refreshToken")
@verify_session
def getSilentToken():
    return make_response(jsonify(_get_token_from_cache(Config.SCOPE)))