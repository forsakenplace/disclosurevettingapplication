from flask import make_response,session,request
from functools import wraps

def verify_session(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.headers.get('Authorization')=='Bearer ' + session.get('token_id'):
            return f(*args, **kwargs)
        else:
             return  make_response("Not authorized",401)
    return decorated_function