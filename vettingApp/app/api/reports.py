from flask import Blueprint, jsonify,make_response,session
from sqlalchemy import desc,asc
from sqlalchemy import and_
from app.models import Request,Project,UserRole,RequestReviewer,UserProject, History
from app import db
from app.api.constants import RequestAction,RequestStatus,Role
from app.api.filters import verify_session
from datetime import date

reports_view = Blueprint('reports', __name__)

@reports_view.route("/api/reports/getRequests/<int:projectId>")
@verify_session
def getRequests(projectId):
    #TODO : add security for request access

    requests=Request.query.filter(Request.project_id==projectId).order_by(desc(Request.date_updated))
    
    requestsList=[]

    for request in requests:
        reqSubmitted=History.query.filter(and_(History.request_id==request.id,History.status_id==RequestStatus.AwaitingReview)).count() > 0

        outputchecker=''
        if request.outputcheckers.count() > 0:
            outputchecker= request.outputcheckers.first().outputchecker_id

        if reqSubmitted:
            reqHistory=History.query.filter(History.request_id==request.id).order_by(History.date_created)

            firstSubmission=reqHistory.filter(History.status_id==RequestStatus.AwaitingReview).first().date_created
            #delta=date.today()-request.date_updated
            submissionCount=reqHistory.filter(History.status_id==RequestStatus.InReview).count()

            requestsList.append(
                {'id': request.id,
                'statusId': request.status_id,
                'statusDesc':request.status.name_en,
                'firstSubmissionDate':firstSubmission.strftime("%Y-%m-%d, %H:%M:%S"),
                'firstSubmissionDateFormat':firstSubmission.strftime("%d %b, %Y"),
                'daysActive': 0, #delta.days,
                'submissionCount':submissionCount,
                'requester':request.requester_id,
                'outputchecker': outputchecker,
                'fileOutputCount':request.file_output_count,
                'revisionCount': submissionCount-1
                }
            )
    
    return make_response(jsonify(requestsList))

@reports_view.route("/api/reports/getHistory/<int:requestId>")
@verify_session
def getHistory(requestId):
    userId=str(session.get('user').get("family_name"))

    request=Request.query.get(requestId)
    
    projectFound=False
    reqResponseCode=200

    if request:
        projectFound=UserProject.query.filter(and_(UserProject.user_id==userId,UserProject.project_id==request.project_id)).count()>0

    if projectFound:
        history=History.query.filter(History.request_id==requestId).order_by(History.date_created)
        recordsList=[]
        for historyRec in history:
            recordsList.append({
                'id':historyRec.id,
                'statusId':historyRec.status_id,
                'statusDesc':{'en':historyRec.status.name_en,'fr':historyRec.status.name_fr},
                'userId': historyRec.user_id,
                'dateCreated':historyRec.date_created
            })

        firstSubmissionDateRec=history.filter(History.status_id==RequestStatus.AwaitingReview)
        approvalDateRec=history.filter(History.status_id==RequestStatus.Approved)
           
        summary={'firstSubmissionDate':firstSubmissionDateRec.first().date_created if firstSubmissionDateRec.count()>0 else '',
                'approvalDate':approvalDateRec.first().date_created if approvalDateRec.count()>0 else '',
                'daysToApproval': (approvalDateRec.first().date_created-firstSubmissionDateRec.first().date_created).days if approvalDateRec.count()>0 and firstSubmissionDateRec.count() > 0 else 0,
                'submissionCounts': history.filter(History.status_id==RequestStatus.AwaitingReview).count() + (history.filter(History.status_id==RequestStatus.InReview).count()-1),
                'currentStatus': request.status_id}

        data={'summary': summary, 'history': recordsList}

    else:
        reqResponseCode=401

    reqResponse={'statusCode':reqResponseCode, "data":data}

    return make_response(jsonify(reqResponse),reqResponseCode)