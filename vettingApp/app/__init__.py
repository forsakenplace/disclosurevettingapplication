from flask_sqlalchemy import SQLAlchemy
from flask import Blueprint,Flask
from app.config import Config
from flask_session.__init__ import Session

db = SQLAlchemy()


def create_app(config_class=Config):
    app = Flask(__name__,template_folder='UI',static_url_path="/static", static_folder='UI/static')
    app.config.from_object(config_class)
    
    app.config['SESSION_SQLALCHEMY']=db

    Session(app)
    db.init_app(app)

    from app.api.routes import core_view
    from app.api.requests import requests_view
    from app.api.reports import reports_view

    app.register_blueprint(reports_view)
    app.register_blueprint(requests_view)
    app.register_blueprint(core_view)

    return app