class Config:
    SQLALCHEMY_DATABASE_URI = "postgresql://testuser:testpassword@db/DisclosureVettingDb"
    SQLALCHEMY_TRACK_MODIFICATIONS = "False"
    SECRET_KEY = "super_Secret_value"
    
    #SESSION_PERMANENT=False
    SESSION_TYPE="sqlalchemy"
    PERMANENT_SESSION_LIFETIME=1200
    SESSION_USE_SIGNER=True
    SESSION_SQLALCHEMY_TABLE="Sessions"
    #SESSION_TYPE=None

    CLIENT_SECRET = "_u31xuaLUc736sOzle6-74DusWR6~Uv-ri" # Our Quickstart uses this placeholder
    # In your production app, we recommend you to use other ways to store your secret,
    # such as KeyVault, or environment variable as described in Flask's documentation here
    # https://flask.palletsprojects.com/en/1.1.x/config/#configuring-from-environment-variables
    # CLIENT_SECRET = os.getenv("CLIENT_SECRET")
    # if not CLIENT_SECRET:
    #     raise ValueError("Need to define CLIENT_SECRET environment variable")

    AUTHORITY = "https://login.microsoftonline.com/873f7e97-ac8d-4df0-9052-869b4e1dc38d"  # For multi-tenant app
    # AUTHORITY = "https://login.microsoftonline.com/Enter_the_Tenant_Name_Here"

    CLIENT_ID = "f1b8d8c8-b7e7-45b6-aa73-a885e14b50e9"

    LOGOUT="https://login.microsoftonline.com/873f7e97-ac8d-4df0-9052-869b4e1dc38d/oauth2/v2.0/logout"
    
    # You can find more Microsoft Graph API endpoints from Graph Explorer
    # https://developer.microsoft.com/en-us/graph/graph-explorer
    ENDPOINT = 'https://graph.microsoft.com/v1.0/users'  # This resource requires no admin consent

    # You can find the proper permission names from this document
    # https://docs.microsoft.com/en-us/graph/permissions-reference
    SCOPE = ["User.ReadBasic.All"]