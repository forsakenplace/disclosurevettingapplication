import datetime
from app import db
from sqlalchemy.dialects.postgresql import JSONB

class UserRole(db.Model):
    __tablename__ = 'UserRole'
    user_id=db.Column(db.String(100),primary_key=True)
    role_id=db.Column(db.Integer)

class RequestReviewer(db.Model):
    __tablename__ = 'RequestReviewer'
    __table_args__ = (
        db.PrimaryKeyConstraint('request_id', 'outputchecker_id'),
    )
    request_id = db.Column(db.Integer,db.ForeignKey('Request.id'))
    outputchecker_id=db.Column(db.String(100))
    dateCreated=db.Column(db.DateTime, default=datetime.datetime.utcnow)
    
class RequestDiscussion(db.Model):
    __tablename__ = 'RequestDiscussion'
    id=db.Column(db.Integer, primary_key=True)
    user_id=db.Column(db.String(100))
    request_id=db.Column(db.Integer)
    comment=db.Column(db.Text)
    date_submitted = db.Column(db.DateTime,
    default=datetime.datetime.utcnow)

class Project(db.Model):
    __tablename__ = 'Project'
    id=db.Column(db.Integer, primary_key=True)
    name=db.Column(db.String(50))

class UserProject(db.Model):
    __tablename__ = 'UserProject'
    __table_args__ = (
        db.PrimaryKeyConstraint('user_id', 'project_id'),
    )
    user_id=db.Column(db.String(100))
    project_id=db.Column(db.Integer,db.ForeignKey('Project.id'))
    project = db.relationship('Project', foreign_keys='UserProject.project_id', lazy='joined')

class RequestStatusCodeset(db.Model):
    __tablename__ = 'RequestStatusCodeset'
    id = db.Column(db.Integer, primary_key=True)
    name_en=db.Column(db.String(50))
    name_fr=db.Column(db.String(50))


class RoleCodeset(db.Model):
    __tablename__ = 'RoleCodeset'
    id = db.Column(db.Integer, primary_key=True)
    name=db.Column(db.String(100))

class History(db.Model):
    __tablename__='History'
    id= db.Column(db.Integer, primary_key=True)
    request_id=db.Column(db.Integer,db.ForeignKey('Request.id'))
    status_id=db.Column(db.Integer,db.ForeignKey('RequestStatusCodeset.id'))
    status = db.relationship('RequestStatusCodeset', foreign_keys='History.status_id', lazy='joined')
    date_created=db.Column(db.DateTime,
    default=datetime.datetime.utcnow)
    user_id=db.Column(db.String(100))

class Request(db.Model):
    __tablename__ = 'Request'
    id = db.Column(db.Integer, primary_key=True)
    requester_id=db.Column(db.String(100),nullable=False)
    status_id=db.Column(db.Integer,db.ForeignKey('RequestStatusCodeset.id'))
    project_id=db.Column(db.Integer,db.ForeignKey('Project.id'))
    date_submitted = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    file_output_count=db.Column(db.Integer, default=0)
    date_updated=db.Column(db.DateTime, default=datetime.datetime.utcnow)
    data=db.Column(JSONB)

    project = db.relationship('Project', foreign_keys='Request.project_id', lazy='joined')
    status = db.relationship('RequestStatusCodeset', foreign_keys='Request.status_id', lazy='joined')
    outputcheckers=db.relationship('RequestReviewer', lazy='dynamic', cascade="all")