import React,{useEffect,useState, useContext} from 'react';
import {Switch,Route,Redirect} from 'react-router-dom';
import {CircularProgress} from '@material-ui/core';

import Logout from '../Components/CommonComponents/Logout';
import OutputCheckerRoutes from './OutputCheckerRoutes';
import ReporterRoutes from './ReporterRoutes';

import Localization from '../HoC/Localization';

import { UserRole } from '../ApplicationConfig/constants';
import {UserInfoContext, defaultUserInfoValues} from "../StateObjects/UserInfoContext";
import {ProjectInfoContext, defaultProjectInfoValues} from "../StateObjects/ProjectInfoContext";

import {Auth} from '../Api/auth'

export default function InitializeRoutes(){

  const [pageStatus,setPageStatus]=useState({
    dataLoaded:false,
    errorMessage:'',
    component:null
  });

  let storeProjectInfo = localStorage.getItem("projectInfo");
  if(!storeProjectInfo){
    storeProjectInfo = JSON.stringify(defaultProjectInfoValues);
    localStorage.setItem("projectInfo", storeProjectInfo);
  }
  const [projectInfo, setProjectInfo] = useState(JSON.parse(storeProjectInfo));
  const handleProjectChange = (newProjectInfo)=>{
    localStorage.setItem("projectInfo", JSON.stringify(newProjectInfo))
    setProjectInfo(newProjectInfo);
  }

  let storeUserInfo = localStorage.getItem("userInfo");
  if(!storeUserInfo){
    storeUserInfo = JSON.stringify(defaultUserInfoValues);
    localStorage.setItem("userInfo", storeUserInfo);
  }
  const [userInfo, setUserInfo] = useState(JSON.parse(storeUserInfo));
  const handleUserChange = (newUserInfo)=>{
    localStorage.setItem("userInfo", JSON.stringify(newUserInfo))
    setUserInfo(newUserInfo);
  }

  useEffect(() => {
    Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/getUserInfo")
      .then(res => res.json())
      .then(
        (result) => {

          if (result.statusCode==200){
            handleUserChange({id:result.data.id,
              fullName:result.data.fullName,
              role:result.data.role,
              projects: result.data.projects});

            setPageStatus({...pageStatus,dataLoaded:true});
            
            //if project is not set OR a change of user occured
            if (projectInfo.id.length==0 || (userInfo.id!=undefined && result.data.id != userInfo.id)){
              handleProjectChange(result.data.projects[0]);
            }
          }
          else{
        
            setPageStatus({...pageStatus,dataLoaded:false,errorMessage:result.message,component:<Logout />});
            
          }
        })
        .catch(
          (error) => {
            setPageStatus({...pageStatus,dataLoaded:false,errorMessage:"An error occured while getting data"});
        })
      
  }, [])
  
  return(
    <UserInfoContext.Provider value={{userInfo, setUserInfo:handleUserChange}}>
      <Switch>
          <Route exact path="/">
              <Redirect from='/' to='/en/home' /> 
          </Route>
          <Route path="/:lang(en|fr)?">
              {pageStatus.dataLoaded && 
              <Localization>
                <ProjectInfoContext.Provider value={{projectInfo, setProjectInfo:handleProjectChange}}>
                  {(userInfo.role==UserRole.outputchecker && <OutputCheckerRoutes />) || (userInfo.role==UserRole.reporter && <ReporterRoutes />)}
                </ProjectInfoContext.Provider>
              </Localization>}
              
              {!pageStatus.dataLoaded && pageStatus.errorMessage.length==0 &&
              <div style={{height:'100%',display: 'flex',alignItems:'center',justifyContent:'center'}}>
                <CircularProgress />
              </div>}
              
              {pageStatus.errorMessage.length>0 && 
              <div style={{height:'100%',display: 'flex',alignItems:'center',justifyContent:'center', flexDirection:'column'}}>
                {pageStatus.errorMessage}
                
                {pageStatus.component}
                
              </div>}

          </Route>
      </Switch>
    </UserInfoContext.Provider>
  )
}