import React,{useState} from 'react';
import {Route,Switch,useRouteMatch} from 'react-router-dom';
import {RequestType} from '../ApplicationConfig/Outputchecker/constants'
import RequestDashboard from '../Components/OutputChecker/RequestDashboard';
import RequestsDashboard from '../Components/OutputChecker/RequestsDashboard';
import ReportsDashboard from '../Components/Reports/ReportsDashboard';
import RequestReport from '../Components/Reports/RequestHistoryComponents/RequestReport';

export default function OutputCheckerRoutes(){
    let match = useRouteMatch();
    
    let requests = localStorage.getItem("requestList");
    if(!requests){
        requests = "[]";
        localStorage.setItem("requestList",requests);
    }
    const [rows, setRows] = useState(JSON.parse(requests));
    const handleRequestListChange = (newValue) => {
        localStorage.setItem("requestList",JSON.stringify(newValue));
        setRows(newValue);
    }
    
    return (
       <Switch>
            <Route exact path={match.path+"/home"}>
                <RequestsDashboard rows={rows} setRows={handleRequestListChange} />
            </Route>
            <Route path={match.path+"/viewRequest/:requestId"} > 
                <RequestDashboard rows={rows} setRows={handleRequestListChange} />
            </Route>
            <Route path={match.path+"/reports"}>     
                <ReportsDashboard />
            </Route>
            <Route path={match.path+"/reports/:requestId"}>     
                <RequestReport />
            </Route>
        </Switch>
    )
}