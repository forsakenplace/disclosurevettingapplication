import React,{useEffect,useState} from 'react';
import {Route,Switch,useRouteMatch} from 'react-router-dom';

import RequestForm from '../Components/Reporter/NewRequestForm';
import ViewRequest from '../Components/Reporter/ViewRequest';
import EditRequest from '../Components/Reporter/EditRequest';
import RequestsList from '../Components/Reporter/RequestsList';

import ReportsDashboard from '../Components/Reports/ReportsDashboard';
import RequestReport from '../Components/Reports/RequestHistoryComponents/RequestReport';

export default function ReporterRoutes(){
    let match = useRouteMatch();
    return (
       <Switch>
           <Route exact path={match.path+"/home"}>
                <RequestsList />
            </Route>
            <Route exact path={match.path+"/newrequest"}>
                <RequestForm />
            </Route>
            <Route exact path={match.path+"/editrequest/:requestId"}>
                <EditRequest />
            </Route>
            <Route exact path={match.path+"/viewrequest/:requestId"}>
                <ViewRequest />
            </Route>
            <Route path={match.path+"/reports/:requestId"}>     
                <RequestReport />
            </Route> 
            <Route path={match.path+"/reports"}>     
                <ReportsDashboard />
            </Route>
        </Switch>
    )
}