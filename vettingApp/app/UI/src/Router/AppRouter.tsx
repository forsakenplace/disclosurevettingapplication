import React,{useEffect,useState} from 'react';
import {BrowserRouter, Route, Switch,Redirect} from 'react-router-dom';
import {ThemeProvider} from '@material-ui/styles';
import {CircularProgress} from '@material-ui/core';
import { SnackbarProvider } from 'notistack';

import InitializeRoutes from './InitializeRoutes'

import {theme} from '../Theme/theme';
import {useStyles} from '../Theme/globalStyles';

import {Auth} from '../Api/auth';

export default function AppRouter() {

    useStyles();
    let token=document.getElementById("token").innerHTML;

    if (token){
        Auth.login(token);
        document.getElementById("token").innerHTML=null;
    }

    return (
        <>
        {token && 
            <BrowserRouter>
                <ThemeProvider theme={theme}>
                    <SnackbarProvider 
                    maxSnack={3} 
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }} 
                    iconVariant={{
                        info: <CircularProgress size={30} style={{color:'#FFFFFF',marginRight:'1em'}} />,
                    }}>

                    <InitializeRoutes />                
                            
                    </SnackbarProvider>
                </ThemeProvider>
            </BrowserRouter>}

        {!token && "Not authorized"}
        </>
    )

}