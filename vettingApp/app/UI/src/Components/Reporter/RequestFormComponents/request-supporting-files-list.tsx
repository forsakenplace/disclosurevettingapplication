import React from 'react';
import Tooltip from '@material-ui/core/Tooltip';
import InfoIcon from '@material-ui/icons/Info';
import Button from '@material-ui/core/Button';
import FilesTable from './files-table'
import { useFieldArray } from 'react-hook-form';

export default function RequestSupportingFilesList({control, errors, mandatories, files, index}) {
    const { fields, append, remove} = useFieldArray({
        control,
        name: "outputFiles["+index+"].otherSupportingFiles"
    });

    let errorsList = {};
    if(errors.outputFiles && errors.outputFiles[index]){
        errorsList["outputFiles["+index+"].mandatorySupportingFiles"]=errors.outputFiles[index].mandatorySupportingFiles;
        errorsList["outputFiles["+index+"].otherSupportingFiles"]=errors.outputFiles[index].otherSupportingFiles;
    };
    
    return (
        <>
            <h3>Mandatory supporting files:</h3>
            <ul>
                {Object.keys(mandatories).map((item, index)=> (
                    <li key={index}>{item}</li>
                ))}
                {Object.keys(mandatories).length===0 && (
                    <li key={0}>No mandatory files</li>
                )}
            </ul>
            <p>Note: supporting files will not be released. Please name your support files to allow easy pairing of the corresponding output file.</p>
            <div>
                <Button type="button" variant="contained" onClick={()=> append({fileName:"", fileContents:"", notes:"", required:"false"})}>Add Supporting File</Button>
                <Tooltip title="In addition to the mandatory files listed, include other files as required by the Survey Specific Guidelines, syntax files or other files requested by the analyst." arrow>
                    <InfoIcon />
                </Tooltip>
            </div>
            <FilesTable 
                mandatories={mandatories}
                fields={fields}
                mandatoryFieldsName={"outputFiles["+index+"].mandatorySupportingFiles"}
                otherFieldsName={"outputFiles["+index+"].otherSupportingFiles"}
                remove={remove}
                files={files}
                errors={errorsList}
                control={control}
            />
        </>
    );
}