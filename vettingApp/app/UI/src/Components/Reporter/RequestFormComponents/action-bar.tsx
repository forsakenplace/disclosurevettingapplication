import React from 'react';
import { useHistory } from 'react-router-dom';
import {Button,Typography, Grid} from '@material-ui/core';
import {Save,Cancel,NavigateNext,ArrowBack} from '@material-ui/icons'

export default function ActionBar(props) {
    const history = useHistory();

    let nextCaption="Submit"
    
    if (props.navigationNextCaption)
    {
        nextCaption=props.navigationNextCaption
    }
    
    let handleCancelAction = ()=> history.push("/");
    if(props.handleCancel){
        handleCancelAction=props.handleCancel;
    }

    return (
        <Grid item xs={6}>
            <Typography variant="h2">Requester</Typography>
            <p>user account</p>
            <Typography variant="h2">Actions</Typography>
            {props.handleSave &&
                <>
                <Button color="primary" onClick={props.handleSave}>
                    <Save />&nbsp;Save
                </Button>
                <br />
                </>
            }
            <Button color="primary" onClick={handleCancelAction}>
                <Cancel />&nbsp;Cancel
            </Button>
            <br />
            <Button color="primary" type="submit"><NavigateNext />&nbsp;{nextCaption}</Button>
        </Grid>
    );
}