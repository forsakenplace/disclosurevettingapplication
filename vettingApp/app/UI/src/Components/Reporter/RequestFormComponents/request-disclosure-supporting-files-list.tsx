import React from 'react';
import Button from '@material-ui/core/Button';

import { useFieldArray } from 'react-hook-form';
import FilesTable from './files-table';

export default function RequestDisclosureSupportingFilesList({control, errors, files, mandatories}) {
    const { fields, append, remove} = useFieldArray({
        control,
        name: "otherDisclosureSupportingFiles"
    });

    return (
        <>
            <h3>Supporting Files for Residual Disclosure Risk:</h3>
            <ul>
                {Object.keys(mandatories).map((item, index)=> (
                    <li key={index}>{item}</li>
                ))}
            </ul>
            <div>
                <Button type="button" variant="contained" onClick={()=> append({fileName:"", fileContents:"", notes:"", required:"false"})}>Add Supporting File for Residual Disclosure</Button>
            </div>
            <FilesTable 
                mandatories={mandatories}
                fields={fields}
                mandatoryFieldsName={"mandatoryDisclosureSupportingFiles"}
                otherFieldsName={"otherDisclosureSupportingFiles"}
                remove={remove}
                files={files}
                errors={errors}
                control={control}
            />
        </>
    );
}