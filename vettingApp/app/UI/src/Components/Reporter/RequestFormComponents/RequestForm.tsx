import React,{useState, useEffect} from 'react';
import {Typography,Grid,Paper, Tabs, Tab} from '@material-ui/core';
import { useTranslation } from "react-i18next";
import FormErrorsAlert from '../../CommonComponents/FormErrorsAlert';
import {CalendarToday,Edit} from '@material-ui/icons';
import {makeStyles} from '@material-ui/styles';
import {useHistory} from 'react-router-dom';
import { useSnackbar } from 'notistack';
import {Auth} from '../../../Api/auth';
import PropTypes from 'prop-types';

import {FormProvider, useForm} from 'react-hook-form';
import RequestHeader from './request-header';
import RequestOutputList from './request-output-list';
import RequestOutputFile from './request-output-file';
import RequestDisclosure from './request-disclosure';
import RequestFooter from './request-footer';
import ActionBar from './action-bar';
import outputFileDefaults from '../../../StateObjects/outputFileDefaults.json';
import {RequestStatusId} from '../../../ApplicationConfig/constants';

const useStyles = makeStyles((theme) => ({
    header:{
        backgroundColor:'#f5f5f5',
        width:'100%',
        height:150,
        padding: theme.spacing(3, 0, 0, 3),
        borderBottom:'1px solid #dee2e6',
        flexGrow:1,
        boxSizing:'border-box',
      },
      requestStatusPaper:{
        boxShadow:'none',
        padding: theme.spacing(1, 2, 1, 2),
        maxWidth:200,
        float:'left'
      },
}))

export default function RequestForm({formData, setFormData, requestIdValue}){
    const classes = useStyles();

    const history=useHistory();

    const { t } = useTranslation();

    const {enqueueSnackbar} = useSnackbar();

    const formMethods = useForm({defaultValues: formData});

    const [tabValue, setTabValue] = useState(0);
    const [tabsDisabled, setTabsDisabled] = useState(formData.tabsDisabled);
    const [editOutputFile, setEditOutputFile] = useState(0);
    const [requestId, setRequestId] = useState(requestIdValue);

    const handleEditOutputFile = (output)=>{
        formMethods.clearErrors("outputFiles");
        setEditOutputFile(output);
        let newFormData = {...formData, ...formMethods.getValues()}
        setFormData(newFormData);
        formMethods.reset(formData);
    }

    const onSubmit = (data)=> {
        if(!data.otherDisclosureSupportingFiles){
            data.otherDisclosureSupportingFiles=[];
        }

        if(tabsDisabled){
            data.tabsDisabled = false;

            Auth.authorizedFetch("/api/createNewRequest", {method:"POST", body: JSON.stringify({...formData, ...data}), headers:{'Content-type':'application/json'}})
            .then((response)=> {
                if(!response.ok){
                    enqueueSnackbar(response.statusText,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
                    throw Error(response.statusText)
                }
                
                return response.json();
            })
            .then(responseData => {
                setRequestId(responseData.newRequestId)
                setFormData({...formData, ...data});
                setTabsDisabled(false);
                setTabValue(1);
            });
        }else if(editOutputFile!==0){
            let newOutputFiles = formData.outputFiles.slice();
            newOutputFiles[editOutputFile-1] = {...outputFileDefaults, ...data.outputFiles[editOutputFile-1]};
            setFormData({...formData, ...{outputFiles: newOutputFiles}});
            setEditOutputFile(0);
        }else{
            if(formData.outputFiles.length===0 && !formMethods.errors.outputFiles){
                formMethods.setError("outputFiles", {type:"required", message:"An output file is required"});
                return;
            }else if(formData.outputFiles.length>0 && !formMethods.errors.outputFiles){
                for(let i = 0; i<formData.outputFiles.length; i++){
                    if(formData.outputFiles[i].outputFile===""){
                        formMethods.setError("outputFiles", {type:"required", message:"One or more of the outputs have not been completed"});
                        return;
                    }
                }
            }

            Auth.authorizedFetch("/api/editRequest/"+requestId, {method:"POST", body: JSON.stringify({formData:{...formData, ...data}, requestStatus: RequestStatusId.AwaitingReview}), headers:{'Content-type':'application/json'}})
            .then((response)=> {
                if(!response.ok){
                    enqueueSnackbar(response.statusText,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
                    throw Error(response.statusText)
                }
                history.push("/");
            });
        }
    };
    
    async function asyncSaveForm(){
        try{
            let formValues = formMethods.getValues();
            if(editOutputFile!==0){
                const editFileData = formValues.outputFiles.slice()[editOutputFile-1];
                formValues.outputFiles = formData.outputFiles;
                formValues.outputFiles[editOutputFile-1] = editFileData;
            }
            const newData = {...formData, ...formValues};
            if(JSON.stringify(formData)!==JSON.stringify(newData)){
                const response = await Auth.authorizedFetch("/api/editRequest/"+requestId, {method:"POST", body: JSON.stringify({formData:newData, requestStatus: RequestStatusId.WorkInProgress}), headers:{'Content-type':'application/json'}});
                if(!response.ok){
                    enqueueSnackbar(response.statusText,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
                    throw Error(response.statusText)
                }
                setFormData(newData);
            }
        }catch(error){
            console.log(error)
        }
    }

    const saveForm = ()=>{
        try{
            let formValues = formMethods.getValues();
            if(editOutputFile!==0){
                const editFileData = formValues.outputFiles.slice()[editOutputFile-1];
                formValues.outputFiles = formData.outputFiles;
                formValues.outputFiles[editOutputFile-1] = editFileData;
            }
            const newData = {...formData, ...formValues};
            Auth.authorizedFetch("/api/editRequest/"+requestId, {method:"POST", body: JSON.stringify({formData:newData, requestStatus: RequestStatusId.WorkInProgress}), headers:{'Content-type':'application/json'}})
            .then((response)=> {
                if(!response.ok){
                    enqueueSnackbar(response.statusText,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
                    throw Error(response.statusText)
                }
                setFormData(newData);
                enqueueSnackbar('Succesfully saved',{variant:'success',autoHideDuration:2000,preventDuplicate: true});
            });
        }catch(error){
            console.log(error)
        }
    }

    const handleTabChange = (e, newVal)=> {
        asyncSaveForm();
        if(editOutputFile!==0){
            setEditOutputFile(0);
        }
        setTabValue(newVal);
    };

    useEffect(()=> {
        formMethods.reset(formData);
    }, [tabsDisabled]);

    return (
        <Grid container direction="column" spacing={2}>
            <Grid item>
                <Paper className={classes.header}>
                <Grid container direction="row">
                    <Grid item xs={6}>
                        <Typography variant="h1">{t("requestFormTitle")}</Typography>
                        <CalendarToday fontSize="small"/>
                        <Typography variant="body2" component='span'>Submitted at 13:47 on Tuesday, August 11th 2020</Typography> 
                    </Grid>
                    <Grid item xs={6}>
                        <Paper className={classes.requestStatusPaper} style={{backgroundColor:'#6b778c'}}>
                            <Edit style={{ color: '#FFFFFF',marginTop:5}}/>
                            <Typography component="span" style={{ color: '#FFFFFF',marginTop:-5}}>Work in progress</Typography>
                        </Paper>
                    </Grid>
                </Grid>
                </Paper>
            </Grid>
            <Grid item>
                <FormErrorsAlert errors={formMethods.errors}/>
            </Grid>
            <Grid item>
                <Typography variant="h1">{t("requestFormTitle")}</Typography>
                <Tabs value={tabValue} onChange={handleTabChange}>
                    <Tab label="Request Header" id="simple-tab-0" aria-controls="simple-tabpanel-0"/>
                    <Tab label="Request Output Files List" id="simple-tab-1" aria-controls="simple-tabpanel-1" disabled={tabsDisabled}/>
                    <Tab label="Request Dislcosure" id="simple-tab-2" aria-controls="simple-tabpanel-2" disabled={tabsDisabled}/>
                    <Tab label="Request Footer" id="simple-tab-3" aria-controls="simple-tabpanel-3" disabled={tabsDisabled}/>
                    <Tab label="Discussion" id="simple-tab-4" aria-controls="simple-tabpanel-4"/>
                </Tabs>
                <FormProvider {...formMethods}>
                    <form onSubmit={formMethods.handleSubmit(onSubmit)} noValidate>
                        {tabsDisabled &&
                            <Grid container spacing={3} direction="row">
                                <RequestHeader formData={formData} setFormData={setFormData}/>
                                <ActionBar navigationNextCaption={"Create Request"} />
                            </Grid>
                        }
                        {!tabsDisabled && editOutputFile===0 &&
                            <>
                                <div hidden={tabValue!==0}>
                                    <Grid container spacing={3} direction="row">
                                        <RequestHeader formData={formData} setFormData={setFormData}/>
                                        <ActionBar handleSave={saveForm}/>
                                    </Grid>
                                </div>
                                <div hidden={tabValue!==1}>
                                    <Grid container spacing={3} direction="row">
                                        <RequestOutputList handleEditOutputFile={(output)=> handleEditOutputFile(output)} formData={formData} setFormData={setFormData} />
                                        <ActionBar handleSave={saveForm}/>
                                    </Grid>
                                </div>
                                <div hidden={tabValue!==2}>
                                    <Grid container spacing={3} direction="row">
                                        <RequestDisclosure formData={formData} />
                                        <ActionBar handleSave={saveForm}/>
                                    </Grid>
                                </div>
                                <div hidden={tabValue!==3}>
                                    <Grid container spacing={3} direction="row">
                                        <RequestFooter />
                                        <ActionBar handleSave={saveForm}/>
                                    </Grid>
                                </div>
                            </>
                        }
                        {!tabsDisabled && editOutputFile!==0 &&
                            <Grid container spacing={3} direction="row">
                                <RequestOutputFile output={editOutputFile} formData={formData} />
                                <ActionBar navigationNextCaption={"Confirm"}  handleCancel={()=>setEditOutputFile(0)} handleSave={saveForm} />
                            </Grid>
                        }
                    </form>
                </FormProvider>
                <div hidden={tabValue!==4}>
                    <pre>{JSON.stringify(formData, null ,2)}</pre>
                </div>
            </Grid>
        </Grid>
    );
}

RequestForm.propTypes = {
    formData: PropTypes.object.isRequired,
    setFormData: PropTypes.func.isRequired,
    requestIdValue: PropTypes.string.isRequired
}