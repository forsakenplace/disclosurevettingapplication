import React, { useState } from 'react';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';

import AlertDialog from '../../CommonComponents/AlertDialog';
import { useFormContext } from 'react-hook-form';
import outputFileDefaults from '../../../StateObjects/outputFileDefaults.json';

import {makeStyles} from '@material-ui/styles';


const useStyles = makeStyles((theme) => ({
    buttonMargin: {
        marginBottom:'1em'
    }
  }));

export default function RequestOutputList({handleEditOutputFile, formData, setFormData}) {
    const classes = useStyles();
    const {errors, clearErrors, trigger, getValues} = useFormContext();

    const [openAlert, setOpenAlert] = useState(false);
    const [currentRowIndex, setCurrentRowIndex] = useState(-1);

    const handleAlertOpen = (index) => {
        clearErrors("outputFiles");

        setCurrentRowIndex(index);
        setOpenAlert(true);
    };

    const addOutputFile = ()=> {
        clearErrors("outputFiles");

        if(getValues("outputFilesFolder") !== ""){
            let defaults = {...outputFileDefaults};
            defaults["id"] = formData.outputFiles.length+1;
            const newOutputList = formData.outputFiles.concat(defaults);
            setFormData({...formData, ...{outputFiles: newOutputList}});
        }else{
            trigger("outputFilesFolder");
        }
    };

    const deleteOutputFile = (index)=>{
        const newOutputList = formData.outputFiles.slice(0, index).concat(formData.outputFiles.slice(index+1));
        for(let i = 0; i<newOutputList.length; i++){
            newOutputList[i].id = i+1;
        }
        setFormData({...formData, ...{outputFiles: newOutputList}});
        
        setCurrentRowIndex(-1);
        setOpenAlert(false);
    };

    return (
        <>
            <Grid item xs={6}>
                <Typography variant="h2" style={{fontSize:'1.5em',marginBottom:'1em'}}>Output file list</Typography>
                <Button onClick={addOutputFile} variant="contained" className={classes.buttonMargin}>Add Output File</Button>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>File Name</TableCell>
                                <TableCell />
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {formData.outputFiles.map((item, index)=> (
                                <TableRow key={index}>
                                    <TableCell>
                                        {"Output file #" + item.id}
                                    </TableCell>
                                    <TableCell>
                                        {item.outputFile}
                                    </TableCell>
                                    <TableCell>
                                        <Button type="button" variant="contained" onClick={()=> handleEditOutputFile(item.id)}>Modify</Button>&nbsp;
                                        <Button type="button" variant="contained" onClick={()=> handleAlertOpen(index)}>Delete</Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                <FormHelperText error={!!errors.outputFiles}>{errors.outputFiles?.message}</FormHelperText>
            </Grid>
            <AlertDialog openAlert={openAlert} handleAlertClose={()=>setOpenAlert(false)} handleConfirmButton={()=>deleteOutputFile(currentRowIndex)} descriptionMessage={"Are you sure you want to continue with this action?"}/>
        </>
    );
}