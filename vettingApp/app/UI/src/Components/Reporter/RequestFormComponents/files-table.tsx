import React from 'react';
import TextField from '@material-ui/core/TextField';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import { Controller } from 'react-hook-form';

export default function FilesTable({
    mandatories,
    files,
    fields,
    mandatoryFieldsName,
    otherFieldsName,
    remove,
    errors,
    control
}) {
    const requiredErrorMessage = "This field is required";
    
    return (
        <TableContainer>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>File Name</TableCell>
                        <TableCell>File Contents</TableCell>
                        <TableCell>Notes</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {Object.keys(mandatories).map((item, index)=> (
                        <TableRow key={index}>
                            <TableCell>
                                <FormControl required variant="outlined" fullWidth error={errors[mandatoryFieldsName] && errors[mandatoryFieldsName][item] && errors[mandatoryFieldsName][item].fileName ? true : false} >
                                    <Controller
                                        render={({onBlur, onChange, value})=> (
                                            <Select
                                                onChange={onChange}
                                                value={value}
                                            >
                                                {files.map((item, index)=> (
                                                        <MenuItem key={index} value={item}>{item}</MenuItem>
                                                    ))
                                                }
                                            </Select>
                                        )}
                                        name={`${mandatoryFieldsName}["${item}"].fileName`} 
                                        control={control}
                                        rules={{required:requiredErrorMessage}}
                                        defaultValue={mandatories[item].fileName}
                                    />
                                    <FormHelperText>{errors[mandatoryFieldsName] && errors[mandatoryFieldsName][item] && errors[mandatoryFieldsName][item].fileName ? errors[mandatoryFieldsName][item].fileName.message : "" }</FormHelperText>
                                </FormControl>
                            </TableCell>
                            <TableCell>
                                {item}
                            </TableCell>
                            <TableCell>
                                <Controller
                                    as={
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            multiline
                                        />
                                    }
                                    name={`${mandatoryFieldsName}["${item}"].notes`} 
                                    control={control}
                                    defaultValue={mandatories[item].notes}
                                />
                            </TableCell>
                        </TableRow>
                    ))}
                    {fields.map((item, index)=> (
                        <TableRow key={index}>
                            <TableCell>
                                <FormControl required variant="outlined" fullWidth error={errors[otherFieldsName] && errors[otherFieldsName][index] && errors[otherFieldsName][index].fileName ? true : false} >
                                    <Controller
                                        render={({onBlur, onChange, value})=> (
                                            <Select
                                                onChange={onChange}
                                                value={value}
                                            >
                                                {files.map((item, index)=> (
                                                        <MenuItem key={index} value={item}>{item}</MenuItem>
                                                    ))
                                                }
                                            </Select>
                                        )}
                                        name={`${otherFieldsName}["${index}"].fileName`} 
                                        control={control}
                                        rules={{required:requiredErrorMessage}}
                                        defaultValue={item.fileName}
                                    />
                                    <FormHelperText>{errors[otherFieldsName] && errors[otherFieldsName][index] && errors[otherFieldsName][index].fileName ? errors[otherFieldsName][index].fileName.message : "" }</FormHelperText>
                                </FormControl>
                            </TableCell>
                            <TableCell>
                                <FormControl required variant="outlined" fullWidth error={errors[otherFieldsName] && errors[otherFieldsName][index] && errors[otherFieldsName][index].fileContents ? true : false} >
                                    <Controller
                                        render={({onBlur, onChange, value})=> (
                                            <Select
                                                onChange={onChange}
                                                value={value}
                                            >
                                                {Object.keys(mandatories).concat("Other").map((item, index)=> (
                                                        <MenuItem key={index} value={item}>{item}</MenuItem>
                                                    ))
                                                }
                                            </Select>
                                        )}
                                        name={`${otherFieldsName}["${index}"].fileContents`} 
                                        control={control}
                                        rules={{required:requiredErrorMessage}}
                                        defaultValue={item.fileContents}
                                    />
                                    <FormHelperText>{errors[otherFieldsName] && errors[otherFieldsName][index] && errors[otherFieldsName][index].fileContents ? errors[otherFieldsName][index].fileContents.message : "" }</FormHelperText>
                                </FormControl>
                            </TableCell>
                            <TableCell>
                                <Controller
                                    as={
                                        <TextField
                                            variant="outlined"
                                            fullWidth
                                            multiline
                                        />
                                    }
                                    name={`${otherFieldsName}["${index}"].notes`} 
                                    control={control}
                                    defaultValue={item.notes}
                                />
                            </TableCell>
                            <TableCell>
                                <Button type="button" variant="contained" onClick={()=> remove(index)}>Delete</Button>
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}