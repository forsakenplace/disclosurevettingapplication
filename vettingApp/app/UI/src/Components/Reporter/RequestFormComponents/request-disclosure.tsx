import React from 'react';
import TextField from '@material-ui/core/TextField';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Tooltip from '@material-ui/core/Tooltip';
import InfoIcon from '@material-ui/icons/Info';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

import { useFormContext, Controller } from 'react-hook-form';
import RequestDisclosureSupportingFilesList from './request-disclosure-supporting-files-list';

import {makeStyles} from '@material-ui/styles';

const useStyles = makeStyles((theme) => ({
    bottomSpacing: {
        marginBottom:'1em'
    },
    leftIdent:{
        marginLeft:'1em'
    },
    leftIdent2:{
        marginLeft:'1em'
    }
  }));

export default function RequestDisclosure({formData}) {
    const classes = useStyles();

    const { register, control, watch, errors } = useFormContext();

    const watchFields = watch(["outputPreviousRelease","versionPreviousRelease"]);

    const files = ["abc", "def","ghi"];

    const requiredErrorMessage = "This field is required";

    return (
        <>
            <Grid item xs={6}>
                <Typography variant="h2" style={{fontSize:'1.5em',marginBottom:'1em'}}>Residual disclosure risk</Typography>
                <fieldset>
                    <legend>Residual Disclosure Header</legend>
                    <FormControl component="fieldset" error={!!errors.outputPreviousRelease}>
                        <FormLabel component="legend">Have other outputs been previously released for this project?</FormLabel>
                        <Controller
                            render={({onBlur, onChange, value})=> (
                                <RadioGroup id="outputPreviousRelease" onChange={onChange} value={value} row>
                                    <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                    <FormControlLabel value="No" control={<Radio />} label="No" />
                                </RadioGroup>
                            )}
                            name="outputPreviousRelease"
                            control={control}
                        />
                        <FormHelperText>{errors.outputPreviousRelease?.message}</FormHelperText>
                    </FormControl>
                    {watchFields.outputPreviousRelease === "Yes" &&
                        <Container className={classes.leftIdent}>
                            <FormControl component="fieldset" error={!!errors.subsetVariables}>
                                <FormLabel component="legend">
                                    For this request, are any variables a subset of another variable?
                                    <Tooltip title="Examples: had depression in the past 5 years and had depression in the previous year, had a chronic condition and had a respiratory chronic condition" arrow>
                                        <InfoIcon />
                                    </Tooltip>
                                </FormLabel>
                                <Controller
                                    render={({onBlur, onChange, value})=> (
                                        <RadioGroup id="subsetVariables" onChange={onChange} value={value} row>
                                            <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                            <FormControlLabel value="No" control={<Radio />} label="No" />
                                        </RadioGroup>
                                    )}
                                    name="subsetVariables"
                                    control={control}
                                />
                                <FormHelperText>{errors.subsetVariables?.message}</FormHelperText>
                            </FormControl>
                            <FormControl component="fieldset" error={!!errors.versionPreviousRelease}>
                                <FormLabel component="legend">Has a version of this output, in part of in whole, been previously released for this project?</FormLabel>
                                <Controller
                                    render={({onBlur, onChange, value})=> (
                                        <RadioGroup id="versionPreviousRelease" onChange={onChange} value={value} row>
                                            <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                            <FormControlLabel value="No" control={<Radio />} label="No" />
                                        </RadioGroup>
                                    )}
                                    name="versionPreviousRelease"
                                    control={control}
                                />
                                <FormHelperText>{errors.versionPreviousRelease?.message}</FormHelperText>
                            </FormControl>
                            {watchFields.versionPreviousRelease === "Yes" &&
                                <Container className={classes.leftIdent}>
                                    <p>Compared to other output for this project, have you :</p>
                                    <FormControl component="fieldset" error={!!errors.changedSubSample} required fullWidth>
                                        <FormLabel component="legend">Changed the sub-sample of population of interest?</FormLabel>
                                        <Controller
                                            render={({onBlur, onChange, value})=> (
                                                <RadioGroup id="changedSubSample" onChange={onChange} value={value} row>
                                                    <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                    <FormControlLabel value="No" control={<Radio />} label="No" />
                                                </RadioGroup>
                                            )}
                                            name="changedSubSample"
                                            control={control}
                                            rules={{required:requiredErrorMessage}}
                                        />
                                        <FormHelperText>{errors.changedSubSample?.message}</FormHelperText>
                                    </FormControl>
                                    <FormControl component="fieldset" error={!!errors.droppedOutliers} required fullWidth>
                                        <FormLabel component="legend">Droppped individual cases or outliers?</FormLabel>
                                        <Controller
                                            render={({onBlur, onChange, value})=> (
                                                <RadioGroup id="droppedOutliers" onChange={onChange} value={value} row>
                                                    <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                    <FormControlLabel value="No" control={<Radio />} label="No" />
                                                </RadioGroup>
                                            )}
                                            name="droppedOutliers"
                                            control={control}
                                            rules={{required:requiredErrorMessage}}
                                        />
                                        <FormHelperText>{errors.droppedOutliers?.message}</FormHelperText>
                                    </FormControl>
                                    <FormControl component="fieldset" error={!!errors.imputedData} required fullWidth>
                                        <FormLabel component="legend">Imputed the missing data?</FormLabel>
                                        <Controller
                                            render={({onBlur, onChange, value})=> (
                                                <RadioGroup id="imputedData" onChange={onChange} value={value} row>
                                                    <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                    <FormControlLabel value="No" control={<Radio />} label="No" />
                                                </RadioGroup>
                                            )}
                                            name="imputedData"
                                            control={control}
                                            rules={{required:requiredErrorMessage}}
                                        />
                                        <FormHelperText>{errors.imputedData?.message}</FormHelperText>
                                    </FormControl>
                                    <FormControl component="fieldset" error={!!errors.modifiedVariables} required fullWidth>
                                        <FormLabel component="legend">Recoded or modified any variables even slightly?</FormLabel>
                                        <Controller
                                            render={({onBlur, onChange, value})=> (
                                                <RadioGroup id="modifiedVariables" onChange={onChange} value={value} row>
                                                    <FormControlLabel value="Yes" control={<Radio />} label="Yes" />
                                                    <FormControlLabel value="No" control={<Radio />} label="No" />
                                                </RadioGroup>
                                            )}
                                            name="modifiedVariables"
                                            control={control}
                                            rules={{required:requiredErrorMessage}}
                                        />
                                        <FormHelperText>{errors.modifiedVariables?.message}</FormHelperText>
                                    </FormControl>
                                    <TextField
                                        id="explainChanges"
                                        name="explainChanges"
                                        label="Explanation of changes"
                                        variant="outlined"
                                        inputRef={register({required:requiredErrorMessage})}
                                        error={!!errors.explainChanges}
                                        helperText={errors.explainChanges?.message}
                                        required
                                        fullWidth
                                        className={classes.bottomSpacing}
                                        multiline
                                    />
                                </Container>
                            }
                            <TextField
                                id="outputRelates"
                                name="outputRelates"
                                label="Please describe how the output in this request relates to other output for this project"
                                variant="outlined"
                                inputRef={register({required:requiredErrorMessage})}
                                error={!!errors.outputRelates}
                                helperText={errors.outputRelates?.message}
                                required
                                fullWidth
                                className={classes.bottomSpacing}
                                multiline
                            />
                            <TextField
                                id="previousVettingDates"
                                name="previousVettingDates"
                                label="Indicate the date(s) of the previous vetting requests related to this request"
                                variant="outlined"
                                inputRef={register({required:requiredErrorMessage})}
                                error={!!errors.previousVettingDates}
                                helperText={errors.previousVettingDates?.message}
                                required
                                fullWidth
                                className={classes.bottomSpacing}
                            />
                        </Container>
                    }
                </fieldset>
                <fieldset>
                    <legend>Residual Disclosure Supporting Files</legend>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <RequestDisclosureSupportingFilesList control={control} errors={errors} files={files} mandatories={formData.mandatoryDisclosureSupportingFiles} />
                        </Grid>
                    </Grid>
                </fieldset>
            </Grid>
        </>
    );
}