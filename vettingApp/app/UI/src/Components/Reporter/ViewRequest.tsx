import React,{useState,useEffect} from 'react';
import {Container, Typography,Grid} from '@material-ui/core';
import { useTranslation } from "react-i18next";
import DefaultHeader from '../CommonComponents/DefaultHeader';
import {useParams, useHistory} from 'react-router-dom'
import { useSnackbar } from 'notistack';
import {Auth} from '../../Api/auth';

import RequestDetails from "./ViewRequestComponents/RequestDetails";
import {RequestActionId} from '../../ApplicationConfig/constants';

export default function ViewRequest(){
    const {lang, requestId} = useParams();
    const {enqueueSnackbar} = useSnackbar();
    const [request, setRequest] = useState(null);
    const history = useHistory();

    const { t } = useTranslation();
    document.title =t("view request");

    const getRequestData = ()=> {
        Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/getRequest/"+requestId)
        .then((response)=> {
            if(!response.ok){
                enqueueSnackbar(response.statusText,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
                throw Error(response.statusText)
            }
            
            return response.json();
        })
        .then(data => {
            setRequest(data.data);
        })
    };
    
    useEffect(()=> {
        getRequestData();
    }, []);

    const handleRequestAction = (newStatus) => {
        Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/updateRow/" + lang + "/" + request.id + "/" + newStatus)
        .then((response)=> {
            if(!response.ok){
                enqueueSnackbar(response.statusText,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
                throw Error(response.statusText)
            }
            
            return response.json();
        })
        .then((data) => {
            if(newStatus===RequestActionId.Delete){
                history.push("/"+lang+"/home");
            }else{
                getRequestData();
            }
        })
    }

    return (
        <>
        <DefaultHeader />
        <main tabIndex={-1}>
            <Container maxWidth="xl" className="page-container">
                <Grid container direction="row" alignItems="stretch">
                    {request !== null &&
                        <RequestDetails request={request} lang={lang} handleRequestAction={handleRequestAction} />
                    }
                </Grid>
            </Container>
        </main>
        </>
    );
}