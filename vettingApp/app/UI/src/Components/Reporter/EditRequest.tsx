import React,{useState,useEffect} from 'react';
import {Container} from '@material-ui/core';
import { useTranslation } from "react-i18next";
import DefaultHeader from '../CommonComponents/DefaultHeader';
import {useParams} from 'react-router-dom'
import { useSnackbar } from 'notistack';
import {Auth} from '../../Api/auth';
import RequestForm from './RequestFormComponents/RequestForm';

export default function EditRequest(){
    const {requestId} = useParams();
    const {enqueueSnackbar} = useSnackbar();
    const [formData, setFormData] = useState(null);

    const { t } = useTranslation();
    document.title =t("Edit form");

    useEffect(()=> {
        Auth.authorizedFetch("/api/getRequest/"+requestId)
        .then((response)=> {
            if(!response.ok){
                enqueueSnackbar(response.statusText,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
                throw Error(response.statusText)
            }
            
            return response.json();
        })
        .then(data => {
            setFormData(data.data.data);
        });
    }, []);
    
    return (
        <React.Fragment>
            <DefaultHeader />
            <main tabIndex={-1}>
                <Container maxWidth="xl" className="page-container">
                    {formData !== null &&
                        <RequestForm formData={formData} setFormData={setFormData} requestIdValue={requestId}/>
                    }
                </Container>
            </main>
        </React.Fragment>
    );
}