import React,{useContext} from "react";
import {Grid,
    Paper,
    Typography,
    Tabs,
    Tab,Button,Box
} from '@material-ui/core';
import {Link as RouterLink} from 'react-router-dom';
import {makeStyles} from '@material-ui/styles';

import {RequestStatusId, RequestActionId} from '../../../ApplicationConfig/constants';

import {
    CalendarToday,
    Note,
    InsertDriveFile,
    Description,
    Comment,
    AddCircle
  } from '@material-ui/icons';

import {RequestStatusIcon} from '../../OutputChecker/RequestStatusIcon';
import FilesDetails from './FilesDetails';
import DisclosureDetails from './DisclosureDetails';

import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
    root: {
      boxShadow: 'none',
      borderRightWidth: '1px',
      borderRightStyle: 'solid',
      borderRightColor: theme.palette.divider,
      height: '100%',
      boxSizing: 'border-box',
      padding: theme.spacing(3, 2, 0, 0),
    },
    requestDetailZone:{
      boxShadow: 'none',
      borderRightWidth: '1px',
      borderRightStyle: 'solid',
      borderRightColor: theme.palette.divider,
    },
    requestIdenZone:{
      boxShadow: 'none',
      padding: theme.spacing(2, 0, 0, 2),
    },
    listRoot: {
      width: '100%'
    },
    header:{
      backgroundColor:'#f5f5f5',
      width:'100%',
      height:150,
      padding: theme.spacing(3, 0, 0, 3),
      borderBottom:'1px solid #dee2e6',
      flexGrow:1,
      boxSizing:'border-box',
    },
    requestStatusPaper:{
      boxShadow:'none',
      padding: theme.spacing(1, 2, 1, 2),
      maxWidth:200,
      float:'right'
    },
    filterTab:{
      width:'100%',
      height:100
    }
  }));

export default function RequestDetails({request, lang, handleRequestAction}){
  const classes = useStyles();
  const [tabValue, setTabValue] = React.useState(0);
  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };

  let backColor;
  let statusDesc;
  var actionList=[];
  
  if(request.statusId==RequestStatusId.InReview){
    statusDesc="In review";
    backColor='#ffab00';

    actionList.push(<Button color="primary" onClick={()=>handleRequestAction(RequestActionId.Withdraw)}><RequestStatusIcon statusId={RequestStatusId.Canceled} style={{ color: '#f00'}}/>&nbsp;Withdraw</Button>);
    actionList.push(<Button color="primary" onClick={()=>handleRequestAction(RequestActionId.Cancel)}><RequestStatusIcon statusId={RequestStatusId.Canceled} style={{ color: '#f00'}}/>&nbsp;Cancel</Button>);
    actionList.push(<Button color="primary" onClick={()=>handleRequestAction(RequestActionId.Duplicate)}><RequestStatusIcon statusId={RequestStatusId.Approved} style={{ color: '#008000'}}/>&nbsp;Duplicate</Button>);
  }else if(request.statusId==RequestStatusId.AwaitingReview){
    statusDesc="Awaiting review";
    backColor='#ffab00';

    actionList.push(<Button color="primary" onClick={()=>handleRequestAction(RequestActionId.Withdraw)}><RequestStatusIcon statusId={RequestStatusId.Canceled} style={{ color: '#f00'}}/>&nbsp;Withdraw</Button>);
    actionList.push(<Button color="primary" onClick={()=>handleRequestAction(RequestActionId.Cancel)}><RequestStatusIcon statusId={RequestStatusId.Canceled} style={{ color: '#f00'}}/>&nbsp;Cancel</Button>);
  }else if (request.statusId==RequestStatusId.Canceled){
    statusDesc="Canceled";
    backColor='#FF0000';

    actionList.push(<Button color="primary" onClick={()=>handleRequestAction(RequestActionId.Duplicate)}><RequestStatusIcon statusId={RequestStatusId.Approved} style={{ color: '#008000'}}/>&nbsp;Duplicate</Button>);
  }else if (request.statusId==RequestStatusId.Denied ){
    statusDesc="Errors, needs revision";
    backColor='#bf2600';

    actionList.push(<Button color="primary" onClick={()=>handleRequestAction(RequestActionId.Duplicate)}><RequestStatusIcon statusId={RequestStatusId.Approved} style={{ color: '#008000'}}/>&nbsp;Duplicate</Button>);
  }else if (request.statusId==RequestStatusId.WorkInProgress){
    statusDesc="Work in progress";
    backColor='#6b778c';

    actionList.push(<Button component={RouterLink} color="primary" to={"/"+lang+"/editrequest/"+request.id} ><RequestStatusIcon statusId={RequestStatusId.Approved} style={{ color: '#008000'}}/>&nbsp;Edit</Button>);
    actionList.push(<Button color="primary" onClick={()=>handleRequestAction(RequestActionId.Delete)}><RequestStatusIcon statusId={RequestStatusId.Canceled} style={{ color: '#f00'}}/>&nbsp;Delete</Button>);
  }else if (request.statusId==RequestStatusId.Draft){
    statusDesc="Work in progress";
    backColor='#6b778c';

    actionList.push(<Button component={RouterLink} color="primary" to={"/"+lang+"/editrequest/"+request.id} ><RequestStatusIcon statusId={RequestStatusId.Approved} style={{ color: '#008000'}}/>&nbsp;Edit</Button>);
    actionList.push(<Button color="primary" onClick={()=>handleRequestAction(RequestActionId.Delete)}><RequestStatusIcon statusId={RequestStatusId.Canceled} style={{ color: '#f00'}}/>&nbsp;Delete</Button>);
  }else if (request.statusId==RequestStatusId.Approved){
    statusDesc="Approved";
    backColor='#064';

    actionList.push(<Button color="primary" onClick={()=>handleRequestAction(RequestActionId.Duplicate)}><RequestStatusIcon statusId={RequestStatusId.Approved} style={{ color: '#008000'}}/>&nbsp;Duplicate</Button>);
  }

  return(
    <Grid container direction="column">
        <Grid item>
            <Paper className={classes.header} component="header">
                <Grid container direction='row'>
                    <Grid item xs={6}>
                        <Typography variant="h1">Request details</Typography>
                        <CalendarToday fontSize="small"/> <Typography variant="body2" component='span'>Submitted at {request.submittedOnTime} on {request.submittedOnDateFormat}</Typography> 
                    </Grid>
                    <Grid item xs={4}>
                        <Paper className={classes.requestStatusPaper} style={{backgroundColor:backColor}}>
                            <RequestStatusIcon statusId={request.statusId} style={{ color: '#FFFFFF',marginTop:5}}/> <Typography component="span" style={{ color: '#FFFFFF',marginTop:-5}}>{statusDesc}</Typography>
                        </Paper>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
        <Grid item>
            <Grid container direction="row">
                <Grid item lg={10}>
                    <Paper className={classes.requestDetailZone}>
                        <Tabs value={tabValue} onChange={handleTabChange} aria-label="simple tabs example">
                            <Tab icon={<InsertDriveFile />} label="Files" {...a11yProps(0)} />
                            <Tab icon={<Description />} label="Residual disclosure" {...a11yProps(1)} />
                            <Tab icon={<Note />} label="Comments" {...a11yProps(2)} />
                            <Tab icon={<Comment />} label="Discussion" {...a11yProps(3)} />
                        </Tabs>
                        <TabPanel value={tabValue} index={0}>
                            <FilesDetails requestData={request.data}/>
                        </TabPanel>
                        <TabPanel value={tabValue} index={1}>
                            <DisclosureDetails requestData={request.data}/>
                        </TabPanel>
                        <TabPanel value={tabValue} index={2}>
                            {request.data.additionalComments === "" &&
                                "No additional Comments"
                            }
                            {request.data.additionalComments !== "" &&
                                request.data.additionalComments
                            }
                        </TabPanel>
                        <TabPanel value={tabValue} index={3}>
                            Discussion
                        </TabPanel>
                    </Paper>
                </Grid>
                <Grid item lg={2}>
                    <Paper className={classes.requestIdenZone}>
                        <Typography variant="h6">Requester</Typography>
                        <p>{request.requester}</p>
                        <Typography variant="h6">Project</Typography>
                        <p>{request.projectName}</p>
                        {request.outputcheckers.length>0 && (
                            <>
                            <Typography variant="h6">Reviewers</Typography>
                            {request.outputcheckers.map((user)=>(
                                <p key={user.id}>{user.id}</p>
                            ))}
                            </>
                        )}   
                        {actionList.length > 0 &&
                        <>
                          <Typography variant="h6">Actions</Typography>
                          {actionList.map((action, i)=>(
                            <span key={i}>{action}</span>
                          ))}
                        </>}
                    </Paper>                                  
                </Grid>
            </Grid>
        </Grid>
    </Grid>
  )
}

function TabPanel({ children, value, index, ...other }) {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}