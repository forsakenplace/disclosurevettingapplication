import React from 'react';
import {Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody} from '@material-ui/core';

export default function DisclosureDetails({requestData}){
    let mandatoryFiles = [];
    for(const [key, value] of Object.entries(requestData.mandatoryDisclosureSupportingFiles)){
        mandatoryFiles.push({fileName:value.fileName, fileContents:key, notes:value.notes});
    }
    mandatoryFiles = mandatoryFiles.concat(requestData.otherDisclosureSupportingFiles);

    return (
        <Grid container item spacing={3}>
            <Grid item xs={12}>
                <TableContainer>
                    <Table>
                        <TableHead>
                        <TableRow>
                                <TableCell>Disclosure Supporting Files</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>File Name</TableCell>
                                <TableCell>File Contents</TableCell>
                                <TableCell>Notes</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {mandatoryFiles.map((row, i) => (
                                <TableRow key={i}>
                                    <TableCell>
                                        {row.fileName}
                                    </TableCell>
                                    <TableCell>
                                        {row.fileContents}
                                    </TableCell>
                                    <TableCell>
                                        {row.notes}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </Grid>
    );
}