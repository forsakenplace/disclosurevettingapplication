import React from 'react';
import {Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody} from '@material-ui/core';

export default function FilesDetails({requestData}){

    let supportingFiles = [];
    requestData.outputFiles.forEach((file)=>{
        for(const [key, value] of Object.entries(file.mandatorySupportingFiles)){
            supportingFiles.push({fileName:value.fileName, fileContents:key, notes:value.notes});
        }
        supportingFiles = supportingFiles.concat(file.otherSupportingFiles);
    });
   
    return (
        <Grid container item spacing={3}>
            <Grid item xs={12}>
                <TableContainer>
                    <Table>
                        <TableHead>
                        <TableRow>
                                <TableCell>Output Files</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>File Name</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {requestData.outputFiles.map((row,i) => (
                                <TableRow key={i}>
                                    <TableCell>
                                        {row.outputFile}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
            <Grid item xs={12}>
                <TableContainer>
                    <Table>
                        <TableHead>
                        <TableRow>
                                <TableCell>Supporting Files</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>File Name</TableCell>
                                <TableCell>File Contents</TableCell>
                                <TableCell>Notes</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {supportingFiles.map((row,i) => (
                                <TableRow key={i}>
                                    <TableCell>
                                        {row.fileName}
                                    </TableCell>
                                    <TableCell>
                                        {row.fileContents}
                                    </TableCell>
                                    <TableCell>
                                        {row.notes}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </Grid>
    );
}