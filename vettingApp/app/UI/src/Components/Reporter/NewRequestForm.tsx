import React, {useState, useContext} from 'react';
import {Container} from '@material-ui/core';
import { useTranslation } from "react-i18next";
import DefaultHeader from '../CommonComponents/DefaultHeader';
import RequestForm from './RequestFormComponents/RequestForm';
import requestFormDefaults from '../../StateObjects/requestFormDefaults.json';
import {UserInfoContext} from '../../StateObjects/UserInfoContext';
import {ProjectInfoContext} from '../../StateObjects/ProjectInfoContext';
import { format } from 'date-fns';

export default function NewRequestForm(){
    const { t } = useTranslation();
    document.title = t("requestFormTitle");

    const {projectInfo} = useContext(ProjectInfoContext);
    const {userInfo} = useContext(UserInfoContext);

    let formDefaultValues = localStorage.getItem("requestFormData");
    if(!formDefaultValues){
        let defaultValues = requestFormDefaults;
        defaultValues.user={id:userInfo.id, name:userInfo.fullName, email:userInfo.id , username:userInfo.id};
        defaultValues.project=projectInfo;
        defaultValues.date = format(new Date(), "yyyy-MM-dd");

        formDefaultValues = JSON.stringify(defaultValues);
        localStorage.setItem("requestFormData", formDefaultValues);
    }
    const [formData, setFormData] = useState(JSON.parse(formDefaultValues));
    const handleFormUpdate = (newFormData)=>{
        localStorage.setItem("requestFormData", JSON.stringify(newFormData));
        setFormData(newFormData);
    }

    return (
        <React.Fragment>
            <DefaultHeader />
            <main tabIndex={-1}>
                <Container maxWidth="xl" className="page-container">
                   <RequestForm formData={formData} setFormData={handleFormUpdate} requestIdValue={"0"}/>
                </Container>
            </main>
        </React.Fragment>
    );
}