import React,{useState,useEffect,useContext} from 'react';

import {ProjectInfoContext} from '../../StateObjects/ProjectInfoContext';
import {UserInfoContext} from '../../StateObjects/UserInfoContext';

import { makeStyles } from '@material-ui/core/styles';

import {Container, 
    Typography,
    Tabs,
    Tab,
    Box,
    Button} from '@material-ui/core';

import {RequestStatusId} from '../../ApplicationConfig/constants'

import AccountCircle from '@material-ui/icons/AccountCircle';

import { useTranslation } from "react-i18next";
import DefaultHeader from '../CommonComponents/DefaultHeader';
import EnhancedTable from './RequestsListComponents/EnhancedTable';
import { useSnackbar } from 'notistack';
import {Auth} from '../../Api/auth';

import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
    filterBtn:{
      border:'1px black solid',
      marginRight:'5px',
      marginTop:'10px',
      marginBottom:'10px'
    },
    filterHoverBtn:{
      backgroundColor:'#e0e0e0'
    },
  }));

export default function RequestsList(){

    const {projectInfo} = useContext(ProjectInfoContext);
    const {userInfo} = useContext(UserInfoContext);

    const classes = useStyles();

    const { t } = useTranslation();
    document.title =t("requestListTitle");
    
    const [tabValue, setTabValue] = React.useState(0);
    const [rows, setRows] = useState([]);
    const [myRequests,setMyRequests]=React.useState(false);

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    
    const [pageStatus,setPageStatus]=useState({
        dataLoaded:false,
        errorMessage:''
    });
    const handleTabChange = (event, newValue) => {
        setTabValue(newValue);
      };

    const applyMyRequestsFilter=(val) => {
        setMyRequests(val);
    }

    const getRequestData=()=>{
        Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/getRequests/" + projectInfo.id)
          .then(res => res.json())
          .then(
            (result) => {
              setRows(result);
              setPageStatus({...pageStatus,dataLoaded:true})
    
            }
          )
          .catch(
            (error)=>{
              setPageStatus({...pageStatus,dataLoaded:false,errorMessage:"An error occured. Please try again."})
            }
          ) 
    
          //TODO: check if a page change is required in case the user deleted all rows on a page
      }
      
      useEffect(() => {
        getRequestData()
      }, [projectInfo.id]);

    useEffect(()=>{
        let key;
        if (!pageStatus.dataLoaded && pageStatus.errorMessage=='')
        {
            key=enqueueSnackbar('Loading data',{variant:'info', persist: true,preventDuplicate: true});
        }
        else if (pageStatus.dataLoaded){
            closeSnackbar(key);
            enqueueSnackbar('Succesfully loaded data',{variant:'success',autoHideDuration:2000,preventDuplicate: true});
        }

        if (!pageStatus.dataLoaded && pageStatus.errorMessage!='') {
            closeSnackbar(key);
            enqueueSnackbar(pageStatus.errorMessage,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
        }
       
    },[pageStatus]);

    const filteredRows=rows.filter(r=>(myRequests && r.requester==userInfo.id) || !myRequests );

    return (
        <React.Fragment>
            <DefaultHeader />
            <main tabIndex={-1}>
                <Container maxWidth="xl" className="page-container">
                    <Typography variant="h1">{t("requestListTitle")}</Typography>
                    <Button onClick={()=>applyMyRequestsFilter(!myRequests)} className={`${classes.filterBtn} ${myRequests?classes.filterHoverBtn:''}`}><AccountCircle /> View my requests</Button>
                    <Tabs value={tabValue} onChange={handleTabChange} aria-label="simple tabs example">
                        <Tab label="All" {...a11yProps(0)} />
                        <Tab label="Draft" {...a11yProps(1)} />
                        <Tab label="Work in progress" {...a11yProps(2)} />
                        <Tab label="Awaiting review" {...a11yProps(3)} />
                        <Tab label="Approved" {...a11yProps(4)} />
                        <Tab label="Denied" {...a11yProps(5)} />
                        <Tab label="Canceled" {...a11yProps(6)} />
                    </Tabs>
                    <TabPanel value={tabValue} index={0}>
                        <EnhancedTable rows={filteredRows} refreshPage={myRequests} setPageStatus={setPageStatus} refreshData={getRequestData} />
                    </TabPanel>
                    <TabPanel value={tabValue} index={1}>
                        <EnhancedTable rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.Draft))} refreshPage={myRequests} setPageStatus={setPageStatus} refreshData={getRequestData} />
                    </TabPanel>
                    <TabPanel value={tabValue} index={2}>
                        <EnhancedTable rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.WorkInProgress))} setPageStatus={setPageStatus} refreshPage={myRequests} refreshData={getRequestData} />
                    </TabPanel>
                    <TabPanel value={tabValue} index={3}>
                        <EnhancedTable rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.AwaitingReview))} setPageStatus={setPageStatus} refreshPage={myRequests} refreshData={getRequestData} />
                    </TabPanel>
                    <TabPanel value={tabValue} index={4}>
                        <EnhancedTable rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.Approved))} setPageStatus={setPageStatus} refreshPage={myRequests} refreshData={getRequestData}/>
                    </TabPanel>
                    <TabPanel value={tabValue} index={5}>
                        <EnhancedTable rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.Denied))} setPageStatus={setPageStatus} refreshPage={myRequests} refreshData={getRequestData}/>
                    </TabPanel>
                    <TabPanel value={tabValue} index={6}>
                        <EnhancedTable rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.Canceled))} setPageStatus={setPageStatus} refreshPage={myRequests} refreshData={getRequestData}/>
                    </TabPanel>
                </Container>
            </main>
        </React.Fragment>
    );
}

function TabPanel({ children, value, index, ...other }) {
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && children
          
        }
      </div>
    );
  }
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }