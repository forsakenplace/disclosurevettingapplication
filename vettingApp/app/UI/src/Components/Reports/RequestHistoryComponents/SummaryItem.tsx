import React from 'react';
import {Paper} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
      summaryPaperItem:{
        boxShadow:'none',
        textAlign:'center',
        borderRight:'1px solid #dfe1e6',
        padding:theme.spacing(2)
      },
      summaryHeader:{
          fontFamily: theme.typography.fontFamily,
          fontSize:'1.2em',
          margin:0
      },
      summaryData:{
        fontFamily: theme.typography.fontFamily,
        fontSize:'1.2em'
      }
}))

export default function SummaryItem(props){
    const classes = useStyles();
    return (<Paper className={classes.summaryPaperItem}>
        <span className={classes.summaryData}>{props.data != ''?props.data:'-'}</span>
    <h6 className={classes.summaryHeader}>{props.caption}</h6>
    </Paper>)
}

SummaryItem.propTypes = {
    data: PropTypes.string,
    caption:PropTypes.string.isRequired
  };