import React,{useState,useEffect} from 'react';
import {Container, Typography,List,ListItem,Grid,Paper} from '@material-ui/core';
import { useTranslation } from "react-i18next";
import DefaultHeader from '../../CommonComponents/DefaultHeader';
import HistoryEntry from './HistoryEntry';
import SummaryItem from './SummaryItem';
import {Auth} from '../../../Api/auth';

import { useParams} from "react-router";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    summaryPaper: {
        boxShadow:'none',
        border:'1px solid #dfe1e6',
        marginBottom:'1em'
      },
      sectionHeader:{
        fontFamily: theme.typography.fontFamily,
        fontSize:'2em',
        marginBottom:'0.50em',
        marginLeft:0,
        marginRight:0,
        marginTop:0
      }
}))

export default function RequestReport(){

    const classes = useStyles();

    const { t } = useTranslation();
    const {requestId}=useParams("requestId");
    const {lang}=useParams("lang");
    
    document.title =t("requestReportTitle");

    const [data,setData]=useState([])
    

    const [pageStatus,setPageStatus]=React.useState({
        dataLoaded:false,
        errorMessage:''
    });

    useEffect(()=>{
        Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/reports/getHistory/" + requestId)
        .then(res => res.json())
        .then((response)=>{
            setData(response.data);
            setPageStatus({...pageStatus,dataLoaded:true});
        })
        .catch(
        (error)=>{
            setPageStatus({...pageStatus,errorMessage:"An error occured while getting data. Please try again."})
        }
        )
    },[requestId])

    return (
        <React.Fragment>
            <DefaultHeader />
            <main tabIndex={-1}>
                <Container maxWidth="xl" className="page-container">
                 <h1 className={classes.sectionHeader}>{t("requestReportSummary")}</h1>
                    {pageStatus.dataLoaded && 
                    <>
                    <Paper className={classes.summaryPaper}>
                    <Grid container direction="row" justify="center" alignItems="center">
                        <Grid item lg={3}>
                            <SummaryItem data={data.summary.firstSubmissionDate.length>0 && <time>{data.summary.firstSubmissionDate}</time>} caption="First submission date" />

                   </Grid>
                        <Grid item lg={3}>
                            <SummaryItem data={data.summary.approvalDate.length>0 && <time>{data.summary.approvalDate}</time>} caption="Approval date" />

                            </Grid>
                        <Grid item lg={3}>
                            <SummaryItem data={data.summary.daysToApproval} caption="Days to approval" />
                            </Grid>
                        <Grid item lg={3}>
                        <SummaryItem data={data.summary.submissionCounts} caption=" Submission total" />
                            </Grid>
                    </Grid>
                    </Paper>
                    <h1 className={classes.sectionHeader}>{t("requestReportTimeline")}</h1>
                    <List component="nav" >
                        {data.history.map((row)=>{
                            //row.statusDesc.filter(function (item) {return item[lang]})[0][lang]
                            return <ListItem key={row.id}><HistoryEntry id={row.id} statusDesc={row.statusDesc[lang]} statusId={row.statusId} userId={row.userId} dateCreated={row.dateCreated}/></ListItem>
                        })}
                    </List>
                    </>
                    }
                    {!pageStatus.dataLoaded && pageStatus.errorMessage!='' && pageStatus.errorMessage}
                </Container>
            </main>
        </React.Fragment>
    );
}