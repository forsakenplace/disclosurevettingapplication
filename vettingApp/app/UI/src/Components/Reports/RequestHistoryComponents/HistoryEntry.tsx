import React from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from "react-i18next";

import {RequestStatusId} from '../../../ApplicationConfig/constants';

import {InsertDriveFile,
    Edit,
    AccessTime,
    Cancel,
    CheckCircle,
    Visibility,
    Flag} from '@material-ui/icons';
import {Card,Grid,Typography,Tooltip} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

function GetHistoryText(statusId,userId){

    const { t } = useTranslation();

    if(statusId==RequestStatusId.Draft)
    {
       return <Typography>{t("historyCreated") + userId}</Typography>;
    }
    else if (statusId==RequestStatusId.WorkInProgress)
    {
        return <Typography>{t("historyUpdated") + userId}</Typography>;
    }
    else if (statusId==RequestStatusId.AwaitingReview)
    {
        return <Typography>{t("historyAwaitingReview") + userId}</Typography>;
    }
    else if (statusId==RequestStatusId.InReview)
    {
        return <Typography>{t("historyInReview") + userId}</Typography>;
    }
    else if (statusId==RequestStatusId.Denied)
    {
        return <Typography>{t("historyDenied") + userId}</Typography>;
    }
    else if (statusId==RequestStatusId.Canceled)
    {
        return <Typography>{t("historyCanceled") + userId}</Typography>;
    }
    else if (statusId==RequestStatusId.Approved)
    {
        return <Typography>{t("historyApproved") + userId}</Typography>;
    }
}

function GetStatusIcon(statusId,statusDesc){

    var iconToDisplay=<InsertDriveFile fontSize="large" style={{ color: '#6b778c'}} />

    if(statusId==RequestStatusId.WorkInProgress)
    {
        iconToDisplay=<Edit fontSize="large" style={{ color: '#6b778c'}} />
    }
    else if(statusId==RequestStatusId.AwaitingReview)
    {
        iconToDisplay= <AccessTime fontSize="large" style={{ color: '#ffab00'}} />;
    }
    else if (statusId==RequestStatusId.Canceled){
        iconToDisplay=  <Cancel fontSize="large" style={{color:'#FF0000'}}/>;
    }
    else if (statusId==RequestStatusId.Approved){
        iconToDisplay=<CheckCircle fontSize="large" style={{ color: '#064'}}/>;
    }
    else if (statusId==RequestStatusId.InReview)
    {
        iconToDisplay=<Visibility fontSize="large" style={{ color: '#ffab00'}}/>
    }
    else if (statusId==RequestStatusId.Denied)
    {
        iconToDisplay=<Flag fontSize="large" style={{ color: '#bf2600'}}/>
    }
    return <Tooltip title={statusDesc}>{iconToDisplay}</Tooltip>;
}

const useStyles = makeStyles((theme) => ({
    historyEntry: {
        marginBottom: theme.spacing(2),
      },
      historyCard:{
        border:'2px solid #dfe1e6;'
      },
      historyInfo:{
          backgroundColor:'#f5f6f8'
      },
      padding:{
          padding:'1em'
      },
      historyIcon:{
          boxShadow:'none',
          textAlign:'center',
          borderRadius:'100% !important',
          border:'2px solid #dfe1e6',
          width:'50%'
      }
}))

export default function HistoryEntry(props){
    const classes = useStyles();

    return (
        <Grid container direction="row" className={classes.historyEntry}>
            <Grid item lg={1}><Card className={classes.historyIcon}>{GetStatusIcon(props.statusId,props.statusDesc)}</Card></Grid>
            <Grid item lg={11}>
                <Card className={classes.historyCard}>
                    <Grid container direction="column">
                        <Grid item lg={12}><Card className={classes.padding}><Typography variant="h6"><time>{props.dateCreated}</time></Typography></Card></Grid>
                        <Grid item lg={12}><Card className={`${classes.historyInfo} ${classes.padding}`}>{GetHistoryText(props.statusId,props.userId)}</Card></Grid>
                    </Grid>
                </Card>
            </Grid>
        </Grid>
    )
}

HistoryEntry.propTypes = {
    id: PropTypes.number.isRequired,
    userId:PropTypes.string.isRequired,
    statusId:PropTypes.number.isRequired,
    statusDesc:PropTypes.string.isRequired,
    dateCreated:PropTypes.string.isRequired
  };