import React from 'react';
import {TableHead,TableRow,TableCell,TableSortLabel} from '@material-ui/core';

import PropTypes from 'prop-types';
import clsx from 'clsx';

const headCells = [
    { id: 'firstSubmissionDate', numeric: false, disablePadding: false, label: 'First submission date' },
    { id: 'lastOutputDate', numeric: false, disablePadding: false, label: 'Last output date' },
    { id: 'daysActive', numeric: true, disablePadding: false, label: 'Days active' },
    { id: 'submissionCount', numeric: false, disablePadding: false, label: 'Submisions' },
    { id: 'revisionCount', numeric: false, disablePadding: false, label: 'Revisions' },
    { id: 'statusId', numeric: true, disablePadding: false, label: 'Current request status' },
    { id: 'requester', numeric: false, disablePadding: false, label: 'Researcher' },
    { id: 'outputChecker', numeric: false, disablePadding: false, label: 'Checker name' },
    { id: 'fileCount', numeric: true, disablePadding: false, label: 'Total files' },
  ];
  
  export default function EnhancedTableHead(props) {
    const { classes, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = (property) => (event) => {
      onRequestSort(event, property);
    };
  
    return (
      <TableHead>
        <TableRow>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align='left'
              padding={headCell.disablePadding ? 'none' : 'default'}
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }
  
  EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
  };