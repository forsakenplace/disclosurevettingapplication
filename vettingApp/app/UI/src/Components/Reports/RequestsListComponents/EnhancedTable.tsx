import React,{useEffect, useContext} from 'react';

import { useHistory, useParams } from "react-router-dom"

import {RequestStatusId} from '../../../ApplicationConfig/constants'

import {ProjectInfoContext} from '../../../StateObjects/ProjectInfoContext';

import { lighten, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import InsertDriveFile from '@material-ui/icons/InsertDriveFile';

import CheckCircle from '@material-ui/icons/CheckCircle';
import Edit from '@material-ui/icons/Edit';
import AccessTime from '@material-ui/icons/AccessTime';
import Cancel from '@material-ui/icons/Cancel';
import Attachment from '@material-ui/icons/Attachment';
import Visibility from '@material-ui/icons/Visibility';
import Flag from '@material-ui/icons/Flag';
import Pageview from '@material-ui/icons/Pageview';

import EnhancedTableHead from './EnhancedTableHead';

import {Auth} from '../../../Api/auth';

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  filterBtn:{
    border:'1px black solid',
    marginRight:'5px',
    marginTop:'10px',
    marginBottom:'10px'
  },
  filterHoverBtn:{
    backgroundColor:'#e0e0e0'
  },
}));

export default function EnhancedTable(props) {

  const [rows,setRows]=React.useState([]);

  const {projectInfo} = useContext(ProjectInfoContext);      
       
  const getRequestData=()=>{
    Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/reports/getRequests/" + projectInfo.id)
      .then(res => res.json())
      .then(
        (result) => {
          setRows(result);
          props.setPageStatus({...props.pageStatus,dataLoaded:true});

        }
      )
      .catch(
        (error)=>{
          props.setPageStatus({...props.pageStatus,dataLoaded:false,errorMessage:"An error occured. Please try again."})
        }
      ) 
  }

  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  let  history  = useHistory();
  let { lang } = useParams();

  useEffect(() => {
    getRequestData()
  }, [projectInfo.id]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleViewClick=(requestId)=>{
    history.push("/" + lang + "/reports/" + requestId);
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const isSelected = (name) => selected.indexOf(name) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  return (
  
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size="medium"
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.id);
                  const labelId = `enhanced-table-checkbox-${index}`;
                 // var status=row.status.split("|")[1]
                  var status=row.statusId
                  var iconToDisplay=<InsertDriveFile style={{ color: '#6b778c'}} />

                  if(status==RequestStatusId.WorkInProgress)
                  {
                    iconToDisplay=<Edit style={{ color: '#6b778c'}} />
                  }
                  else if(status==RequestStatusId.AwaitingReview)
                  {
                    iconToDisplay= <AccessTime style={{ color: '#ffab00'}} />;
                  }
                  else if (status==RequestStatusId.Canceled){
                    iconToDisplay=  <Cancel style={{color:'#FF0000'}}/>;
                  }
                  else if (status==RequestStatusId.Approved){
                    iconToDisplay=<CheckCircle style={{ color: '#064'}}/>;
                  }
                  else if (status==RequestStatusId.InReview)
                  {
                    iconToDisplay=<Visibility style={{ color: '#ffab00'}}/>
                  }
                  else if (status==RequestStatusId.Denied)
                  {
                    iconToDisplay=<Flag style={{ color: '#bf2600'}}/>
                  }

                  return (
                    <TableRow
                      hover
                      //onClick={(event) => handleClick(event, row.id)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.id}
                      selected={isItemSelected}
                    >
                      <TableCell component="th" id={labelId} align="left" scope="row">
                        <Tooltip title={row.statusDesc}>
                        {iconToDisplay}
                        </Tooltip>
                      </TableCell>
                      <TableCell align="left">{row.firstSubmissionDateFormat}</TableCell>
                      <TableCell align="left">?</TableCell>
                      <TableCell align="left">{row.daysActive}</TableCell>
                      <TableCell align="left">{row.submissionCount}</TableCell>
                      <TableCell align="left">{row.revisionCount}</TableCell>
                      <TableCell align="left">{row.requester}</TableCell>
                      <TableCell align="left">{row.outputchecker}</TableCell>
                      <TableCell align="left"><Attachment /> {row.fileOutputCount}</TableCell>
                      <TableCell align="left"><Tooltip title="View request"><IconButton onClick={()=>handleViewClick(row.id)}><Pageview /></IconButton></Tooltip></TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 33  * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}
