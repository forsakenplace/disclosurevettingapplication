import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function AlertDialog({openAlert, handleAlertClose, handleConfirmButton, descriptionMessage}) {

    return (
        <>
        <Dialog
        open={openAlert}
        onClose={handleAlertClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">Are you sure?</DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                    {descriptionMessage}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleAlertClose} color="primary">No</Button>
                <Button onClick={handleConfirmButton} color="primary" autoFocus>Yes</Button>
            </DialogActions>
        </Dialog>
        </>
    );
}

AlertDialog.propTypes = {
    openAlert: PropTypes.bool.isRequired,
    handleAlertClose: PropTypes.func.isRequired,
    handleConfirmButton: PropTypes.func.isRequired,
    descriptionMessage: PropTypes.string.isRequired
}
