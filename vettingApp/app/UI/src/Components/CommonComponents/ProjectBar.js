import React, {useContext} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

import {UserInfoContext} from '../../StateObjects/UserInfoContext';
import {ProjectInfoContext} from '../../StateObjects/ProjectInfoContext';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    hidden:{
        visibility:'hidden'
    },
  }));

export default function ProjetBar(){
  const {projectInfo, setProjectInfo} = useContext(ProjectInfoContext)
  const {userInfo} = useContext(UserInfoContext);

  const classes = useStyles();

  const onProjectChange=(event)=>{
    const project = userInfo.projects.find(proj => proj.id===event.target.value);
    setProjectInfo(project);
  }
  
  return (
    <div className={classes.root}>
      <FormControl variant="outlined" style={{width:'100%'}}>
        <InputLabel id="project-name-label">Filter by project</InputLabel>
        <Select id="project-name-select" 
        labelId="project-name-label" 
        onChange={onProjectChange}
        value={projectInfo.id}
        label="Filter by project"
        margin="dense">
            {userInfo.projects.map((project)=>(
              <MenuItem key={project.id} value={project.id}>{project.name}</MenuItem>
            ))}
        </Select>
        </FormControl>
    </div>
  );
}
