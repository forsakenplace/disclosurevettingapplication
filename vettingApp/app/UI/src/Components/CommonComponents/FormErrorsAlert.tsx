import React from 'react';
import {Alert, AlertTitle} from '@material-ui/lab';

export default function FormErrorsAlert({errors}){
    if(Object.keys(errors).length === 0){
        return null;
    }

    let i = 0;
    const errorListOutput = (item, index, errorObject)=>{
        if(errorObject[item].message){
            return (
                <li key={i++}>
                    <a href={"#"+item}>{item} : {errorObject[item].type} - {errorObject[item].message}</a>
                </li>
            );
        }else{
            let sublist = [];
            for(const key of Object.keys(errorObject[item])){
                sublist.push(errorListOutput(key, index+"-"+key, errorObject[item]));
            }
            return (
                <React.Fragment key={i++}>
                    <li key={i++}>
                        {item} :
                    </li>
                    <ul key={i++}>{sublist}</ul>
                </React.Fragment>
            );
        }
    };

    return (
        <Alert severity="error">
            <AlertTitle>Error</AlertTitle>
            <ul>
                {Object.keys(errors).map((item, index)=> (
                    errorListOutput(item, index, errors)
                ))}
            </ul>
        </Alert>
    );
}