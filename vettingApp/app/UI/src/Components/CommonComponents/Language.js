import React from 'react';
import {Button} from '@material-ui/core';
import { useTranslation } from "react-i18next";
import { useHistory, useParams } from "react-router-dom"

export default function Language(props) {

  let  history  = useHistory();
  let { lang } = useParams();
  const { t } = useTranslation();

  var flipLang=t("flipLang");

  const changeLanguage=()=>{
    history.push(location.pathname.replace("/" + lang, "/" + flipLang))
  }
  return (
    <React.Fragment>
      <h2 className="screen-reader-text">{t("languageChangeScreenReader")}</h2>
      <Button onClick={()=>changeLanguage()}>
        <span lang={flipLang}>{t("languageChange")}</span>
      </Button>
    </React.Fragment>
  );
}
