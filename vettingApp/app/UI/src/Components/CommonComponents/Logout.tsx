import React from 'react';
import {Button} from '@material-ui/core';

import {Auth} from '../../Api/auth';

import { useSnackbar } from 'notistack';


export default function Logout(){

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const logoutHandler=()=>{
        Auth.authorizedFetch(process.env.API_ROOT_URL + "/logout")
        .then(res => res.json())
        .then(
            (result) => {
                if (result.statusCode==200)
                {
                    Auth.logout();              
                    window.location.href=result.redirectUrl;
                }
            }
        )
        .catch(
            (error)=>{
                enqueueSnackbar("Error happened while logging out. Please try again.",{variant:'error',autoHideDuration:2000,preventDuplicate: true});
            }
        )
    }

    return (
        <Button onClick={logoutHandler} variant="outlined" color="primary">
        Logout
      </Button>
    )
}