import React from 'react';
import PropTypes from 'prop-types';
import {Paper,Typography} from '@material-ui/core'
import RequestCard from './RequestCard';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    paper: {
        height: '100%',
        padding: '1em',
        width: 280,
        backgroundColor:'#f5f5f5',
        border:'1px solid #dee2e6',
        boxSizing: 'border-box',
        boxShadow:'none'
      }
  }));

export default function RequestsList(props){
    const classes = useStyles();

    return (<Paper className={classes.paper}>
        <Typography variant="h2">{props.statusIcon} {props.statusTitle}</Typography>
        {props.rows.map((row)=>{
           
            return (<RequestCard key={row.id} statusId={row.statusId} 
                reporter={row.requester}
                fileCount={row.fileOutputCount}
                updatedOn="3:30pm"
                requestId={row.id}
                />)
        })}
    </Paper>)
}

RequestsList.propTypes = {
    rows: PropTypes.array.isRequired,
    statusIcon:PropTypes.object.isRequired,
    statusTitle: PropTypes.string.isRequired
  };