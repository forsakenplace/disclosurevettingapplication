import React,{useState, useEffect, useContext} from 'react';
import {Container, Typography, FormControl,MenuItem,InputLabel,Select} from '@material-ui/core';
import DefaultHeader from '../CommonComponents/DefaultHeader';

import Flag from '@material-ui/icons/Flag';
import Visibility from '@material-ui/icons/Visibility';
import AccessTime from '@material-ui/icons/AccessTime';
import Cancel from '@material-ui/icons/Cancel';
import CheckCircle from '@material-ui/icons/CheckCircle';

import {UserInfoContext} from '../../StateObjects/UserInfoContext';
import {ProjectInfoContext} from '../../StateObjects/ProjectInfoContext';

import {RequestType} from '../../ApplicationConfig/Outputchecker/constants'
import {RequestStatusId} from '../../ApplicationConfig/constants'

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import RequestsList from './RequestsList';

import { useSnackbar } from 'notistack';

import {Auth} from '../../Api/auth'

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    request:{
        backgroundColor:'#FFFFFF',
        marginTop:'1em',
        marginBottom:'1em'
    },
    paperBar: {
        height: 100,
        padding: '1em',
        backgroundColor:'#f5f5f5',
        border:'1px solid #dee2e6',
        boxShadow: 'none',
        boxSizing:'border-box'
      },
    control: {
      padding: theme.spacing(2),
    },
  }));

export default function RequestsDashboard({rows, setRows}){
    const {projectInfo} = useContext(ProjectInfoContext);
    const {userInfo} = useContext(UserInfoContext);
      
    const [pageStatus,setPageStatus]=React.useState({
        dataLoaded:false,
        errorMessage:''
    });

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    let filterVal = localStorage.getItem("ocFilterId");
    if(!filterVal){
        filterVal = JSON.stringify(RequestType.MyRequests);
        localStorage.setItem("ocFilterId",filterVal);
    }
    const [filterId, setFilterId] = useState(JSON.parse(filterVal));
    const handleOcFilterChange = (newValue) => {
        localStorage.setItem("ocFilterId",JSON.stringify(newValue));
        setFilterId(newValue);
    }
    
    useEffect(()=>{
        let key;
        if (!pageStatus.dataLoaded && pageStatus.errorMessage=='')
        {
            key=enqueueSnackbar('Loading data',{variant:'info', persist: true,preventDuplicate: true});
        }
        else if (pageStatus.dataLoaded){
            closeSnackbar(key);
            enqueueSnackbar('Succesfully loaded data',{variant:'success',autoHideDuration:2000,preventDuplicate: true});
        }

        if (!pageStatus.dataLoaded && pageStatus.errorMessage!='') {
            closeSnackbar(key);
            enqueueSnackbar(pageStatus.errorMessage,{variant:'error',autoHideDuration:2000,preventDuplicate: true});
        }
    
    },[pageStatus]);

      useEffect(() => {

        Auth.authorizedFetch(process.env.API_ROOT_URL + "/api/getRequests/" + projectInfo.id)
          .then(res => res.json())
          .then(
            (result) => {
              setRows(result);
              setPageStatus({...pageStatus,dataLoaded:true});
            }
          )
          .catch(
            (error)=>{
              setPageStatus({...pageStatus,dataLoaded:false,errorMessage:"An error occured. Please try again."})
            }
          )
      }, [projectInfo.id]);

      const classes = useStyles();    

      const onRequestTypeChange=(event)=>{
        handleOcFilterChange(event.target.value);
      }

    let filteredRows;
    if (filterId==RequestType.MyRequests)
    {
        filteredRows=rows.filter(r=>(r.outputcheckers.findIndex(el=>el.id==userInfo.id)!=-1));
        
    }
    else if (filterId==RequestType.AllRequests)
    {
        filteredRows=rows;
    }
    else if (filterId==RequestType.UnassignedRequests)
    {
        filteredRows=rows.filter(r=>(r.outputcheckers.length==0));
    }
    
    return (
        <React.Fragment>
            <DefaultHeader />
            <main>
                <div className="page-container">
                    <Grid container direction="column" className={classes.root} spacing={2}>
                        <Grid item lg={12}>
                            <Paper className={classes.paperBar}>
                                <FormControl variant="outlined">
                                    <InputLabel id="request-type-label">Request filtering</InputLabel>
                                    <Select 
                                    id="requests"
                                    labelId="request-type-label"
                                    onChange={onRequestTypeChange}
                                    value={filterId}
                                    label="Request filtering"
                                    >
                                        <MenuItem value={RequestType.MyRequests}>Show my requests</MenuItem>
                                        <MenuItem value={RequestType.AllRequests}>Show all requests</MenuItem>
                                        <MenuItem value={RequestType.UnassignedRequests}>Show unassigned</MenuItem>
                                    </Select>
                                </FormControl>
                            </Paper>
                        </Grid>
                        <Grid item>
                            <Grid container justify="center" spacing={2} direction="row" alignItems="stretch">
                                <Grid item>
                                    <RequestsList 
                                    statusIcon={<AccessTime fontSize="small" style={{ color: '#ffab00'}} />}
                                    statusTitle="Available for review" 
                                    rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.AwaitingReview))}/>
                                </Grid>
                                <Grid item>
                                    <RequestsList 
                                    statusIcon={<Visibility fontSize="small" style={{ color: '#ffab00'}}/>}
                                    statusTitle="In review" 
                                    rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.InReview))}/>
                                </Grid>
                                <Grid item>
                                    <RequestsList 
                                    statusIcon={<CheckCircle fontSize="small" style={{ color: '#064'}}/>}
                                    statusTitle="Approved" 
                                    rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.Approved))}/>
                                </Grid>
                                <Grid item>
                                    <RequestsList 
                                    statusIcon={<Flag fontSize="small" style={{ color: '#bf2600'}}/>}
                                    statusTitle="Denied" 
                                    rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.Denied))}/>
                                </Grid>
                                <Grid item>
                                    <RequestsList 
                                    statusIcon={<Cancel fontSize="small" style={{color:'#FF0000'}}/>}
                                    statusTitle="Cancelled" 
                                    rows={filteredRows.filter(r=>(r.statusId==RequestStatusId.Canceled))}/>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>                    
                </div>
            </main>
        </React.Fragment>
    );
}