import React from 'react';
import {Container, Typography} from '@material-ui/core';
import DefaultHeader from '../CommonComponents/DefaultHeader';

export default function RequestsApproval(){
    return (
        <React.Fragment>
            <DefaultHeader />
            <main>
                <Container maxWidth="xl" className="page-container">
                    <Typography variant="h1">To be continued...</Typography>
                    
                </Container>
            </main>
        </React.Fragment>
    );
}