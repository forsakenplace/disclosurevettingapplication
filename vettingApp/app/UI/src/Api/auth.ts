/**
 * Utility class for handling client-side authentication.
 */

export class Auth {

    
    static isAuthenticated () {
        const token = Auth.token();
        return token != null;
    };

    static login (id_token: string) {
        const storage = window.localStorage;
        storage.setItem('jwt_token', id_token);
        //storage.setItem('jwt_refresh_token', refresh_token);
    }

    static logout () {
        window.localStorage.clear();
    }

    static header () {
        const token = Auth.token();
        if (token) {
            return {'Authorization': `Bearer ${token}`}
        }
        return {}
    }

    static token () {
        const storage = window.localStorage;
        return storage.getItem("jwt_token");
    }

    /**
     * Identical to `window.fetch()`, except it handles token authentication
     * and refresh for you.
     */
    static authorizedFetch (info: RequestInfo, init: RequestInit = {}) {
        const headers = new Headers(init.headers);
        const token = Auth.token();
        if (token) {
            headers.set('Authorization', `Bearer ${token}`);
        }

        return fetch(info, {
            ...init,
            headers: headers
        });
    }
}