export const XS_SCREEN = 599;
export const SM_SCREEN = 959;
export const MD_SCREEN = 1279;
export const LG_SCREEN = 1919;

export const HEAD_H = 64;
export const EXP_HEAD_H = 160;
export const FOOT_H = 144;
