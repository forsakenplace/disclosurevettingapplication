import React from 'react'

export const defaultUserInfoValues = {
    id: '',
    fullName:'',
    role: 0,
    projects:[]
};

export const UserInfoContext = React.createContext({userInfo: defaultUserInfoValues, setUserInfo: (values)=>{}});