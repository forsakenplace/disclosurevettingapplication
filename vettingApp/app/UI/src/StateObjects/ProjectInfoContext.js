import React from 'react'

export const defaultProjectInfoValues = {
    id: '',
    name:''
};

export const ProjectInfoContext = React.createContext({projectInfo: defaultProjectInfoValues, setProjectInfo: (values)=>{}});