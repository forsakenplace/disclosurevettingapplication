import React from 'react'
import i18n from "i18next";
import { useParams } from 'react-router';


export default function Localization(props){
    let { lang } = useParams();
    if (i18n.language != lang)
    {
      i18n.changeLanguage(lang);
    }
    return (props.children)


}