#!/bin/bash
set -e

while !</dev/tcp/db/5432; do
    echo "Waiting for POSTGRES"
    sleep 1
done

python setup.py develop

gunicorn --bind 0.0.0.0:5000 run:app